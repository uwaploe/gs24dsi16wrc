// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/utils/util_auto_calibrate.c $
// $Rev: 25371 $
// $Date: 2014-02-26 13:25:18 -0600 (Wed, 26 Feb 2014) $

#include <stdio.h>

#include "24dsi16wrc.h"
#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi16wrc_auto_calibrate
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI16WRC_IOCTL_AUTO_CALIBRATE service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi16wrc_auto_calibrate(int fd, int index)
{
	int	errs;

	gsc_label_index("Auto-Calibrate", index);
	errs	= dsi16wrc_dsl_ioctl(fd, DSI16WRC_IOCTL_AUTO_CALIBRATE, NULL);
	printf("%s\n", errs ? "FAIL <---" : "PASS");
	return(errs);
}


