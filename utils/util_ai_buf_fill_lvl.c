// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/utils/util_ai_buf_fill_lvl.c $
// $Rev: 32482 $
// $Date: 2015-07-04 15:00:15 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>

#include "24dsi16wrc.h"
#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi16wrc_ai_buf_fill_lvl
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI16WRC_IOCTL_AI_BUF_FILL_LVL service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		get		The results are reported here. This may be NULL.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi16wrc_ai_buf_fill_lvl(int fd, int index, s32* get)
{
	int	errs;
	s32	set;

	gsc_label_index("AI Buffer Fill Level", index);
	errs	= dsi16wrc_dsl_ioctl(fd, DSI16WRC_IOCTL_AI_BUF_FILL_LVL, &set);
	printf(	"%s  (%ld sample%s)\n",
			errs ? "FAIL <---" : "PASS",
			(long) set,
			(set == 1) ? "" : "s");

	if (get)
		get[0]	= set;

	return(errs);
}


