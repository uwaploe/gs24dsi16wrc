#include <errno.h>
#include <stdio.h>

#include "24dsi16wrc_dsl.h"

int dsi16wrc_dsl_close(int fd)
{
	int	err;
	int	status;

	status	= close(fd);

	if (status == -1)
		printf("ERROR: close() failure, errno = %d\n", errno);

	err	= (status == -1) ? 1 : 0;
	return(err);
}
