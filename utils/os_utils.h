// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/utils/linux/os_utils.h $
// $Rev: 32074 $
// $Date: 2015-06-15 22:33:10 -0500 (Mon, 15 Jun 2015) $

#ifndef __OS_UTILS_H__
#define __OS_UTILS_H__

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <asm/types.h>
#include <sys/ioctl.h>

#include "os_common.h"



// data types *****************************************************************

typedef struct
{
	pthread_t	thread;
	char		name[64];
} os_thread_t;

typedef	struct
{
	void*		key;	// struct is valid only if key == & of struct
	sem_t		sem;
} os_sem_t;

typedef enum
{
	OS_SEM_GET_OK,	// The operation was a success.
	OS_SEM_GET_TO,	// The operation timed out.
	OS_SEM_GET_ER	// There was an error.
} os_sem_get_t;



// prototypes *****************************************************************

void	os_close(int fd);
int		os_count_boards(const char* base);		// # of boards

int		os_id_driver(const char* base);			// # of errors
void	os_id_host(void);

void	os_kbd_close(void);
void	os_kbd_open(void);
int		os_kbd_hit(void);
int		os_kbd_read(void);

int		os_open(unsigned int index, const char* base);

int		os_sem_create(os_sem_t* sem);
int		os_sem_create_qty(os_sem_t* sem, int cap, int put);
int		os_sem_destroy(os_sem_t* sem);
int		os_sem_lock(os_sem_t* sem);
int		os_sem_lock_to(os_sem_t* sem, int timeout_ms);
int		os_sem_unlock(os_sem_t* sem);

void	os_sleep_ms(int ms);

int		os_thread_create(os_thread_t* thread, const char* name, int (*func)(void* arg), void* arg);
int		os_thread_destroy(os_thread_t* thread);



#endif
