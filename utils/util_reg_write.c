// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/utils/util_reg_write.c $
// $Rev: 32102 $
// $Date: 2015-06-16 16:29:33 -0500 (Tue, 16 Jun 2015) $

#include <stdio.h>

#include "24dsi16wrc.h"
#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"
#include "gsc_utils.h"



//*****************************************************************************
int dsi16wrc_reg_write(int fd, u32 reg, u32 value)
{
	gsc_reg_t	arg;
	int			errs;

	arg.reg		= reg;
	arg.value	= value;
	arg.mask	= 0;
	errs		= dsi16wrc_dsl_ioctl(fd, DSI16WRC_IOCTL_REG_WRITE, (void*) &arg);
	return(errs);
}


