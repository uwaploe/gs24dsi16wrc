// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/utils/util_auto_cal_sts.c $
// $Rev: 32482 $
// $Date: 2015-07-04 15:00:15 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>

#include "24dsi16wrc.h"
#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi16wrc_auto_cal_sts
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI16WRC_IOCTL_AUTO_CAL_STS service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		get		The results are reported here. This may be NULL.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi16wrc_auto_cal_sts(int fd, int index, s32* get)
{
	char		buf[128];
	int			errs;
	const char*	ptr;
	s32			set;

	gsc_label_index("Auto-Cal Status", index);
	errs	= dsi16wrc_dsl_ioctl(fd, DSI16WRC_IOCTL_AUTO_CAL_STS, &set);

	switch (set)
	{
		default:

			errs++;
			ptr	= buf;
			sprintf(buf, "Unrecognized option: 0x%lX", (long) set);
			break;

		case DSI16WRC_AUTO_CAL_STS_ACTIVE:

			ptr	= "Active";
			break;

		case DSI16WRC_AUTO_CAL_STS_FAIL:

			ptr	= "Fail";
			break;

		case DSI16WRC_AUTO_CAL_STS_PASS:

			ptr	= "PASS";
			break;
	}

	printf("%s  (%s)\n", errs ? "FAIL <---" : "PASS", ptr);

	if (get)
		get[0]	= set;

	return(errs);
}


