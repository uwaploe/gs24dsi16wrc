// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/driver/close.c $
// $Rev: 32699 $
// $Date: 2015-07-09 17:02:50 -0500 (Thu, 09 Jul 2015) $

#include "main.h"



//*****************************************************************************
int dev_close(dev_data_t* dev)
{
	initialize_ioctl(dev, NULL);
	gsc_wait_close(dev);
	dev_io_close(dev);
	gsc_dma_close(dev, 0);
	gsc_irq_close(dev, 0);
	return(0);
}



