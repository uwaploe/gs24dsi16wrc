// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/id/id.c $
// $Rev: 32516 $
// $Date: 2015-07-04 15:23:40 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>
#include <string.h>

#include "main.h"



//*****************************************************************************
static void _local_label_string(const char* str)
{
	int	len;

	printf("%s", str);
	len	= 7 - (int) strlen(str);

	for (; len > 0; len--)
		printf(" ");
}



//*****************************************************************************
static void _local_label_long(long l)
{
	char	buf[32];

	sprintf(buf, "%ld", l);
	_local_label_string(buf);
}



//*****************************************************************************
static int _voltage_ranges(int fd)
{
	int	errs	= 0;
	s32	range;

	gsc_label("Voltage Ranges");
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_V_RANGE, &range);

	if (errs == 0)
	{
		switch (range)
		{
			default:

				errs	= 1;
				printf("ERROR: %s, line %d\n",__FILE__, __LINE__);
				break;

			case DSI16WRC_QUERY_V_RANGE_10_BY_10:

				printf("+-10V, +-1V, +-0.1V, +-0.01V\n");
				break;

			case DSI16WRC_QUERY_V_RANGE_10_BY_2:

				printf("+-10V, +-5V, +-2.5V, +-1.25V\n");
				break;
		}
	}

	return(errs);
}



//*****************************************************************************
static int _input_modes(int fd)
{
	gsc_label("Input Modes");
	printf("Differential, Zero Test, +Vref Test\n");
	return(0);
}



//*****************************************************************************
static int _channel_qty(int fd)
{
	int	errs	= 0;
	s32	qty;

	gsc_label("A/D Channels");
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_CHANNEL_QTY, &qty);

	if (errs)
	{
	}
	else if (qty <= 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. qty response error: %ld)\n",
				__LINE__,
				(long) qty);
	}
	else
	{
		_local_label_long(qty);
		printf("(24/20/18/16-bit resolution)\n");
	}

	return(errs);
}



//*****************************************************************************
static int _channel_max(int fd)
{
	int	errs	= 0;
	s32	max;

	gsc_label("Maximum Channels");
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_CHANNEL_MAX, &max);

	if (errs)
	{
	}
	else if (max <= 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. max response error: %ld)\n",
				__LINE__,
				(long) max);
	}
	else
	{
		_local_label_long(max);
		printf("(24/20/18/16-bit resolution)\n");
	}






	return(errs);
}



//*****************************************************************************
static int _fref_default(int fd)
{
	int	errs	= 0;
	s32	ref;

	gsc_label("Default Fref");

	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_FREF_DEFAULT, &ref);

	if (errs == 0)
	{
		gsc_label_long_comma(ref);
		printf(" Hz\n");
	}

	return(errs);
}



//*****************************************************************************
static int _fgen_range(int fd)
{
	int	errs	= 0;
	s32	max;
	s32	min;

	gsc_label("Fgen Range");

	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_FGEN_MAX, &max);
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_FGEN_MIN, &min);

	if (errs)
	{
	}
	else if (max <= 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. max response error: %ld)\n",
				__LINE__,
				(long) max);
	}
	else if (min <= 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. min response error: %ld)\n",
				__LINE__,
				(long) min);
	}
	else if (min >= max)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. range error: %ld-%ld)\n",
				__LINE__,
				(long) min,
				(long) max);
	}
	else
	{
		gsc_label_long_comma(min);
		printf(" to ");
		gsc_label_long_comma(max);
		printf(" Hz\n");
	}

	return(errs);
}



//*****************************************************************************
static int _nvco_range(int fd)
{
	int	errs	= 0;
	s32	max;
	s32	min;

	gsc_label("Nvco Range");

	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_NVCO_MAX, &max);
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_NVCO_MIN, &min);

	if (errs)
	{
	}
	else if (max < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. max response error: %ld)\n",
				__LINE__,
				(long) max);
	}
	else if (min < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. min response error: %ld)\n",
				__LINE__,
				(long) min);
	}
	else if (min >= max)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. range error: %ld-%ld)\n",
				__LINE__,
				(long) min,
				(long) max);
	}
	else
	{
		gsc_label_long_comma(min);
		printf(" to ");
		gsc_label_long_comma(max);
		printf(" \n");
	}

	return(errs);
}



//*****************************************************************************
static int _nref_range(int fd)
{
	int	errs	= 0;
	s32	max;
	s32	min;

	gsc_label("Nref Range");

	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_NREF_MAX, &max);
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_NREF_MIN, &min);

	if (errs)
	{
	}
	else if (max < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. max response error: %ld)\n",
				__LINE__,
				(long) max);
	}
	else if (min < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. min response error: %ld)\n",
				__LINE__,
				(long) min);
	}
	else if (min >= max)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. range error: %ld-%ld)\n",
				__LINE__,
				(long) min,
				(long) max);
	}
	else
	{
		gsc_label_long_comma(min);
		printf(" to ");
		gsc_label_long_comma(max);
		printf("\n");
	}

	return(errs);
}



//*****************************************************************************
static int _ndiv_range(int fd)
{
	int	errs	= 0;
	s32	max;
	s32	min;

	gsc_label("Ndiv Range");

	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_NDIV_MAX, &max);
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_NDIV_MIN, &min);

	if (errs)
	{
	}
	else if (max < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. max response error: %ld)\n",
				__LINE__,
				(long) max);
	}
	else if (min < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. min response error: %ld)\n",
				__LINE__,
				(long) min);
	}
	else if (min >= max)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. range error: %ld-%ld)\n",
				__LINE__,
				(long) min,
				(long) max);
	}
	else
	{
		gsc_label_long_comma(min);
		printf(" to ");
		gsc_label_long_comma(max);
		printf("\n");
	}

	return(errs);
}



//*****************************************************************************
static int _fsamp_range(int fd)
{
	int	errs	= 0;
	s32	max;			// Sample Rate Maximum
	s32	min;			// Sample Rate Minimum

	gsc_label("Sample Rates");

	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_FSAMP_MAX, &max);
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_FSAMP_MIN, &min);

	if (errs)
	{
	}
	else if (max < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. max response error: %ld)\n",
				__LINE__,
				(long) max);
	}
	else if (min < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. min response error: %ld)\n",
				__LINE__,
				(long) min);
	}
	else if (min >= max)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. range error: %ld-%ld)\n",
				__LINE__,
				(long) min,
				(long) max);
	}
	else
	{
		gsc_label_long_comma(min);
		printf(" to ");
		gsc_label_long_comma(max);
		printf(" S/S/C\n");
	}

	return(errs);
}



//*****************************************************************************
static int _filter_freq(int fd)
{
	int	errs;
	s32	freq;

	gsc_label("Filter Frequency");
	errs	= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_FILTER_FREQ, &freq);

	if (errs)
	{
	}
	else if (freq == 0)
	{
		printf("No Filter Installed\n");
	}
	else if (freq < 0)
	{
		printf("FAIL <---  (%d. unknown)\n", __LINE__);
	}
	else
	{
		gsc_label_long_comma(freq);
		printf(" Hz\n");
	}

	return(errs);
}



//*****************************************************************************
static int _fifo_size(int fd)
{
	int	errs	= 0;
	s32	size;			// FIFO Size in samples

	gsc_label("FIFO Size");

	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_FIFO_SIZE, &size);

	if (errs)
	{
	}
	else if (size <= 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. size response error: %ld)\n",
				__LINE__,
				(long) size);
	}
	else
	{
		gsc_label_long_comma(size);
		printf(" Samples Deep");

		if ((size % 1024) == 0)
			printf("  (%ldK)", (long) size / 1024);

		printf("\n");
	}

	return(errs);
}



//*****************************************************************************
static int _initialize(int fd)
{
	int	errs	= 0;
	s32	ms;

	gsc_label("Initialize Period");

	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_INIT_MS, &ms);

	if (errs)
	{
	}
	else if (ms <= 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. ms response error: %ld)\n",
				__LINE__,
				(long) ms);
	}
	else
	{
		gsc_label_long_comma(ms);
		printf(" ms maximum\n");
	}

	return(errs);
}



//*****************************************************************************
static int _auto_calibrate(int fd)
{
	int	errs	= 0;
	s32	ms;

	gsc_label("Auto-Calibrate Period");

	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_AUTO_CAL_MS, &ms);

	if (errs)
	{
	}
	else if (ms < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. ms response error: %ld)\n",
				__LINE__,
				(long) ms);
	}
	else if (ms == 0)
	{
		printf("Auto-Calibration not supported\n");
	}
	else
	{
		gsc_label_long_comma(ms);
		printf(" ms maximum\n");
	}

	return(errs);
}



//*****************************************************************************
static int _id_board_bcfgr(int fd)
{
	gsc_reg_def_t			list[2]	= { { NULL }, { NULL } };
	const gsc_reg_def_t*	ptr;

	int	errs	= 0;

	printf("\n");
	ptr	= dsi16wrc_reg_get_def_id(DSI16WRC_GSC_BCFGR);

	if (ptr)
	{
		list[0]	= ptr[0];
		errs	= gsc_reg_list(fd, list, 1, dsi16wrc_reg_read);
	}
	else
	{
		errs++;
		printf(	"FAIL <---  (%d. dsi16wrc_reg_get_def_id, DSI16WRC_GSC_BCFGR)\n",
				__LINE__);
	}

	return(errs);
}



//*****************************************************************************
static int _register_map(int fd)
{
	int	errs;

	printf("\n");
	errs	= dsi16wrc_reg_list(fd, 0);
	return(errs);
}




/******************************************************************************
*
*	Function:	id_device
*
*		Identify the device and its features.
*
*	Arguments:
*
*		fd		The handle to the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int id_device(int fd)
{
	int	errs	= 0;

	gsc_label("Board Features");
	printf("\n");

	gsc_label_level_inc();

	errs	+= _input_modes(fd);
	errs	+= _voltage_ranges(fd);
	errs	+= _channel_qty(fd);
	errs	+= _channel_max(fd);
	errs	+= _fref_default(fd);
	errs	+= _fgen_range(fd);
	errs	+= _nvco_range(fd);
	errs	+= _nref_range(fd);
	errs	+= _ndiv_range(fd);
	errs	+= _fsamp_range(fd);
	errs	+= _filter_freq(fd);
	errs	+= _fifo_size(fd);
	errs	+= _initialize(fd);
	errs	+= _auto_calibrate(fd);

	gsc_label_level_dec();

	errs	+= _id_board_bcfgr(fd);
	errs	+= _register_map(fd);

	return(errs);
}


