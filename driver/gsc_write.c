// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/gsc_write.c $
// $Rev: 31783 $
// $Date: 2015-06-09 10:42:50 -0500 (Tue, 09 Jun 2015) $

#include "main.h"



// #defines *******************************************************************

#if defined(DEV_SUPPORTS_WRITE)

#define	_IO_ABORT	GSC_WAIT_IO_TX_ABORT
#define	_IO_DONE	GSC_WAIT_IO_TX_DONE
#define	_IO_ERROR	GSC_WAIT_IO_TX_ERROR
#define	_IO_TIMEOUT	GSC_WAIT_IO_TX_TIMEOUT

#endif



/******************************************************************************
*
*	Function:	_write_work
*
*	Purpose:
*
*		Implement the mode independent working portion of the write()
*		procedure. If a timeout is permitted and called for, we wait a single
*		timer tick and check again.
*
*	Arguments:
*
*		alt		The device data structure.
*
*		usr_buf	The bytes to write come from here.
*
*		count	The number of bytes requested. This is positive.
*
*		st_end	Timeout at this point in time (in system ticks).
*
*		fn_aval	This is the mode specific function that determines the number
*				of bytes that may be written.
*
*		fn_work	This is the function implementing the mode specific write
*				functionality.
*
*	Returned:
*
*		>= 0	The number of bytes transferred.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

#if defined(DEV_SUPPORTS_WRITE)
static long _write_work(
	GSC_ALT_STRUCT_T*	alt,
	const char*			usr_buf,
	size_t				count,	// bytes
	os_time_tick_t		st_end,	// The "system tick" time that we timeout.
	long				(*fn_aval)(
							GSC_ALT_STRUCT_T*	alt,
							size_t				count),	// bytes
	long				(*fn_work)(
							GSC_ALT_STRUCT_T*	alt,
							const char*			buf,
							size_t				count,	// bytes
							os_time_tick_t		st_end))
{
	dev_io_t*	io			= &alt->tx;
	int			ret;
	long		total		= 0;	// The number of bytes transferred.
	long		want		= 0;	// Bytes we want to write this time.
	char*		xfer_buf	= io->mem.ptr;
	long		xfer_did;
	long		xfer_do;

	for (;;)
	{
		if (want <= 0)
		{
			// Transfer one block at a time.

			if (count > io->mem.bytes)
				want	= io->mem.bytes;
			else
				want	= count;

			xfer_buf	= io->mem.ptr;
			ret			= os_mem_copy_from_user(xfer_buf, usr_buf, want);

			if (ret)
			{
				// We couldn't get user data.
				total	= -EFAULT;
				gsc_wait_resume_io(alt, _IO_DONE | _IO_ERROR);
				break;
			}

			usr_buf	+= want;
		}

		// See how much data we can transfer.
		xfer_do	= (fn_aval)(alt, count);

		if (xfer_do > (long) io->mem.bytes)
			xfer_do	= io->mem.bytes;	// The size of the transfer buffer.

		if (xfer_do > want)
			xfer_do	= want;	// Try to complete the request.

		// Either transfer the data or see what to do next.

		if (io->abort)
		{
			// We've been told to quit.
			io->abort	= 0;
			gsc_wait_resume_io(alt, _IO_DONE | _IO_ABORT);
			break;
		}

		if (xfer_do > 0)
			xfer_did	= (fn_work)(alt, xfer_buf, xfer_do, st_end);
		else
			xfer_did	= 0;

		if (xfer_did < 0)
		{
			// There was a problem.
			total	= xfer_did;
			gsc_wait_resume_io(alt, _IO_DONE | _IO_ERROR);
			break;
		}
		else if (xfer_did > 0)
		{
			// House keeping.
			count		-= xfer_did;
			total		+= xfer_did;
			want		-= xfer_did;
			xfer_buf	+= xfer_did;
		}

		// Now what do we do?

		if (count == 0)
		{
			// The request has been fulfilled.
			gsc_wait_resume_io(alt, _IO_DONE);
			break;
		}
		else if (io->non_blocking)
		{
			// We can't block.

			if (xfer_did > 0)
			{
				// See if we can transfer more data.
				continue;
			}
			else
			{
				// We can't wait to transfer more data.
				// We have essentially timed out.
				gsc_wait_resume_io(alt, _IO_DONE | _IO_TIMEOUT);
				break;
			}
		}
		else if (os_time_tick_timedout(st_end))
		{
			// We've timed out.
			gsc_wait_resume_io(alt, _IO_DONE | _IO_TIMEOUT);
			break;
		}
		else if (xfer_did)
		{
			// Some data was transferred, so go back for more.
			continue;
		}

		if (io->abort)
		{
			// We've been told to quit.
			io->abort	= 0;
			gsc_wait_resume_io(alt, _IO_DONE | _IO_ABORT);
			break;
		}

		// Wait for 1 timer tick before checking again.
		os_time_tick_sleep(1);
	}

	return(total);
}
#endif



/******************************************************************************
*
*	Function:	gsc_write
*
*	Purpose:
*
*		Implement the write() procedure. Only one call active at a time.
*
*	Arguments:
*
*		alt		The structure for the device to write to.
*
*		buf		The data requested goes here.
*
*		count	The number of bytes requested.
*
*	Returned:
*
*		>= 0	The number of bytes transferred.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

#if defined(DEV_SUPPORTS_WRITE)
long gsc_write(GSC_ALT_STRUCT_T* alt, const void* buf, size_t count)
{
	dev_io_t*		io		= &alt->tx;
	long			ret;
	size_t			samples;
	os_time_tick_t	st_end;	// The "system tick" time that we timeout.

	for (;;)	// We'll use a loop for convenience.
	{
		// Gain exclusive access to the device structure.
		ret	= os_sem_lock(&alt->sem);

		if (ret)
		{
			// We didn't get the lock.
			break;
		}

		// Perform argument validation.

		if (count <= 0)
		{
			// There is no work to do.
			ret	= 0;
			gsc_wait_resume_io(alt, _IO_DONE);
			os_sem_unlock(&alt->sem);
			break;
		}

		if (io->bytes_per_sample == 4)
		{
		}
		else if (io->bytes_per_sample == 2)
		{
		}
		else if (io->bytes_per_sample == 1)
		{
		}
		else
		{
			// This is an internal error.
			ret	= -EINVAL;
			gsc_wait_resume_io(alt, _IO_DONE | _IO_ERROR);
			os_sem_unlock(&alt->sem);
			break;
		}

		if (count % io->bytes_per_sample)
		{
			// Requests must be in sample size increments.
			ret	= -EINVAL;
			gsc_wait_resume_io(alt, _IO_DONE | _IO_ERROR);
			os_sem_unlock(&alt->sem);
			break;
		}

		if (buf == NULL)
		{
			// No buffer provided.
			ret	= -EINVAL;
			gsc_wait_resume_io(alt, _IO_DONE | _IO_ERROR);
			os_sem_unlock(&alt->sem);
			break;
		}

		// Compute the I/O timeout end.
		st_end	= os_time_tick_get() + io->timeout_s * os_time_tick_rate();

		// Transfer access control to the read lock.
		ret	= os_sem_lock(&io->sem);

		if (ret)
		{
			// We didn't get the lock.
			gsc_wait_resume_io(alt, _IO_DONE | _IO_ERROR);
			os_sem_unlock(&alt->sem);
			break;
		}

		os_sem_unlock(&alt->sem);
		ret	= dev_write_startup(alt);

		if (ret)
		{
			// There was a problem.
			gsc_wait_resume_io(alt, _IO_DONE | _IO_ERROR);
			os_sem_unlock(&io->sem);
			break;
		}

		// Perform the operation.
		samples	= count / io->bytes_per_sample;

		if ((samples <= (size_t) io->pio_threshold) ||
			(io->io_mode == GSC_IO_MODE_PIO))
		{
			ret	= _write_work(	alt,
								buf,
								count,	// bytes
								st_end,
								DEV_PIO_WRITE_AVAILABLE,
								DEV_PIO_WRITE_WORK);
		}
		else if (io->io_mode == GSC_IO_MODE_DMA)
		{
			ret	= _write_work(	alt,
								buf,
								count,	// bytes
								st_end,
								DEV_DMA_WRITE_AVAILABLE,
								DEV_DMA_WRITE_WORK);
		}
		else if (io->io_mode == GSC_IO_MODE_DMDMA)
		{
			ret	= _write_work(	alt,
								buf,
								count,	// bytes
								st_end,
								DEV_DMDMA_WRITE_AVAILABLE,
								DEV_DMDMA_WRITE_WORK);
		}
		else
		{
			ret	= -EINVAL;
			gsc_wait_resume_io(alt, _IO_DONE | _IO_ERROR);
		}

		//	Clean up.

		os_sem_unlock(&io->sem);
		break;
	}

	return(ret);
}
#endif



/******************************************************************************
*
*	Function:	gsc_write_abort_active_xfer
*
*	Purpose:
*
*		Abort an active write.
*
*	Arguments:
*
*		alt		The data structure to access.
*
*	Returned:
*
*		1		An active write was aborted.
*		0		A write was not in progress.
*
******************************************************************************/

#if defined(DEV_SUPPORTS_WRITE)
int gsc_write_abort_active_xfer(GSC_ALT_STRUCT_T* alt)
{

	dev_data_t*	dev	= GSC_ALT_DEV_GET(alt);
	int			i;
	int			ret;

	alt->tx.abort	= 1;
	ret				= gsc_dma_abort_active_xfer(dev, &alt->tx);
	i				= os_sem_lock(&alt->tx.sem);
	ret				= ret ? ret : (alt->tx.abort ? 0 : 1);
	alt->tx.abort	= 0;

	if (i == 0)
		os_sem_unlock(&alt->tx.sem);

	return(ret);
}
#endif



/******************************************************************************
*
*	Function:	gsc_write_pio_work_8_bit
*
*	Purpose:
*
*		Perform PIO based writes of 8-bit values.
*
*	Arguments:
*
*		alt		The data structure to access.
*
*		buff	The source for the data to write.
*
*		count	The number of bytes to write.
*
*		st_end	The "system ticks" time at which we timeout.
*
*	Returned:
*
*		>= 0	The number of bytes transferred.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

#if defined(DEV_SUPPORTS_WRITE)
#if defined(GSC_WRITE_PIO_WORK) || defined(GSC_WRITE_PIO_WORK_8_BIT)
long gsc_write_pio_work_8_bit(
	GSC_ALT_STRUCT_T*	alt,
	const char*			buff,
	size_t				count,
	os_time_tick_t		st_end)
{
	long		qty	= count;
	VADDR_T		dst	= alt->tx.io_reg_vaddr;
	u8*			src	= (u8*) buff;

	for (; count > 0; )
	{
		count	-= 1;
		os_reg_mem_tx_u8(NULL, dst, src[0]);
		src++;

		if (alt->tx.timeout_s)
		{
			if (os_time_tick_timedout(st_end))
			{
				// We've timed out.
				break;
			}
		}
	}

	qty	-= count;
	return(qty);
}
#endif
#endif



/******************************************************************************
*
*	Function:	gsc_write_pio_work_16_bit
*
*	Purpose:
*
*		Perform PIO based writes of 16-bit values.
*
*	Arguments:
*
*		alt		The data structure to access.
*
*		buff	The source for the data to write.
*
*		count	The number of bytes to write.
*
*		st_end	The "system tick" time at which we timeout.
*
*	Returned:
*
*		>= 0	The number of bytes transferred.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

#if defined(DEV_SUPPORTS_WRITE)
#if defined(GSC_WRITE_PIO_WORK) || defined(GSC_WRITE_PIO_WORK_16_BIT)
long gsc_write_pio_work_16_bit(
	GSC_ALT_STRUCT_T*	alt,
	const char*			buff,
	size_t				count,
	os_time_tick_t		st_end)
{
	long		qty	= count;
	VADDR_T		dst	= alt->tx.io_reg_vaddr;
	u16*		src	= (u16*) buff;

	for (; count > 0; )
	{
		count	-= 2;
		os_reg_mem_tx_u16(NULL, dst, src[0]);
		src++;

		if (alt->tx.timeout_s)
		{
			if (os_time_tick_timedout(st_end))
			{
				// We've timed out.
				break;
			}
		}
	}

	qty	-= count;
	return(qty);
}
#endif
#endif



/******************************************************************************
*
*	Function:	gsc_write_pio_work_32_bit
*
*	Purpose:
*
*		Perform PIO based writes of 32-bit values.
*
*	Arguments:
*
*		alt		The data structure to access.
*
*		buff	The source for the data to write.
*
*		count	The number of bytes to write.
*
*		st_end	The "system tick" time at which we timeout.
*
*	Returned:
*
*		>= 0	The number of bytes transferred.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

#if defined(DEV_SUPPORTS_WRITE)
#if defined(GSC_WRITE_PIO_WORK) || defined(GSC_WRITE_PIO_WORK_32_BIT)
long gsc_write_pio_work_32_bit(
	GSC_ALT_STRUCT_T*	alt,
	const char*			buff,
	size_t				count,
	os_time_tick_t		st_end)
{
	long		qty	= count;
	VADDR_T		dst	= alt->tx.io_reg_vaddr;
	u32*		src	= (u32*) buff;

	for (; count > 0; )
	{
		count	-= 4;
		os_reg_mem_tx_u32(NULL, dst, src[0]);
		src++;

		if (alt->tx.timeout_s)
		{
			if (os_time_tick_timedout(st_end))
			{
				// We've timed out.
				break;
			}
		}
	}

	qty	-= count;
	return(qty);
}
#endif
#endif



/******************************************************************************
*
*	Function:	gsc_write_pio_work
*
*	Purpose:
*
*		Perform PIO based writes of 32, 16 or 8 bit values.
*
*	Arguments:
*
*		alt		The data structure to access.
*
*		buff	The source for the data to write.
*
*		count	The number of bytes to write.
*
*		st_end	The "system tick" time at which we timeout.
*
*	Returned:
*
*		>= 0	The number of bytes transferred.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

#if defined(DEV_SUPPORTS_WRITE)
#if defined(GSC_WRITE_PIO_WORK)
long gsc_write_pio_work(
	GSC_ALT_STRUCT_T*	alt,
	const char*			buff,
	size_t				count,
	os_time_tick_t		st_end)
{
	long	qty;

	if (alt->tx.bytes_per_sample == 4)
		qty	= gsc_write_pio_work_32_bit(alt, buff, count, st_end);
	else if (alt->tx.bytes_per_sample == 2)
		qty	= gsc_write_pio_work_16_bit(alt, buff, count, st_end);
	else if (alt->tx.bytes_per_sample == 1)
		qty	= gsc_write_pio_work_8_bit(alt, buff, count, st_end);
	else
		qty	= -EINVAL;

	return(qty);
}
#endif
#endif


