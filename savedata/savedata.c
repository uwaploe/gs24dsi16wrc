// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/savedata/savedata.c $
// $Rev: 32584 $
// $Date: 2015-07-04 17:28:47 -0500 (Sat, 04 Jul 2015) $

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "main.h"



// #defines	*******************************************************************

#define	_1M		(1024L * 1024L)



// variables	***************************************************************

static	u32	_buffer[_1M];



/******************************************************************************
*
*	Function:	_channels
*
*	Purpose:
*
*		Check to see how many channels the board has.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		chan	Report the number of channels here.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

static int _channels(int fd, s32* chans)
{
	int	errs;

	gsc_label("Input Channels");
	errs	= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_CHANNEL_QTY, chans);

	if (errs == 0)
		printf("%ld Channels\n", (long) chans[0]);

	return(errs);
}



/******************************************************************************
*
*	Function:	_read_data
*
*	Purpose:
*
*		Read data into the buffer.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		errs	The number of errors encountered so far?
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

static int _read_data(int fd, int errs)
{
	long	get		= sizeof(_buffer) / 4;
	int		got;

	gsc_label("Reading");

	if (errs)
	{
		errs	= 0;
		printf("SKIPPED  (due to errors)\n");
	}
	else
	{
		got	= dsi16wrc_dsl_read(fd, _buffer, get);

		if (got < 0)
		{
			errs	= 1;
		}
		else if (got != get)
		{
			errs	= 1;
			printf(	"FAIL <---  (got %ld samples, requested %ld)\n",
					(long) got,
					(long) get);
		}
		else
		{
			errs	= 0;
			printf(	"PASS  (%ld samples)\n",
					(long) get);
		}
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	_save_data
*
*	Purpose:
*
*		Save the read data to a text file.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		chan	The number of channels.
*
*		errs	have there been any errors so far?
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

static int _save_data(int fd, int chan, int errs)
{
	FILE*		file;
	int			i;
	long		l;
	const char*	name	= "data.txt";
	long		samples	= sizeof(_buffer) / 4;

	gsc_label("Saving");

	for (;;)
	{
		if (errs)
		{
			printf("SKIPPED  (due to errors)\n");
			errs	= 0;
			break;
		}

		file	= fopen(name, "w+b");

		if (file == NULL)
		{
			printf("FAIL <---  (unable to create %s)\n", name);
			errs	= 1;
			break;
		}

		for (l = 0; l < samples; l++)
		{
			i	= fprintf(file, "  %08lX", (long) _buffer[l]);

			if (i != 10)
			{
				printf("FAIL <---  (fprintf() failure to %s)\n", name);
				errs	= 1;
				break;
			}

			if ((l % chan) == (chan - 1))
			{
				i	= fprintf(file, "\r\n");

				if (i != 2)
				{
					printf("FAIL <---  (fprintf() failure to %s)\n", name);;
					errs	= 1;
					break;
				}
			}
		}

		fclose(file);

		if (errs == 0)
			printf("PASS  (%s)\n", name);

		break;
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	save_data
*
*	Purpose:
*
*		Configure the board, then capture data to a file.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		width	The desired data width.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int save_data(int fd, s32 width)
{
	s32	chans	= 32;
	int	errs	= 0;

	gsc_label("Capture & Save");
	printf("\n");
	gsc_label_level_inc();

	errs	+= _channels(fd, &chans);
	errs	+= dsi16wrc_config_board(fd, -1, -1, 10000);
	errs	+= dsi16wrc_data_width(fd, -1, width, NULL);
	errs	+= dsi16wrc_ai_buf_clear_at_boundary(fd, -1);
	errs	+= _read_data(fd, errs);
	errs	+= dsi16wrc_ai_buf_overflow(fd, -1, DSI16WRC_AI_BUF_OVERFLOW_TEST, NULL);
	errs	+= dsi16wrc_ai_buf_underflow(fd, -1, DSI16WRC_AI_BUF_UNDERFLOW_TEST, NULL);
	errs	+= _save_data(fd, chans, errs);

	gsc_label_level_dec();
	return(errs);
}



