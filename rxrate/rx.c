// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/rxrate/rx.c $
// $Rev: 32930 $
// $Date: 2015-09-15 18:01:15 -0500 (Tue, 15 Sep 2015) $

#include <errno.h>
#include <stdio.h>
#include <sys/time.h>

#include "main.h"



// #defines *******************************************************************

#define	_1M	(1024L * 1024L)



// variables ******************************************************************

static u32	_rx[_1M];



/******************************************************************************
*
*	Function:	_rate_test
*
*	Purpose:
*
*		Perform for the read rate test.
*
*	Arguments:
*
*		fd		The handle to the board.
*
*		seconds	The minimum number of seconds to operate.
*
*		mb		The niminum number of mega-bytes to read.
*
*		get		The number of bytes to request with each read call.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

static int _rate_test(int fd, int seconds, long mb, long get)
{
	struct timeval	begin;
	int				errs	= 0;
	long			got;
	long long		limit	= (long long) mb * 1000000L;
	struct timeval	minimum;
	struct timeval	now;
	long long		sps;
	long long		total	= 0;
	long			us		= 1000000L * seconds;

	gsc_label("Reading");

	gettimeofday(&begin, NULL);
	minimum.tv_sec	= begin.tv_sec + seconds;
	minimum.tv_usec	= begin.tv_usec;

	for (;;)
	{
		got	= read(fd, _rx, get);

		if (got < 0)
		{
			errs++;
			printf("FAIL <--- (read");
			printf(", total so far %lld", total);
			printf(", requested %ld bytes", (long) get);
			printf(", errno %d)\n", errno);
			break;
		}

		gettimeofday(&now, NULL);
		total	+= got;

		if ((total >= limit) && (now.tv_sec > minimum.tv_sec))
		{
			us	= (now.tv_sec - begin.tv_sec) * 1000000L
				+ (now.tv_usec - begin.tv_usec);
			sps	= (total * 10000000L / 4 / us + 5) / 10;

			printf(	"PASS  (");
			gsc_label_long_comma(total);
			printf(" Bytes, %ld.%06ld Seconds, ", us / 1000000, us % 1000000);
			gsc_label_long_comma(sps);
			printf(" S/S)\n");
			break;
		}
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	rx_rate_test
*
*	Purpose:
*
*		Perform an Rx Rate test - measure the read throughput.
*
*	Arguments:
*
*		fd		The handle to the board.
*
*		mb		The niminum number of mega-bytes to read.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int rx_rate_test(int fd, long mb)
{
	int		errs	= 0;
	long	get		= sizeof(_rx);
	int		seconds	= 5;

	gsc_label ("Read Rate");
	printf("(%d second minimum, %ld MB minimum)\n", seconds, mb);
	gsc_label_level_inc();

	errs	+= dsi16wrc_ai_buf_clear	(fd, -1);
	errs	+= dsi16wrc_ai_buf_overflow	(fd, -1, DSI16WRC_AI_BUF_OVERFLOW_CLEAR, NULL);
	errs	+= dsi16wrc_ai_buf_underflow(fd, -1, DSI16WRC_AI_BUF_UNDERFLOW_CLEAR, NULL);

	errs	+= _rate_test(fd, seconds, mb, get);

	errs	+= dsi16wrc_ai_buf_overflow	(fd, -1, -1, NULL);
	errs	+= dsi16wrc_ai_buf_underflow(fd, -1, -1, NULL);

	gsc_label_level_dec();
	return(errs);
}



