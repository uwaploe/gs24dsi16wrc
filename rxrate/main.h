// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/rxrate/main.h $
// $Rev: 32930 $
// $Date: 2015-09-15 18:01:15 -0500 (Tue, 15 Sep 2015) $

#ifndef __MAIN_H__
#define __MAIN_H__

#include "24dsi16wrc.h"
#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"
#include "gsc_utils.h"



// #defines	*******************************************************************

#define	VERSION			"1.4"
// 1.4	Updated to use the newer common Linux driver sources.
//		Updated the status results displayed.
//		Renamed one or more variables.
//		Added an I/I Mode command line argument.
// 1.3	Additional LINTing.
// 1.2	Updated the parameter list to gsc_id_driver().
// 1.1	Split the utility code into two libraries: common and device specific.
// 1.0	initial release.



// prototypes	***************************************************************


int	rx_rate_test(int fd, long mb);



#endif
