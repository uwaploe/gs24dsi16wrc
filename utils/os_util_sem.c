// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/utils/linux/os_util_sem.c $
// $Rev: 32769 $
// $Date: 2015-09-07 15:20:51 -0500 (Mon, 07 Sep 2015) $

#include <errno.h>
#include <fcntl.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/time.h>

#include "os_utils.h"



//*****************************************************************************
#ifndef CLOCK_REALTIME
#define clock_gettime(c,ts)	_my_clock_gettime(ts)

static int _my_clock_gettime(struct timespec* ts)
{
	int				ret;
	struct timeval	tv;

	if (ts)
	{
		ret	= 0;
		gettimeofday(&tv, NULL);
		ts->tv_sec	= tv.tv_sec;
		ts->tv_nsec	= tv.tv_usec * 1000;
	}
	else
	{
		ret	= 1;
	}

	return(ret);
}
#endif



//*****************************************************************************
int os_sem_create(os_sem_t* sem)
{
	int	ret;

	ret	= os_sem_create_qty(sem, 1, 1);
	return(ret);
}



//*****************************************************************************
int os_sem_create_qty(os_sem_t* sem, int cap, int put)
{
	int	ret;

	if ((sem) && (cap > 0) && (put >= 0) && (put <= cap))
	{
		memset(sem, 0, sizeof(os_sem_t));
		ret	= sem_init(&sem->sem, 0, put);

		if (ret)
			ret	= -errno;
		else
			sem->key	= (void*) sem;
	}
	else
	{
		ret	= -EINVAL;
	}

	return(ret);
}



//*****************************************************************************
int os_sem_destroy(os_sem_t* sem)
{
	int	ret	= 0;

	if (sem)
	{
		if (sem->key == sem)
		{
			os_sem_unlock(sem);
			ret	= sem_destroy(&sem->sem);

			if (ret)
				ret	= -errno;
		}

		memset(sem, 0, sizeof(os_sem_t));
	}
	else
	{
		ret	= -EINVAL;
	}

	return(ret);
}



//*****************************************************************************
int os_sem_lock(os_sem_t* sem)
{
	int	ret;

	if ((sem) && (sem->key == sem))
	{
		ret	= sem_wait(&sem->sem);

		if (ret)
			ret	= -errno;
	}
	else
	{
		ret	= -EINVAL;
	}

	return(ret);
}



//*****************************************************************************
int os_sem_lock_to(os_sem_t* sem, int timeout_ms)
{
	int				ret;
	struct timespec	ts;

	for (;;)
	{
		if ((sem == NULL) || (sem->key != sem))
		{
			ret	= (int) OS_SEM_GET_ER;
			break;
		}

		if (timeout_ms < 1)
		{
			ret	= (int) OS_SEM_GET_ER;
			break;
		}

		ret	= clock_gettime(CLOCK_REALTIME, &ts);

		if (ret)
		{
			ret	= (int) OS_SEM_GET_ER;
			break;
		}

		ts.tv_sec	+= (timeout_ms / 1000);
		ts.tv_nsec	+= (timeout_ms % 1000) * 1000;

		if (ts.tv_nsec >= 1000000000L)
		{
			ts.tv_sec	+= (ts.tv_nsec / 1000000000L);
			ts.tv_nsec	= (ts.tv_nsec % 1000000000L);
		}

		ret	= sem_timedwait(&sem->sem, &ts);

		if (ret == 0)
			ret	= (int) OS_SEM_GET_OK;
		else if (errno == ETIMEDOUT)
			ret	= (int) OS_SEM_GET_TO;
		else
			ret	= (int) OS_SEM_GET_ER;

		break;
	}

	return(ret);
}



//*****************************************************************************
int os_sem_unlock(os_sem_t* sem)
{
	int	ret;

	if ((sem) && (sem->key == sem))
	{
		ret	= sem_post(&sem->sem);

		if (ret)
			ret	= -errno;
	}
	else
	{
		ret	= -EINVAL;
	}

	return(ret);
}


