// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/utils/util_master_clk_adj.c $
// $Rev: 32102 $
// $Date: 2015-06-16 16:29:33 -0500 (Tue, 16 Jun 2015) $

#include <stdio.h>

#include "24dsi16wrc.h"
#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi16wrc_master_clk_adj
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI16WRC_IOCTL_MASTER_CLK_ADJ service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		set		This is the value to apply.
*
*		get		The results are reported here. This may be NULL.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi16wrc_master_clk_adj(int fd, int index, s32 set, s32* get)
{
	int	errs;

	gsc_label_index("Master Clock Adjust", index);
	errs	= dsi16wrc_dsl_ioctl(fd, DSI16WRC_IOCTL_MASTER_CLK_ADJ, &set);
	printf("%s  (0x%lX)\n", errs ? "FAIL <---" : "PASS", (long) get);

	if (get)
		get[0]	= set;

	return(errs);
}



//*****************************************************************************
int dsi16wrc_master_clk_adj_quiet(int fd, s32 set, s32* get)
{
	int	errs;

	errs	= dsi16wrc_dsl_ioctl(fd, DSI16WRC_IOCTL_MASTER_CLK_ADJ, &set);

	if (get)
		get[0]	= set;

	return(errs);
}


