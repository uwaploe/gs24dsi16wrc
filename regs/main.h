// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/regs/main.h $
// $Rev: 32548 $
// $Date: 2015-07-04 15:46:22 -0500 (Sat, 04 Jul 2015) $

#ifndef __MAIN_H__
#define __MAIN_H__

#include "24dsi16wrc.h"
#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"
#include "gsc_utils.h"



/* #defines	***************************************************************/

#define	VERSION			"1.3"
// 1.3	Updated to use the newer common Linux sources.
// 1.2	Updated the parameter list to gsc_id_driver().
// 1.1	Split the utility code into two libraries: common and device specific.
// 1.0	initial release.



/* data types	**************************************************************/

typedef struct
{
	const char*	name;				// NULL terminates list.
	void		(*func)(int fd);	// NULL terminates list.
} menu_item_t;

typedef struct
{
	const char*			title;
	const menu_item_t*	list;
} menu_t;



/* prototypes	**************************************************************/

void	main_menu(int fd);
void	menu_call(int fd, const menu_t* menu);
int		menu_select(const menu_t* menu);
void	reg_mod_by_name(int fd);
void	reg_mod_by_offset(int fd);



#endif
