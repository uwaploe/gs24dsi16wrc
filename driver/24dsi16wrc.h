// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/driver/24dsi16wrc.h $
// $Rev: 32413 $
// $Date: 2015-07-04 13:57:13 -0500 (Sat, 04 Jul 2015) $

#ifndef __24DSI16WRC_H__
#define __24DSI16WRC_H__

#include "gsc_common.h"
#include "gsc_pci9056.h"



// #defines	*******************************************************************

#define	DSI16WRC_BASE_NAME					"24dsi16wrc"

// IOCTL command codes
#define	DSI16WRC_IOCTL_REG_READ				OS_IOCTL_RW( 0, 12, gsc_reg_t)
#define	DSI16WRC_IOCTL_REG_WRITE			OS_IOCTL_RW( 1, 12, gsc_reg_t)
#define	DSI16WRC_IOCTL_REG_MOD				OS_IOCTL_RW( 2, 12, gsc_reg_t)
#define DSI16WRC_IOCTL_QUERY				OS_IOCTL_RW( 3,  4, s32)
#define DSI16WRC_IOCTL_INITIALIZE			OS_IOCTL   ( 4)
#define DSI16WRC_IOCTL_AUTO_CALIBRATE		OS_IOCTL   ( 5)
#define DSI16WRC_IOCTL_AUTO_CAL_STS			OS_IOCTL_R ( 6,  4, s32)
#define DSI16WRC_IOCTL_ADC_MODE				OS_IOCTL_RW( 7,  4, s32)
#define DSI16WRC_IOCTL_AI_BUF_CLEAR			OS_IOCTL   ( 8)
#define DSI16WRC_IOCTL_AI_BUF_ENABLE		OS_IOCTL_RW( 9,  4, s32)
#define DSI16WRC_IOCTL_AI_BUF_FILL_LVL		OS_IOCTL_R (10,  4, s32)
#define DSI16WRC_IOCTL_AI_BUF_OVERFLOW		OS_IOCTL_RW(11,  4, s32)
#define DSI16WRC_IOCTL_AI_BUF_THRESH		OS_IOCTL_RW(12,  4, s32)
#define DSI16WRC_IOCTL_AI_BUF_THR_STS		OS_IOCTL_R (13,  4, s32)
#define DSI16WRC_IOCTL_AI_BUF_UNDERFLOW		OS_IOCTL_RW(14,  4, s32)
#define DSI16WRC_IOCTL_AI_CHANNEL_TAG		OS_IOCTL_RW(15,  4, s32)
#define DSI16WRC_IOCTL_AI_MODE				OS_IOCTL_RW(16,  4, s32)
#define DSI16WRC_IOCTL_AUX_CLK_CTL_MODE		OS_IOCTL_RW(17,  4, s32)
#define DSI16WRC_IOCTL_AUX_SYNC_CTL_MODE	OS_IOCTL_RW(18,  4, s32)
#define DSI16WRC_IOCTL_BURST				OS_IOCTL_RW(19,  4, s32)
#define DSI16WRC_IOCTL_BURST_RATE_DIV		OS_IOCTL_RW(20,  4, s32)
#define DSI16WRC_IOCTL_BURST_SIZE			OS_IOCTL_RW(21,  4, s32)
#define DSI16WRC_IOCTL_BURST_TIMER			OS_IOCTL_RW(22,  4, s32)
#define DSI16WRC_IOCTL_BURST_TRIGGER		OS_IOCTL   (23)
#define DSI16WRC_IOCTL_CH_GRP_0_SRC			OS_IOCTL_RW(24,  4, s32)
#define DSI16WRC_IOCTL_CH_GRP_1_SRC			OS_IOCTL_RW(25,  4, s32)
#define DSI16WRC_IOCTL_CHANNELS_READY		OS_IOCTL_RW(26,  4, s32)
#define DSI16WRC_IOCTL_CONTROL_MODE			OS_IOCTL_RW(27,  4, s32)
#define DSI16WRC_IOCTL_DATA_FORMAT			OS_IOCTL_RW(28,  4, s32)
#define DSI16WRC_IOCTL_DATA_WIDTH			OS_IOCTL_RW(29,  4, s32)
#define DSI16WRC_IOCTL_DIO_DIR_OUT			OS_IOCTL_RW(30,  4, s32)
#define DSI16WRC_IOCTL_DIO_READ				OS_IOCTL_R (31,  4, s32)
#define DSI16WRC_IOCTL_DIO_WRITE			OS_IOCTL_RW(32,  4, s32)
#define DSI16WRC_IOCTL_EXT_CLK_SRC			OS_IOCTL_RW(33,  4, s32)
#define DSI16WRC_IOCTL_IRQ_SEL				OS_IOCTL_RW(34,  4, s32)
#define DSI16WRC_IOCTL_MASTER_CLK_ADJ		OS_IOCTL_RW(35,  4, s32)
#define DSI16WRC_IOCTL_NDIV					OS_IOCTL_RW(36,  4, s32)
#define DSI16WRC_IOCTL_NREF					OS_IOCTL_RW(37,  4, s32)
#define DSI16WRC_IOCTL_NVCO					OS_IOCTL_RW(38,  4, s32)
#define DSI16WRC_IOCTL_RANGE				OS_IOCTL_RW(39,  4, s32)
#define DSI16WRC_IOCTL_RX_IO_ABORT			OS_IOCTL_R (40,  4, s32)
#define DSI16WRC_IOCTL_RX_IO_MODE			OS_IOCTL_RW(41,  4, s32)
#define DSI16WRC_IOCTL_RX_IO_OVERFLOW		OS_IOCTL_RW(42,  4, s32)
#define DSI16WRC_IOCTL_RX_IO_TIMEOUT		OS_IOCTL_RW(43,  4, s32)
#define DSI16WRC_IOCTL_RX_IO_UNDERFLOW		OS_IOCTL_RW(44,  4, s32)
#define DSI16WRC_IOCTL_SW_SYNC				OS_IOCTL   (45)
#define DSI16WRC_IOCTL_SW_SYNC_MODE			OS_IOCTL_RW(46,  4, s32)
#define	DSI16WRC_IOCTL_WAIT_EVENT			OS_IOCTL_RW(47, 28, gsc_wait_t)
#define	DSI16WRC_IOCTL_WAIT_CANCEL			OS_IOCTL_RW(48, 28, gsc_wait_t)
#define	DSI16WRC_IOCTL_WAIT_STATUS			OS_IOCTL_RW(49, 28, gsc_wait_t)
#define DSI16WRC_IOCTL_XCVR_TYPE			OS_IOCTL_RW(50,  4, s32)

//*****************************************************************************
// DSI16WRC_IOCTL_REG_READ
// DSI16WRC_IOCTL_REG_WRITE
// DSI16WRC_IOCTL_REG_MOD
//
// Parameter:	gsc_reg_t*

#define DSI16WRC_GSC_BCTLR					GSC_REG_ENCODE(GSC_REG_GSC,4,0x00)	// Board Control Register
#define DSI16WRC_GSC_RCR					GSC_REG_ENCODE(GSC_REG_GSC,4,0x04)	// Rate Control Register
#define	DSI16WRC_GSC_DIOPR					GSC_REG_ENCODE(GSC_REG_GSC,4,0x08)	// Digital I/O Port Register
#define DSI16WRC_GSC_CSAR					GSC_REG_ENCODE(GSC_REG_GSC,4,0x0C)	// Clock Source Assignment Register
#define DSI16WRC_GSC_RDR					GSC_REG_ENCODE(GSC_REG_GSC,4,0x10)	// Rate Divisors Register
#define DSI16WRC_GSC_BBSR					GSC_REG_ENCODE(GSC_REG_GSC,4,0x1C)	// Burst Block Size Register
#define DSI16WRC_GSC_BUFCR					GSC_REG_ENCODE(GSC_REG_GSC,4,0x20)	// Buffer Control Register
#define DSI16WRC_GSC_BCFGR					GSC_REG_ENCODE(GSC_REG_GSC,4,0x24)	// Board Configuration Register
#define DSI16WRC_GSC_BUFSR					GSC_REG_ENCODE(GSC_REG_GSC,4,0x28)	// Buffer Size Register
#define DSI16WRC_GSC_AVR					GSC_REG_ENCODE(GSC_REG_GSC,4,0x2C)	// Auto-Cal Values Register
#define DSI16WRC_GSC_IDBR					GSC_REG_ENCODE(GSC_REG_GSC,4,0x30)	// Input Data Buffer Register
#define DSI16WRC_GSC_ASIOCR					GSC_REG_ENCODE(GSC_REG_GSC,4,0x34)	// Aux Sync I/O Control Register
#define DSI16WRC_GSC_MCAR					GSC_REG_ENCODE(GSC_REG_GSC,4,0x38)	// Master Clock Adjust Register
#define DSI16WRC_GSC_BTTR					GSC_REG_ENCODE(GSC_REG_GSC,4,0x3C)	// Burst Trigger Timer Register

//*****************************************************************************
// DSI16WRC_IOCTL_QUERY
//
//	Parameter:	s32
//		Pass in a value from the list below.
//		The value returned is the answer to the query.

typedef enum
{
	DSI16WRC_QUERY_AUTO_CAL_MS,		// Max auto-cal period in ms.
	DSI16WRC_QUERY_CHANNEL_MAX,		// Maximum number of channels supported.
	DSI16WRC_QUERY_CHANNEL_QTY,		// The number of A/D channels.
	DSI16WRC_QUERY_COUNT,			// Number of query options.
	DSI16WRC_QUERY_DEVICE_TYPE,		// Value from gsc_dev_type_t
	DSI16WRC_QUERY_FGEN_MAX,		// Rate Generator maximum output rate.
	DSI16WRC_QUERY_FGEN_MIN,		// Rate Generator minimum output rate.
	DSI16WRC_QUERY_FIFO_SIZE,		// FIFO depth in 32-bit samples
	DSI16WRC_QUERY_FILTER_FREQ,		// Image Filter Frequency
	DSI16WRC_QUERY_FREF_DEFAULT,	// The default Fref value for the board.
	DSI16WRC_QUERY_FSAMP_MAX,		// The maximum sample rate.
	DSI16WRC_QUERY_FSAMP_MIN,		// The minimum sample rate.
	DSI16WRC_QUERY_INIT_MS,			// Max initialize period in ms.
	DSI16WRC_QUERY_NDIV_MAX,		// Maximum rate divisor Ndiv value.
	DSI16WRC_QUERY_NDIV_MIN,		// Maximum rate divisor Ndiv value.
	DSI16WRC_QUERY_NREF_MAX,		// PLL: Maximum rate generator Nref value.
	DSI16WRC_QUERY_NREF_MIN,		// PLL: Maximum rate generator Nref value.
	DSI16WRC_QUERY_NVCO_MAX,		// PLL: Maximum rate generator Nvco value.
	DSI16WRC_QUERY_NVCO_MIN,		// PLL: Maximum rate generator Nvco value.
	DSI16WRC_QUERY_V_RANGE,			// 10/5/2.5/1.25 or 10/1/.1/.01

	DSI16WRC_QUERY_LAST				// This is always last. Don't query this.
} dsi16wrc_query_t;

#define	DSI16WRC_IOCTL_QUERY_ERROR			(-1)	// Invalid/unknown query.

// DSI16WRC_QUERY_V_RANGE values returned
#define	DSI16WRC_QUERY_V_RANGE_10_BY_10		10	// 10, 1, .1, .01
#define	DSI16WRC_QUERY_V_RANGE_10_BY_2		2	// 10, 5, 2.5, 1.25

// DSI16WRC_QUERY_V_RANGE values returned
// 0 = no filter
// 150000 = 150KHz filter
// -1 = unknown option

//*****************************************************************************
// DSI16WRC_IOCTL_INITIALIZE				BCTLR D15
//
//	Parameter:	None

//*****************************************************************************
// DSI16WRC_IOCTL_AUTO_CALIBRATE			BCTLR D7 (D12 for status)
//
//	Parameter:	None

//*****************************************************************************
// DSI16WRC_IOCTL_AUTO_CAL_STS				BCTLR D7, D12
//
//	Parameter:	s32
//		Pass in any of the below options, or -1 to read the current setting.

#define	DSI16WRC_AUTO_CAL_STS_ACTIVE		0
#define	DSI16WRC_AUTO_CAL_STS_FAIL			1
#define	DSI16WRC_AUTO_CAL_STS_PASS			2

//*****************************************************************************
// DSI16WRC_IOCTL_ADC_MODE					BCTLR D19
//
//	Parameter:	s32
//		Pass in any of the below options, or -1 to read the current setting.

#define	DSI16WRC_ADC_MODE_HI_RES			0
#define	DSI16WRC_ADC_MODE_HI_SPEED			1

//*****************************************************************************
// DSI16WRC_IOCTL_AI_BUF_CLEAR				BUFCR D20 (w/ D24, D25)
//
//	Parameter:	None

//*****************************************************************************
// DSI16WRC_IOCTL_AI_BUF_ENABLE				BUFCR D19
//
//	Parameter:	s32
//		Pass in any valid option below, or -1 to read the current setting.

#define	DSI16WRC_AI_BUF_ENABLE_NO			0
#define	DSI16WRC_AI_BUF_ENABLE_YES			1

//*****************************************************************************
// DSI16WRC_IOCTL_AI_BUF_FILL_LVL			BUFSR D0-D18
//
//	Parameter:	s32
//		Values returned are from 0 to 0x40000 (256K).

//*****************************************************************************
// DSI16WRC_IOCTL_AI_BUF_OVERFLOW			BUFCR D24
//
//	Parameter:	s32
//		Pass in any of the below options, or -1 to read the current setting.

#define	DSI16WRC_AI_BUF_OVERFLOW_CLEAR		0
#define	DSI16WRC_AI_BUF_OVERFLOW_TEST		(-1)

// The following are the set of returned values.
#define	DSI16WRC_AI_BUF_OVERFLOW_NO			0
#define	DSI16WRC_AI_BUF_OVERFLOW_YES		1

//*****************************************************************************
// DSI16WRC_IOCTL_AI_BUF_THRESH				BUFCR D0-D18
//
//	Parameter:	s32
//		Pass in any valid from 0 to 0x40000, or -1 to read the current setting.

//*****************************************************************************
// DSI16WRC_IOCTL_AI_BUF_THR_STS			BCTLR D14
//
//	Parameter:	s32
//		Pass in any of the below options, or -1 to read the current setting.

#define	DSI16WRC_AI_BUF_THR_STS_IDLE		0
#define	DSI16WRC_AI_BUF_THR_STS_ACTIVE		1

//*****************************************************************************
// DSI16WRC_IOCTL_AI_BUF_UNDERFLOW			BUFCR D25
//
//	Parameter:	s32
//		Pass in any of the below options, or -1 to read the current setting.

#define	DSI16WRC_AI_BUF_UNDERFLOW_CLEAR		0
#define	DSI16WRC_AI_BUF_UNDERFLOW_TEST		(-1)

// The following are the set of returned values.
#define	DSI16WRC_AI_BUF_UNDERFLOW_NO		0
#define	DSI16WRC_AI_BUF_UNDERFLOW_YES		1

//*****************************************************************************
// DSI16WRC_IOCTL_AI_CHANNEL_TAG			BUFCR D23
//
//	Parameter:	s32
//		Pass in any valid option below, or -1 to read the current setting.

#define	DSI16WRC_AI_CHANNEL_TAG_ENABLE		0
#define	DSI16WRC_AI_CHANNEL_TAG_DISABLE		1

//*****************************************************************************
// DSI16WRC_IOCTL_AI_MODE					BCTLR D0-D1
//
//	Parameter:	s32
//		Pass in any of the below options, or -1 to read the current setting.

#define	DSI16WRC_AI_MODE_DIFF				0	// Differential
#define	DSI16WRC_AI_MODE_ZERO				2	// Zero test
#define	DSI16WRC_AI_MODE_VREF				3	// Vref test

//*****************************************************************************
// DSI16WRC_IOCTL_AUX_CLK_CTL_MODE			ASIOCR D0-D1
//
//	Parameter:	s32
//		Pass in any valid option below, or -1 to read the current setting.

#define	DSI16WRC_AUX_CLK_CTL_MODE_INACTIVE	0
#define	DSI16WRC_AUX_CLK_CTL_MODE_INPUT		1
#define	DSI16WRC_AUX_CLK_CTL_MODE_OUTPUT	2

//*****************************************************************************
// DSI16WRC_IOCTL_AUX_SYNC_CTL_MODE			ASIOCR D2-D3
//
//	Parameter:	s32
//		Pass in any valid option below, or -1 to read the current setting.

#define	DSI16WRC_AUX_SYNC_CTL_MODE_INACTIVE	0
#define	DSI16WRC_AUX_SYNC_CTL_MODE_INPUT	1
#define	DSI16WRC_AUX_SYNC_CTL_MODE_OUTPUT	2

//*****************************************************************************
// DSI16WRC_IOCTL_BURST						BCTLR D21
//
//	Parameter:	s32
//		Pass in any valid option below, or -1 to read the current setting.

#define	DSI16WRC_BURST_DISABLE				0
#define	DSI16WRC_BURST_ENABLE				1

//*****************************************************************************
// DSI16WRC_IOCTL_BURST_RATE_DIV			BTTR D0-D23
//
//	Parameter:	s32
//		Pass in any valid from 0 to 0xFFFFFF, or -1 to read the current setting.

//*****************************************************************************
// DSI16WRC_IOCTL_BURST_SIZE				BBSR D0-D23
//
//	Parameter:	s32
//		Pass in any valid from 0 to 0xFFFFFF, or -1 to read the current setting.

//*****************************************************************************
// DSI16WRC_IOCTL_BURST_TIMER				BCTLR D16
//
//	Parameter:	s32
//		Pass in any valid option below, or -1 to read the current setting.

#define	DSI16WRC_BURST_TIMER_DISABLE		0
#define	DSI16WRC_BURST_TIMER_ENABLE			1

//*****************************************************************************
// DSI16WRC_IOCTL_BURST_TRIGGER				BCTLR D22
//
//	Parameter:	None

//*****************************************************************************
// DSI16WRC_IOCTL_CH_GRP_0_SRC				CSAR D0-D3
// DSI16WRC_IOCTL_CH_GRP_1_SRC				CSAR D4-D7
//
//	Parameter:	s32
//		Pass in any valid option below, or -1 to read the current setting.

#define	DSI16WRC_CH_GRP_SRC_RATE_GEN		0
#define	DSI16WRC_CH_GRP_SRC_EXTERN			4
#define	DSI16WRC_CH_GRP_SRC_DIR_EXTERN		5
#define	DSI16WRC_CH_GRP_SRC_DISABLE			6

//*****************************************************************************
// DSI16WRC_IOCTL_CHANNELS_READY			BCTLR D13
//
//	Parameter:	s32
//		Pass in any of the below options, or -1 to read the current setting.
#define	DSI16WRC_CHANNELS_READY_WAIT		0
#define	DSI16WRC_CHANNELS_READY_TEST		(-1)

// These are the values returned.
#define	DSI16WRC_CHANNELS_READY_NO			0
#define	DSI16WRC_CHANNELS_READY_YES			1

//*****************************************************************************
// DSI16WRC_IOCTL_CONTROL_MODE				BCTLR D5
//
//	Parameter:	s32
//		Pass in any of the below options, or -1 to read the current setting.

#define	DSI16WRC_CONTROL_MODE_TARGET		0
#define	DSI16WRC_CONTROL_MODE_INITIATOR		1

//*****************************************************************************
// DSI16WRC_IOCTL_DATA_FORMAT				BCTLR D4
//
//	Parameter:	s32
//		Pass in any of the below options, or -1 to read the current setting.

#define	DSI16WRC_DATA_FORMAT_2S_COMP		0	// Twos Compliment
#define	DSI16WRC_DATA_FORMAT_OFF_BIN		1	// Offset Binary

//*****************************************************************************
// DSI16WRC_IOCTL_DATA_WIDTH				BUFCR D21-D22
//
//	Parameter:	s32
//		Pass in any of the below options, or -1 to read the current setting.

#define	DSI16WRC_DATA_WIDTH_16				0
#define	DSI16WRC_DATA_WIDTH_18				1
#define	DSI16WRC_DATA_WIDTH_20				2
#define	DSI16WRC_DATA_WIDTH_24				3

//*****************************************************************************
// DSI16WRC_IOCTL_DIO_DIR_OUT				DIOPR D8-D11
//
//	Parameter:	s32
//		Pass in any valid from 0 to 0xF, or -1 to read the current setting.

//*****************************************************************************
// DSI16WRC_IOCTL_DIO_READ					DIOPR D0-D3
//
//	Parameter:	s32
//		Values returned are from 0 to 0xF.

//*****************************************************************************
// DSI16WRC_IOCTL_DIO_WRITE					DIOPR D0-D3
//
//	Parameter:	s32
//		Pass in any valid from 0 to 0xF, or -1 to read the current setting.

//*****************************************************************************
// DSI16WRC_IOCTL_EXT_CLK_SRC				BCTLR D18
//
//	Parameter:	s32
//		Pass in any of the below options, or -1 to read the current setting.

#define	DSI16WRC_EXT_CLK_SRC_GRP_0			0
#define	DSI16WRC_EXT_CLK_SRC_GEN			1

//*****************************************************************************
// DSI16WRC_IOCTL_IRQ_SEL					BCTLR D8-D10
//
//	Parameter:	s32
//		Pass in any of the below options, or -1 to read the current setting.

#define	DSI16WRC_IRQ_INIT_DONE				0
#define	DSI16WRC_IRQ_AUTO_CAL_DONE			1
#define	DSI16WRC_IRQ_CHAN_READY				2
#define	DSI16WRC_IRQ_AI_BUF_THRESH_L2H		3
#define	DSI16WRC_IRQ_AI_BUF_THRESH_H2L		4
#define	DSI16WRC_IRQ_AI_BURST_DONE			5

//*****************************************************************************
// DSI16WRC_IOCTL_MASTER_CLK_ADJ			MCAR D0-D15
//
//	Parameter:	s32
//		Pass in any valid from 0 to 0xFFFF, or -1 to read the current setting.

//*****************************************************************************
// DSI16WRC_IOCTL_NDIV						RDR D0-D8
//
//	Parameter:	s32
//		Pass in any value from 1 to 300, or -1 to read the current setting.

//*****************************************************************************
// DSI16WRC_IOCTL_NREF						RCR D12-D23
//	Parameter:	s32
//		Pass in any value from 25 to 300, or -1 to read the current setting.

//*****************************************************************************
// DSI16WRC_IOCTL_NVCO						RCR D0-D11
//
//	Parameter:	s32
//		Pass in any value from 25 tp 300, or -1 to read the current setting.

//*****************************************************************************
// DSI16WRC_IOCTL_RANGE						BCTLR D2-D3
//
//	Parameter:	s32
//		Pass in any of the below options, or -1 to read the current setting.

#define	DSI16WRC_RANGE_10MV					0	// +- 10 m volts
#define	DSI16WRC_RANGE_100MV				1	// +- 100 m volts
#define	DSI16WRC_RANGE_1V					2	// +- 1 volts
#define	DSI16WRC_RANGE_1_25V				3	// +- 1.25 volts
#define	DSI16WRC_RANGE_2_5V					4	// +- 2.5 volts
#define	DSI16WRC_RANGE_5V					5	// +- 5 volts
#define	DSI16WRC_RANGE_10V					6	// +- 10 volts

//*****************************************************************************
// DSI16WRC_IOCTL_RX_IO_ABORT
//
//	Parameter:	s32*
//		The returned value is one of the below options.

#define	DSI16WRC_IO_ABORT_NO				0
#define	DSI16WRC_IO_ABORT_YES				1

//*****************************************************************************
// DSI16WRC_IOCTL_RX_IO_MODE
//
//	Parameter:	s32
//		Pass in any of the gsc_io_mode_t options, or
//		-1 to read the current setting.

#define	DSI16WRC_IO_MODE_DEFAULT			GSC_IO_MODE_PIO
// GSC_IO_MODE_PIO
// GSC_IO_MODE_DMA
// GSC_IO_MODE_DMDMA

//*****************************************************************************
// DSI16WRC_IOCTL_RX_IO_OVERFLOW
//
//	Parameter:	s32
//		Pass in any of the below options, or -1 to read the current setting.

#define	DSI16WRC_IO_OVERFLOW_DEFAULT		DSI16WRC_IO_OVERFLOW_CHECK
#define	DSI16WRC_IO_OVERFLOW_IGNORE			0
#define	DSI16WRC_IO_OVERFLOW_CHECK			1

//*****************************************************************************
// DSI16WRC_IOCTL_RX_IO_TIMEOUT				(in seconds)
//
//	Parameter:	s32
//		Pass in any of the below options, or -1 to read the current setting.

#define	DSI16WRC_IO_TIMEOUT_DEFAULT			10
#define	DSI16WRC_IO_TIMEOUT_NO_SLEEP		0		// Don't sleep to wait for data.
#define	DSI16WRC_IO_TIMEOUT_MIN				0
#define	DSI16WRC_IO_TIMEOUT_MAX				3600	// One hour max.

//*****************************************************************************
// DSI16WRC_IOCTL_RX_IO_UNDERFLOW
//
//	Parameter:	s32
//		Pass in any of the below options, or -1 to read the current setting.

#define	DSI16WRC_IO_UNDERFLOW_DEFAULT		DSI16WRC_IO_UNDERFLOW_CHECK
#define	DSI16WRC_IO_UNDERFLOW_IGNORE		0
#define	DSI16WRC_IO_UNDERFLOW_CHECK			1

//*****************************************************************************
// DSI16WRC_IOCTL_SW_SYNC					BCTLR D6
//
//	Parameter:	None

//*****************************************************************************
// DSI16WRC_IOCTL_SW_SYNC_MODE				BCTLR D17
//
//	Parameter:	s32
//		Pass in any of the below options, or -1 to read the current setting.

#define	DSI16WRC_SW_SYNC_MODE_SYNC			0	// Perform a Sync operation
#define	DSI16WRC_SW_SYNC_MODE_CLR_BUF		1	// Clear the input buffer/

//*****************************************************************************
// DSI16WRC_IOCTL_WAIT_EVENT				all fields must be valid
// DSI16WRC_IOCTL_WAIT_CANCEL				fields need not be valid
// DSI16WRC_IOCTL_WAIT_STATUS				fields need not be valid
//
//	Parameter:	gsc_wait_t*
// gsc_wait_t.flags - see gsc_common.h
// gsc_wait_t.main - see gsc_common.h
// gsc_wait_t.gsc
#define	DSI16WRC_WAIT_GSC_INIT_DONE			0x0001
#define	DSI16WRC_WAIT_GSC_AUTO_CAL_DONE		0x0002
#define	DSI16WRC_WAIT_GSC_CHAN_READY		0x0004
#define	DSI16WRC_WAIT_GSC_AI_BUF_THRESH_L2H	0x0008
#define	DSI16WRC_WAIT_GSC_AI_BUF_THRESH_H2L	0x0010
#define	DSI16WRC_WAIT_GSC_AI_BURST_DONE		0x0020
#define	DSI16WRC_WAIT_GSC_ALL				0x003F
// gsc_wait_t.alt flags
#define	DSI16WRC_WAIT_ALT_ALL				0x0000
// gsc_wait_t.io - see gsc_common.h

//*****************************************************************************
// DSI16WRC_IOCTL_XCVR_TYPE					BCTLR D20
//
//	Parameter:	s32
//		Pass in any of the below options, or -1 to read the current setting.

#define	DSI16WRC_XCVR_TYPE_LVDS				0
#define	DSI16WRC_XCVR_TYPE_TTL				1

#endif
