// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/signals/main.h $
// $Rev: 32584 $
// $Date: 2015-07-04 17:28:47 -0500 (Sat, 04 Jul 2015) $

#ifndef __MAIN_H__
#define __MAIN_H__

#include "24dsi16wrc.h"
#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"
#include "gsc_utils.h"



// #defines	*******************************************************************

#define	VERSION				"1.5"
// 1.5	Updated to use the newer common Linux driver sources.
// 1.4	Additional LINTing.
// 1.3	Updated the parameter list to gsc_id_driver().
// 1.2	Split the utility code into two libraries: common and device specific.
// 1.1	Corrected output spelling.
// 1.0	Initial release.



// prototypes	***************************************************************

int	signals(int fd, int seconds);



#endif
