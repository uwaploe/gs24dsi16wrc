// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/gsc_reg.c $
// $Rev: 31783 $
// $Date: 2015-06-09 10:42:50 -0500 (Tue, 09 Jun 2015) $

#include "main.h"



// #defines *******************************************************************

#ifndef GSC_REG_ENCODE_MASK
	// A device's main.h defnes this if it uses any of the upper 16 bits.
	// It is defined so that the device used bits (16-31) are zero.
	#define	GSC_REG_ENCODE_MASK		(~0UL)
#endif



// data types *****************************************************************

typedef struct
{
	unsigned long	reg;
	unsigned long	type;	// Alt, PCI, PLX, GSC
	unsigned long	size;	// 1, 2, 3 or 4 bytes
	u16				offset;	// Allign per size
	VADDR_T			vaddr;	// Virtual Address
	const os_bar_t*	bar;
	dev_data_t*		dev;
} _reg_t;



// variables ******************************************************************

#ifndef GSC_PCI_SPACE_SIZE
#define	GSC_PCI_SPACE_SIZE	256
#endif

static const os_bar_t	_pci_region	=
{
	/* index		*/	0,	// not needed for PCI registers
	/* offset		*/	0,	// not needed for PCI registers
	/* reg			*/	0,	// not needed for PCI registers
	/* flags		*/	0,	// not needed for PCI registers
	/* io_mapped	*/	0,	// not needed for PCI registers
	/* phys_adrs	*/	0,	// not needed for PCI registers
	/* size			*/	GSC_PCI_SPACE_SIZE,
	/* requested	*/	0,	// not needed for PCI registers
	/* vaddr		*/	0	// not needed for PCI registers
} ;



/******************************************************************************
*
*	Function:	_reg_decode
*
*	Purpose:
*
*		Decode a register id.
*
*	Arguments:
*
*		dev		The structure for the device to access.
*
*		bar		This is the BAR region where the register resides.
*
*		reg		This is the register of interest.
*
*		rt		The decoded register definition.
*
*	Returned:
*
*		None.
*
******************************************************************************/

static void _reg_decode(
	dev_data_t*		dev,
	const os_bar_t*	bar,
	u32				reg,
	_reg_t*			rt)
{
	rt->dev		= dev;
	rt->reg		= reg;
	rt->bar		= bar;
	rt->type	= GSC_REG_TYPE(reg);
	rt->offset	= GSC_REG_OFFSET(reg);
	rt->size	= GSC_REG_SIZE(reg);
	rt->vaddr	= (VADDR_T) ((unsigned long) bar->vaddr + rt->offset);
}



/******************************************************************************
*
*	Function:	_reg_io_mod
*
*	Purpose:
*
*		Perform a read-modify-write operation on an I/O mapped register.
*
*	Arguments:
*
*		rt		The decoded register definition.
*
*		value	The value bits to apply.
*
*		mask	The mask of bits to modify.
*
*	Returned:
*
*		None.
*
******************************************************************************/

static void _reg_io_mod(_reg_t* rt, u32 value, u32 mask)
{
	switch (rt->size)
	{
		default:
		case 1:	os_reg_io_mx_u8(rt->dev, rt->vaddr, value, mask);
				break;

		case 2:	os_reg_io_mx_u16(rt->dev, rt->vaddr, value, mask);
				break;

		case 4:	os_reg_io_mx_u32(rt->dev, rt->vaddr, value, mask);
				break;
	}
}



/******************************************************************************
*
*	Function:	_reg_io_read
*
*	Purpose:
*
*		Read a value from an I/O mapped register.
*
*	Arguments:
*
*		rt		The decoded register definition.
*
*		value	The value read is recorded here.
*
*	Returned:
*
*		None.
*
******************************************************************************/

static void _reg_io_read(_reg_t* rt, u32* value)
{
	switch (rt->size)
	{
		default:
		case 1:	value[0]	= os_reg_io_rx_u8(rt->dev, rt->vaddr);
				break;

		case 2:	value[0]	= os_reg_io_rx_u16(rt->dev, rt->vaddr);
				break;

		case 4:	value[0]	= os_reg_io_rx_u32(rt->dev, rt->vaddr);
				break;
	}
}



/******************************************************************************
*
*	Function:	_reg_io_write
*
*	Purpose:
*
*		Write a value to an I/O mapped register.
*
*	Arguments:
*
*		rt		The decoded register definition.
*
*		value	The value to write to the register.
*
*	Returned:
*
*		None.
*
******************************************************************************/

static void _reg_io_write(_reg_t* rt, u32 value)
{
	switch (rt->size)
	{
		default:
		case 1:	os_reg_io_tx_u8(rt->dev, rt->vaddr, value);
				break;

		case 2:	os_reg_io_tx_u16(rt->dev, rt->vaddr, value);
				break;

		case 4:	os_reg_io_tx_u32(rt->dev, rt->vaddr, value);
				break;
	}
}



/******************************************************************************
*
*	Function:	_reg_mem_mod
*
*	Purpose:
*
*		Perform a read-modify-write operation on a memory mapped register.
*
*	Arguments:
*
*		rt		The decoded register definition.
*
*		value	The value bits to apply.
*
*		mask	The mask of bits to modify.
*
*	Returned:
*
*		None.
*
******************************************************************************/

static void _reg_mem_mod(_reg_t* rt, u32 value, u32 mask)
{
	switch (rt->size)
	{
		default:
		case 1:	os_reg_mem_mx_u8(rt->dev, rt->vaddr, value, mask);
				break;

		case 2:	os_reg_mem_mx_u16(rt->dev, rt->vaddr, value, mask);
				break;

		case 4:	os_reg_mem_mx_u32(rt->dev, rt->vaddr, value, mask);
				break;
	}
}



/******************************************************************************
*
*	Function:	_reg_mem_read
*
*	Purpose:
*
*		Read a value from a memory mapped register.
*
*	Arguments:
*
*		rt		The decoded register definition.
*
*		value	The value read is recorded here.
*
*	Returned:
*
*		None.
*
******************************************************************************/

static void _reg_mem_read(_reg_t* rt, u32* value)
{
	switch (rt->size)
	{
		default:
		case 1:	value[0]	= os_reg_mem_rx_u8(rt->dev, rt->vaddr);
				break;

		case 2:	value[0]	= os_reg_mem_rx_u16(rt->dev, rt->vaddr);
				break;

		case 4:	value[0]	= os_reg_mem_rx_u32(rt->dev, rt->vaddr);
				break;
	}
}



/******************************************************************************
*
*	Function:	_reg_mem_write
*
*	Purpose:
*
*		Write a value to a memory mapped register.
*
*	Arguments:
*
*		rt		The decoded register definition.
*
*		value	The value to write to the register.
*
*	Returned:
*
*		None.
*
******************************************************************************/

static void _reg_mem_write(_reg_t* rt, u32 value)
{
	switch (rt->size)
	{
		default:
		case 1:	os_reg_mem_tx_u8(rt->dev, rt->vaddr, value);
				break;

		case 2:	os_reg_mem_tx_u16(rt->dev, rt->vaddr, value);
				break;

		case 4:	os_reg_mem_tx_u32(rt->dev, rt->vaddr, value);
				break;
	}
}



/******************************************************************************
*
*	Function:	_reg_pci_read_1
*
*	Purpose:
*
*		Read a PCI register that is one byte long.
*
*	Arguments:
*
*		dev		The structure for the device to access.
*
*		rt		The decoded register definition.
*
*		value	The value read is recorded here.
*
*	Returned:
*
*		0		All went well.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

static int _reg_pci_read_1(dev_data_t* dev, _reg_t* rt, u32* value)
{
	int	ret;

	if (rt->offset <= (GSC_PCI_SPACE_SIZE - 1))
	{
		value[0]	= os_reg_pci_rx_u8(dev, rt->offset);
		ret			= 0;
	}
	else
	{
		value[0]	= 0;
		ret			= -EINVAL;
	}

	return(ret);
}



/******************************************************************************
*
*	Function:	_reg_pci_read_2
*
*	Purpose:
*
*		Read a PCI register that is two bytes long. Any alignment is supported.
*
*	Arguments:
*
*		dev		The structure for the device to access.
*
*		rt		The decoded register definition.
*
*		value	The value read is recorded here.
*
*	Returned:
*
*		0		All went well.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

static int _reg_pci_read_2(dev_data_t* dev, _reg_t* rt, u32* value)
{
	int	ret;
	u16	v1;
	u16	v2;

	if (((rt->offset & 1) == 0) && (rt->offset <= (GSC_PCI_SPACE_SIZE - 2)))
	{
		value[0]	= os_reg_pci_rx_u16(dev, rt->offset);
		ret			= 0;
	}
	else if (((rt->offset & 1) == 1) && (rt->offset <= (GSC_PCI_SPACE_SIZE - 3)))
	{
		v1			= os_reg_pci_rx_u16(dev, rt->offset - 1);
		v2			= os_reg_pci_rx_u16(dev, rt->offset + 1);
		value[0]	= ((v2 & 0x00FF) << 8) | ((v1 & 0xFF00) >> 8);
		ret			= 0;
	}
	else
	{
		value[0]	= 0;
		ret			= -EINVAL;
	}

	return(ret);
}



/******************************************************************************
*
*	Function:	_reg_pci_read_3
*
*	Purpose:
*
*		Read a PCI register that is three bytes long. Any alignment is
*		supported.
*
*	Arguments:
*
*		dev		The structure for the device to access.
*
*		rt		The decoded register definition.
*
*		value	The value read is recorded here.
*
*	Returned:
*
*		0		All went well.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

static int _reg_pci_read_3(dev_data_t* dev, _reg_t* rt, u32* value)
{
	int	ret;
	u32	v1;
	u32	v2;

	if (((rt->offset & 3) == 0) && (rt->offset <= (GSC_PCI_SPACE_SIZE - 4)))
	{
		v1			= os_reg_pci_rx_u32(dev, rt->offset);
		value[0]	= v1 & 0x00FFFFFF;
		ret			= 0;
	}
	else if (((rt->offset & 3) == 1) && (rt->offset <= (GSC_PCI_SPACE_SIZE - 4)))
	{
		v1			= os_reg_pci_rx_u32(dev, rt->offset - 1);
		value[0]	= (v1 & 0xFFFFFF00) >> 8;
		ret			= 0;
	}
	else if (((rt->offset & 3) == 2) && (rt->offset <= (GSC_PCI_SPACE_SIZE - 8)))
	{
		v1			= os_reg_pci_rx_u32(dev, rt->offset - 2);
		v2			= os_reg_pci_rx_u32(dev, rt->offset + 2);
		value[0]	= ((v1 & 0xFFFF0000) >> 16) | ((v2 & 0x000000FF) << 16);
		ret			= 0;
	}
	else if (((rt->offset & 3) == 3) && (rt->offset <= (GSC_PCI_SPACE_SIZE - 8)))
	{
		v1			= os_reg_pci_rx_u32(dev, rt->offset - 3);
		v2			= os_reg_pci_rx_u32(dev, rt->offset + 1);
		value[0]	= ((v1 & 0xFF000000) >> 24) | ((v2 & 0x0000FFFF) << 8);
		ret			= 0;
	}
	else
	{
		value[0]	= 0;
		ret			= -EINVAL;
	}

	return(ret);
}



/******************************************************************************
*
*	Function:	_reg_pci_read_4
*
*	Purpose:
*
*		Read a PCI register that is four bytes long. Any alignment is
*		supported.
*
*	Arguments:
*
*		dev		The structure for the device to access.
*
*		rt		The decoded register definition.
*
*		value	The value read is recorded here.
*
*	Returned:
*
*		0		All went well.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

static int _reg_pci_read_4(dev_data_t* dev, _reg_t* rt, u32* value)
{
	int	ret;
	u32	v1;
	u32	v2;

	if (((rt->offset & 3) == 0) && (rt->offset <= (GSC_PCI_SPACE_SIZE - 4)))
	{
		value[0]	= os_reg_pci_rx_u32(dev, rt->offset);
		ret			= 0;
	}
	else if (((rt->offset & 3) == 1) && (rt->offset <= (GSC_PCI_SPACE_SIZE - 8)))
	{
		v1			= os_reg_pci_rx_u32(dev, rt->offset - 1);
		v2			= os_reg_pci_rx_u32(dev, rt->offset + 3);
		value[0]	= ((v1 & 0xFFFFFF00) >> 8) | ((v2 & 0x000000FF) << 24);
		ret			= 0;
	}
	else if (((rt->offset & 3) == 2) && (rt->offset <= (GSC_PCI_SPACE_SIZE - 8)))
	{
		v1			= os_reg_pci_rx_u32(dev, rt->offset - 2);
		v2			= os_reg_pci_rx_u32(dev, rt->offset + 2);
		value[0]	= ((v1 & 0xFFFF0000) >> 16) | ((v2 & 0x0000FFFF) << 16);
		ret			= 0;
	}
	else if (((rt->offset & 3) == 3) && (rt->offset <= (GSC_PCI_SPACE_SIZE - 8)))
	{
		v1			= os_reg_pci_rx_u32(dev, rt->offset - 3);
		v2			= os_reg_pci_rx_u32(dev, rt->offset + 1);
		value[0]	= ((v1 & 0xFF000000) >> 24) | ((v2 & 0x00FFFFFF) << 8);
		ret			= 0;
	}
	else
	{
		value[0]	= 0;
		ret			= -EINVAL;
	}

	return(ret);
}



/******************************************************************************
*
*	Function:	_reg_pci_read
*
*	Purpose:
*
*		Read a value from a PCI register.
*
*	Arguments:
*
*		dev		The structure for the device to access.
*
*		rt		The decoded register definition.
*
*		value	The value read is recorded here.
*
*	Returned:
*
*		0		All went well.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

static int _reg_pci_read(dev_data_t* dev, _reg_t* rt, u32* value)
{
	int	ret;

	switch (rt->size)
	{
		default:	ret		= -EINVAL;
					break;

		case 1:		ret	= _reg_pci_read_1(dev, rt, value);	break;
		case 2:		ret	= _reg_pci_read_2(dev, rt, value);	break;
		case 3:		ret	= _reg_pci_read_3(dev, rt, value);	break;
		case 4:		ret	= _reg_pci_read_4(dev, rt, value);	break;
	}

	return(ret);
}



/******************************************************************************
*
*	Function:	_reg_validate
*
*	Purpose:
*
*		Verify that a regiter id is valid.
*
*	Arguments:
*
*		rt		The decoded register definition.
*
*	Returned:
*
*		0		All went well.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

static int _reg_validate(_reg_t* rt)
{
	int				ret	= -EINVAL;
	unsigned long	ul;

	for (;;)	// We'll use a loop for convenience.
	{
		// Are there extranious bits in the register id?
		ul	= GSC_REG_ENCODE(rt->type, rt->size, rt->offset);

		if (ul != (rt->reg & GSC_REG_ENCODE_MASK))
			break;

		// Does the register extend past the end of the region?
		ul	= rt->offset + rt->size - 1;

		if (ul >= rt->bar->size)
			break;

		// Is the register's size valid?

		if (strchr("\001\002\003\004", (int) rt->size) == NULL)
			break;

		// Is the register properly aligned?

		if ((rt->size == 2) && (rt->offset & 0x1))
			break;
		else if ((rt->size == 4) && (rt->offset & 0x3))
			break;

		//	We don't test the "type" since that is validated
		//	before the register is decoded.

		ret	= 0;
		break;
	}

	return(ret);
}



/******************************************************************************
*
*	Function:	gsc_reg_mod_ioctl
*
*	Purpose:
*
*		Implement the Register Modify (read-modify-write) IOCTL service.
*
*	Arguments:
*
*		alt		The structure to access.
*
*		arg		The IOCTL service's required argument.
*
*	Returned:
*
*		0		All went well.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

int gsc_reg_mod_ioctl(GSC_ALT_STRUCT_T* alt, gsc_reg_t* arg)
{
	dev_data_t*		dev		= GSC_ALT_DEV_GET(alt);
	int				ret;
	_reg_t			rt;
	unsigned long	type	= GSC_REG_TYPE(arg->reg);


	switch (type)
	{
		default:
		case GSC_REG_PCI:	// READ ONLY!
		case GSC_REG_PLX:	// READ ONLY!

			ret	= -EINVAL;
			break;

		case GSC_REG_ALT:

			ret	= dev_reg_mod_alt(alt, arg);
			break;

		case GSC_REG_GSC:

			_reg_decode(dev, &dev->gsc, arg->reg, &rt);
			ret	= _reg_validate(&rt);

			if (ret)
				;
			else if (rt.bar->io_mapped)
				_reg_io_mod(&rt, arg->value, arg->mask);
			else
				_reg_mem_mod(&rt, arg->value, arg->mask);

			break;
	}

	return(ret);
}



/******************************************************************************
*
*	Function:	gsc_reg_read_ioctl
*
*	Purpose:
*
*		Implement the Register Read IOCTL service.
*
*	Arguments:
*
*		alt		The structure to access.
*
*		arg		The IOCTL service's required argument.
*
*	Returned:
*
*		0		All went well.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

int gsc_reg_read_ioctl(GSC_ALT_STRUCT_T* alt, gsc_reg_t* arg)
{
	dev_data_t*		dev		= GSC_ALT_DEV_GET(alt);
	int				ret;
	_reg_t			rt;
	unsigned long	type	= GSC_REG_TYPE(arg->reg);

	switch (type)
	{
		default:

			ret	= -EINVAL;
			break;

		case GSC_REG_ALT:

			ret	= dev_reg_read_alt(alt, arg);
			break;

		case GSC_REG_GSC:

			_reg_decode(dev, &dev->gsc, arg->reg, &rt);
			ret	= _reg_validate(&rt);

			if (ret)
				;
			else if (rt.bar->io_mapped)
				_reg_io_read(&rt, &arg->value);
			else
				_reg_mem_read(&rt, &arg->value);

			break;

		case GSC_REG_PCI:

			_reg_decode(dev, &_pci_region, arg->reg, &rt);
			ret	= _reg_validate(&rt);

			if (ret == 0)
				ret	= _reg_pci_read(dev, &rt, &arg->value);

			break;

		case GSC_REG_PLX:

			_reg_decode(dev, &dev->plx, arg->reg, &rt);
			ret	= _reg_validate(&rt);

			if (ret == 0)
				_reg_mem_read(&rt, &arg->value);

			break;
	}

	return(ret);
}



/******************************************************************************
*
*	Function:	gsc_reg_write_ioctl
*
*	Purpose:
*
*		Implement the Register Write IOCTL service.
*
*	Arguments:
*
*		alt		The structure to access.
*
*		arg		The IOCTL service's required argument.
*
*	Returned:
*
*		0		All went well.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

int gsc_reg_write_ioctl(GSC_ALT_STRUCT_T* alt, gsc_reg_t* arg)
{
	dev_data_t*		dev		= GSC_ALT_DEV_GET(alt);
	int				ret;
	_reg_t			rt;
	unsigned long	type	= GSC_REG_TYPE(arg->reg);

	switch (type)
	{
		default:
		case GSC_REG_PCI:	// READ ONLY!
		case GSC_REG_PLX:	// READ ONLY!

			ret	= -EINVAL;
			break;

		case GSC_REG_ALT:

			ret	= dev_reg_write_alt(alt, arg);
			break;

		case GSC_REG_GSC:

			_reg_decode(dev, &dev->gsc, arg->reg, &rt);
			ret	= _reg_validate(&rt);

			if (ret)
				;
			else if (rt.bar->io_mapped)
				_reg_io_write(&rt, arg->value);
			else
				_reg_mem_write(&rt, arg->value);

			break;
	}

	return(ret);
}



/******************************************************************************
*
*	Function:	gsc_reg_mod
*
*	Purpose:
*
*		Perform a read-modify-write on a register. GSC REGISTERS ONLY!
*
*	Arguments:
*
*		alt		The structure to access.
*
*		reg		The register to access.
*
*		val		The value to write to the register.
*
*	Returned:
*
*		None.
*
******************************************************************************/

void gsc_reg_mod(GSC_ALT_STRUCT_T* alt, u32 reg, u32 val, u32 mask)
{
	gsc_reg_t	arg;

	arg.reg		= reg;
	arg.value	= val;
	arg.mask	= mask;
	gsc_reg_mod_ioctl(alt, &arg);
}


/******************************************************************************
*
*	Function:	gsc_reg_read
*
*	Purpose:
*
*		Read a value from a register.
*
*	Arguments:
*
*		alt		The structure to access.
*
*		reg		The register to access.
*
*	Returned:
*
*		The value read.
*
******************************************************************************/

u32 gsc_reg_read(GSC_ALT_STRUCT_T* alt, u32 reg)
{
	gsc_reg_t	arg;

	arg.reg		= reg;
	arg.value	= 0xDEADBEEF;
	arg.mask	= 0;
	gsc_reg_read_ioctl(alt, &arg);
	return(arg.value);
}



/******************************************************************************
*
*	Function:	gsc_reg_write
*
*	Purpose:
*
*		Write a value to a register. GSC REGISTERS ONLY!
*
*	Arguments:
*
*		alt		The structure to access.
*
*		reg		The register to access.
*
*		val		The value to write to the register.
*
*	Returned:
*
*		None.
*
******************************************************************************/

void gsc_reg_write(GSC_ALT_STRUCT_T* alt, u32 reg, u32 val)
{
	gsc_reg_t	arg;

	arg.reg		= reg;
	arg.value	= val;
	arg.mask	= 0;
	gsc_reg_write_ioctl(alt, &arg);
}


