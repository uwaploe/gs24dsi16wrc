// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/linux/os_mem.c $
// $Rev: 22964 $
// $Date: 2013-08-26 14:55:03 -0500 (Mon, 26 Aug 2013) $

// Linux driver module

#include "main.h"



/******************************************************************************
*
*	Function:	_order_to_size
*
*	Purpose:
*
*		Convert an order value to its corresponding size in page sized units.
*
*	Arguments:
*
*		order	The page size order to convert.
*
*	Returned:
*
*		>= 0	The order corresponding to the given size.
*
******************************************************************************/

static u32 _order_to_size(int order)
{
	u32	size	= PAGE_SIZE;

	// Limit sizes to 1G.

	for (; order > 0; order--)
	{
		if (size >= 0x40000000)
			break;

		size	*= 2;
	}

	return(size);
}



/******************************************************************************
*
*	Function:	_pages_free
*
*	Purpose:
*
*		Free memory allocated via _pages_alloc().
*
*	Arguments:
*
*		io		Parameters for the allocation to be freed.
*
*	Returned:
*
*		None.
*
******************************************************************************/

static void _pages_free(os_mem_t* mem)
{
	s32				bytes;
	unsigned long	ul;

	if (mem->ptr)
	{
		bytes	= mem->bytes;
		ul		= (unsigned long) mem->ptr;

		for (; bytes >= PAGE_SIZE; bytes -= PAGE_SIZE, ul += PAGE_SIZE)
			PAGE_UNRESERVE(ul);

		// Free the memory.
		free_pages((unsigned long) mem->ptr, mem->order);
		mem->ptr		= NULL;
		mem->bytes		= 0;
		mem->order		= 0;
	}
}



/******************************************************************************
*
*	Function:	_size_to_order
*
*	Purpose:
*
*		Convert a size value to a corresponding page size order value.
*
*	Arguments:
*
*		size	The desired size in bytes.
*
*	Returned:
*
*		>= 0	The order corresponding to the given size.
*
******************************************************************************/

static int _size_to_order(u32 size)
{
	u32	bytes	= PAGE_SIZE;
	int	order	= 0;

	// Limit sizes to 1G.

	for (; bytes < 0x40000000;)
	{
		if (bytes >= size)
			break;

		order++;
		bytes	*= 2;
	}

	return(order);
}



//*****************************************************************************
int os_mem_copy_from_user(void* dst, const void* src, long size)
{
	int				ret;
	unsigned long	ul;

	for (;;)	// A convenience loop.
	{
		ul	= access_ok(VERIFY_READ, (void*) src, size);

		if (ul == 0)
		{
			// We can't read from the user's memory.
			ret	= -EFAULT;
			break;
		}

		ul	= copy_from_user(dst, (void*) src, size);

		if (ul)
		{
			// We couldn't read from the user's memory.
			ret	= -EFAULT;
			break;
		}

		ret	= 0;
		break;
	}

	return(ret);
}



//*****************************************************************************
int os_mem_copy_to_user(void* dst, const void* src, long size)
{
	int				ret;
	unsigned long	ul;

	for (;;)	// A convenience loop.
	{
		ul	= access_ok(VERIFY_WRITE, dst, size);

		if (ul == 0)
		{
			// We can't write to the user's memory.
			ret	= -EFAULT;
			break;
		}

		ul	= copy_to_user(dst, (void*) src, size);

		if (ul)
		{
			// We couldn't write to the user's memory.
			ret	= -EFAULT;
			break;
		}

		ret	= 0;
		break;
	}

	return(ret);
}



//*****************************************************************************
void* os_mem_data_alloc(size_t size)
{
	void*	ptr;

	ptr	= kmalloc(size, GFP_KERNEL);

	if (ptr == NULL)
		printk("%s: os_mem_data_alloc(%ld) failed.\n", DEV_NAME, (long) size);

	return(ptr);
}



//*****************************************************************************
void os_mem_data_free(void* ptr)
{
	kfree(ptr);
}



//*****************************************************************************
void* os_mem_dma_alloc(size_t* bytes, os_mem_t* mem)
{
	u8				order;
	void*			ptr;
	unsigned long	tmp;
	unsigned long	ul;

	if (bytes[0] > MEM_ALLOC_LIMIT)
		bytes[0]	= MEM_ALLOC_LIMIT;

	order		= _size_to_order(bytes[0]);
	bytes[0]	= _order_to_size(order);

	for (;;)
	{
		ptr	= (void*) __get_free_pages(GFP_KERNEL | GFP_DMA_GSC | __GFP_NOWARN, order);

		if (ptr)
			break;

		if (order == 0)
			break;

		order--;
		bytes[0]	/= 2;
	}

	if (ptr == NULL)
	{
		mem->ptr	= NULL;
		mem->bytes	= bytes[0];
		mem->order	= 0;
		printk("%s: os_mem_dma_alloc(%ld) failed.\n", DEV_NAME, (long) bytes[0]);
	}
	else
	{
		mem->ptr	= ptr;
		mem->bytes	= bytes[0];
		mem->order	= order;

		// Mark the pages as reserved so the MM will leave'm alone.
		ul	= (unsigned long) mem->ptr;
		tmp	= bytes[0];

		for (; tmp >= PAGE_SIZE; tmp -= PAGE_SIZE, ul += PAGE_SIZE)
			PAGE_RESERVE(ul);
	}

	return(ptr);
}



//*****************************************************************************
void os_mem_dma_free(os_mem_t* mem)
{
	_pages_free(mem);
}



//*****************************************************************************
void* os_mem_virt_to_phys(void* ptr)
{
	void*	pa;

	pa	= (void*) (unsigned long) virt_to_bus(ptr);
	return(pa);
}


