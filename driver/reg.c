// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/driver/reg.c $
// $Rev: 31805 $
// $Date: 2015-06-09 12:14:32 -0500 (Tue, 09 Jun 2015) $

#include "main.h"



//*****************************************************************************
int dev_reg_mod_alt(dev_data_t* dev, gsc_reg_t* arg)
{
	// No alternate register encodings are defined for this driver.
	return(-EINVAL);
}



//*****************************************************************************
int dev_reg_read_alt(dev_data_t* dev, gsc_reg_t* arg)
{
	// No alternate register encodings are defined for this driver.
	return(-EINVAL);
}



//*****************************************************************************
int dev_reg_write_alt(dev_data_t* dev, gsc_reg_t* arg)
{
	// No alternate register encodings are defined for this driver.
	return(-EINVAL);
}


