// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/utils/24dsi16wrc_utils.h $
// $Rev: 32482 $
// $Date: 2015-07-04 15:00:15 -0500 (Sat, 04 Jul 2015) $

#ifndef __24DSI16WRC_UTILS_H__
#define __24DSI16WRC_UTILS_H__

#include "24dsi16wrc.h"
#include "gsc_utils.h"



// #defines	*******************************************************************

#define	ARRAY_ELEMENTS(a)	(sizeof((a))/sizeof((a)[0]))



// prototypes *****************************************************************

int	dsi16wrc_adc_mode(int fd, int index, s32 set, s32* get);			// DSI16WRC_IOCTL_ADC_MODE
int	dsi16wrc_adc_mode_quiet(int fd, s32 set, s32* get);
int	dsi16wrc_ai_buf_clear(int fd, int index);							// DSI16WRC_IOCTL_AI_BUF_CLEAR
int	dsi16wrc_ai_buf_clear_at_boundary(int fd, int index);
int	dsi16wrc_ai_buf_enable(int fd, int index, s32 set, s32* get);		// DSI16WRC_IOCTL_AI_BUF_ENABLE
int	dsi16wrc_ai_buf_fill_lvl(int fd, int index, s32* get);				// DSI16WRC_IOCTL_AI_BUF_FILL_LVL
int	dsi16wrc_ai_buf_overflow(int fd, int index, s32 set, s32* get);		// DSI16WRC_IOCTL_AI_BUF_OVERFLOW
int	dsi16wrc_ai_buf_thr_sts(int fd, int index, s32* get);				// DSI16WRC_IOCTL_AI_BUF_THR_STS
int	dsi16wrc_ai_buf_thresh(int fd, int index, s32 set, s32* get);		// DSI16WRC_IOCTL_AI_BUF_THRESH
int	dsi16wrc_ai_buf_underflow(int fd, int index, s32 set, s32* get);	// DSI16WRC_IOCTL_AI_BUF_UNDERFLOW
int	dsi16wrc_ai_channel_tag(int fd, int index, s32 set, s32* get);		// DSI16WRC_IOCTL_AI_CHANNEL_TAG
int	dsi16wrc_ai_mode(int fd, int index, s32 set, s32* get);				// DSI16WRC_IOCTL_AI_MODE
int	dsi16wrc_auto_cal_sts(int fd, int index, s32* get);					// DSI16WRC_IOCTL_AUTO_CAL_STS
int	dsi16wrc_auto_calibrate(int fd, int index);							// DSI16WRC_IOCTL_AUTO_CALIBRATE
int	dsi16wrc_aux_clk_ctl_mode(int fd, int index, s32 set, s32* get);	// DSI16WRC_IOCTL_AUX_CLK_CTL_MODE
int	dsi16wrc_aux_sync_ctl_mode(int fd, int index, s32 set, s32* get);	// DSI16WRC_IOCTL_AUX_SYNC_CTL_MODE

int	dsi16wrc_burst(int fd, int index, s32 set, s32* get);				// DSI16WRC_IOCTL_BURST
int	dsi16wrc_burst_rate_div(int fd, int index, s32 set, s32* get);		// DSI16WRC_IOCTL_BURST_RATE_DIV
int	dsi16wrc_burst_size(int fd, int index, s32 set, s32* get);			// DSI16WRC_IOCTL_BURST_SIZE
int	dsi16wrc_burst_timer(int fd, int index, s32 set, s32* get);			// DSI16WRC_IOCTL_BURST_TIMER
int	dsi16wrc_burst_trigger(int fd, int index);							// DSI16WRC_IOCTL_BURST_TRIGGER

int	dsi16wrc_channel_group(int fd, s32 channel, s32* group);
int	dsi16wrc_ch_grp_0_src(int fd, int index, s32 set, s32* get);		// DSI16WRC_IOCTL_CH_GRP_0_SRC
int	dsi16wrc_ch_grp_0_src_quiet(int fd, s32 set, s32* get);
int	dsi16wrc_ch_grp_1_src(int fd, int index, s32 set, s32* get);		// DSI16WRC_IOCTL_CH_GRP_1_SRC
int	dsi16wrc_ch_grp_1_src_quiet(int fd, s32 set, s32* get);
int	dsi16wrc_channels_ready(int fd, int index, s32 set, s32* get);		// DSI16WRC_IOCTL_CHANNELS_READY
int	dsi16wrc_channels_ready_quiet(int fd, s32 set, s32* get);
int	dsi16wrc_config_board(int fd, int index, s32 fref, s32 fsamp);
int	dsi16wrc_control_mode(int fd, int index, s32 set, s32* get);		// DSI16WRC_IOCTL_CONTROL_MODE

int	dsi16wrc_data_format(int fd, int index, s32 set, s32* get);			// DSI16WRC_IOCTL_DATA_FORMAT
int	dsi16wrc_data_width(int fd, int index, s32 set, s32* get);			// DSI16WRC_IOCTL_DATA_WIDTH
int	dsi16wrc_dio_dir_out(int fd, int index, s32 set, s32* get);			// DSI16WRC_IOCTL_DIO_DIR_OUT
int	dsi16wrc_dio_read(int fd, int index, s32 set, s32* get);			// DSI16WRC_IOCTL_DIO_READ
int	dsi16wrc_dio_write(int fd, int index, s32 set, s32* get);			// DSI16WRC_IOCTL_DIO_WRITE

int	dsi16wrc_ext_clk_src(int fd, int index, s32 set, s32* get);			// DSI16WRC_IOCTL_EXT_CLK_SRC

int	dsi16wrc_fsamp_compute(int fd, int index, int quiet, s32 fref, s32* fsamp, s32* mode, s32* nvco, s32* nref, s32* ndiv, s32* mca);
int	dsi16wrc_fsamp_report(int fd, int index, int chan, s32* sps);
int	dsi16wrc_fsamp_report_all(int fd, int index);
int	dsi16wrc_fsamp_validate(int fd, s32* fsamp);

int	dsi16wrc_id_board(int fd, int index, const char* desc);
int	dsi16wrc_initialize(int fd, int index);								// DSI16WRC_IOCTL_INITIALIZE
int	dsi16wrc_irq_sel(int fd, int index, s32 set, s32* get);				// DSI16WRC_IOCTL_IRQ_SEL

int	dsi16wrc_master_clk_adj(int fd, int index, s32 set, s32* get);		// DSI16WRC_IOCTL_MASTER_CLK_ADJ
int	dsi16wrc_master_clk_adj_quiet(int fd, s32 set, s32* get);

int	dsi16wrc_ndiv(int fd, int index, s32 set, s32* get);				// DSI16WRC_IOCTL_NDIV
int	dsi16wrc_ndiv_quiet(int fd, s32 set, s32* get);
int	dsi16wrc_nref(int fd, int index, s32 set, s32* get);				// DSI16WRC_IOCTL_NREF
int	dsi16wrc_nref_quiet(int fd, s32 set, s32* get);
int	dsi16wrc_nvco(int fd, int index, s32 set, s32* get);				// DSI16WRC_IOCTL_NVCO
int	dsi16wrc_nvco_quiet(int fd, s32 set, s32* get);

int	dsi16wrc_query_fref_default(int fd, int index, s32* get);
int	dsi16wrc_query_quiet(int fd, s32 set, s32* get);					// DSI16WRC_IOCTL_QUERY

int	dsi16wrc_range(int fd, int index, s32 set, s32* get);				// DSI16WRC_IOCTL_RANGE
int	dsi16wrc_reg_list(int fd, int detail);
int	dsi16wrc_reg_mod(int fd, u32 reg, u32 value, u32 mask);				// DSI16WRC_IOCTL_REG_MOD
int	dsi16wrc_reg_read(int fd, u32 reg, u32* value);						// DSI16WRC_IOCTL_REG_READ
int	dsi16wrc_reg_write(int fd, u32 reg, u32 value);						// DSI16WRC_IOCTL_REG_WRITE
int	dsi16wrc_rx_io_abort(int fd, int index, s32 set, s32* get);			// DSI16WRC_IOCTL_RX_IO_ABORT
int	dsi16wrc_rx_io_mode(int fd, int index, s32 set, s32* get);			// DSI16WRC_IOCTL_RX_IO_MODE
int	dsi16wrc_rx_io_overflow(int fd, int index, s32 set, s32* get);		// DSI16WRC_IOCTL_RX_IO_OVERFLOW
int	dsi16wrc_rx_io_timeout(int fd, int index, s32 set, s32* get);		// DSI16WRC_IOCTL_RX_IO_TIMEOUT
int	dsi16wrc_rx_io_underflow(int fd, int index, s32 set, s32* get);		// DSI16WRC_IOCTL_RX_IO_UNDERFLOW

int	dsi16wrc_sw_sync(int fd, int index);								// DSI16WRC_IOCTL_SW_SYNC
int	dsi16wrc_sw_sync_quiet(int fd);
int	dsi16wrc_sw_sync_mode(int fd, int index, s32 set, s32* get);		// DSI16WRC_IOCTL_SW_SYNC_MODE
int	dsi16wrc_sw_sync_mode_quiet(int fd, s32 set, s32* get);

int	dsi16wrc_wait_cancel(int fd, int index, gsc_wait_t* wait);			// DSI16WRC_IOCTL_WAIT_CANCEL
int	dsi16wrc_wait_event(int fd, gsc_wait_t* wait);						// DSI16WRC_IOCTL_WAIT_EVENT
int	dsi16wrc_wait_status(int fd, int index, gsc_wait_t* wait);			// DSI16WRC_IOCTL_WAIT_STATUS

int	dsi16wrc_xcvr_type(int fd, int index, s32 set, s32* get);			// DSI16WRC_IOCTL_XCVR_TYPE

const gsc_reg_def_t*	dsi16wrc_reg_get_def_id(u32 reg);
const gsc_reg_def_t*	dsi16wrc_reg_get_def_index(int index);
const char*				dsi16wrc_reg_get_desc(u32 reg);
const char*				dsi16wrc_reg_get_name(u32 reg);



#endif


