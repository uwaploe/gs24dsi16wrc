// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/gsc_init.c $
// $Rev: 31783 $
// $Date: 2015-06-09 10:42:50 -0500 (Tue, 09 Jun 2015) $

#include "main.h"



// variables ******************************************************************

gsc_global_t	gsc_global;



/******************************************************************************
*
*	Function:	_check_id
*
*	Purpose:
*
*		Check to see if the referenced device is one we support. If it is, then
*		fill in identity fields for the device structure given.
*
*	Arguments:
*
*		dev		The device structure were we put identify data.
*
*	Returned:
*
*		0		We don't support this device.
*		1		We do support this device.
*
******************************************************************************/

static int _check_id(dev_data_t* dev)
{
	u16	didr;
	int	found	= 0;
	int	i;
	u16	sdidr;
	u16	svidr;
	u16	vidr;

	vidr	= os_reg_pci_rx_u16(dev, 0x00);
	didr	= os_reg_pci_rx_u16(dev, 0x02);
	svidr	= os_reg_pci_rx_u16(dev, 0x2C);
	sdidr	= os_reg_pci_rx_u16(dev, 0x2E);

	for (i = 0; dev_id_list[i].model; i++)
	{
		if ((dev_id_list[i].vendor		== vidr)	&&
			(dev_id_list[i].device		== didr)	&&
			(dev_id_list[i].sub_vendor	== svidr)	&&
			(dev_id_list[i].sub_device	== sdidr))
		{
			found			= 1;
			dev->model		= dev_id_list[i].model;
			dev->board_type	= dev_id_list[i].type;
			break;

			// It is possible for the model and type to change once the device
			// specific code has a chance to examine the board more closely.
		}
	}

#if DEV_PCI_ID_SHOW
	printf(	"ID: %04lX %04lX %04lX %04lX",
			(long) vidr,
			(long) didr,
			(long) svidr,
			(long) sdidr);

	if (found)
	{
		printf(" <--- %s, type %d", dev->model, dev->board_type);
	}

	printf("\n");
#endif

	return(found);
}



/******************************************************************************
*
*	Function:	_dev_data_t_add
*
*	Purpose:
*
*		Add a device to our device list.
*
*	Arguments:
*
*		dev		A pointer to the structure to add.
*
*	Returned:
*
*		0		All went well.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

static int _dev_data_t_add(dev_data_t* dev)
{
	int	ret;
	int	i;

	for (;;)	// A convenience loop.
	{
		for (i = 0; i < (int) ARRAY_ELEMENTS(gsc_global.dev_list); i++)
		{
			if (gsc_global.dev_list[i] == NULL)
				break;
		}

		if (i >= (int) ARRAY_ELEMENTS(gsc_global.dev_list))
		{
			// We don't have enough room to add the device.
			ret	= -ENOMEM;
			printf(	"%s: _dev_data_t_add, line %d:"
					" Too many devices were found. The limit is %d.\n",
					DEV_NAME,
					__LINE__,
					(int) ARRAY_ELEMENTS(gsc_global.dev_list));
			break;
		}

		gsc_global.dev_list[i]	= dev;
		dev->board_index		= i;
		ret						= 0;
		gsc_global.dev_qty++;
		break;
	}

	return(ret);
}



//*****************************************************************************
int gsc_init_add_device(dev_data_t* dev)
{
	int	found;
	int	ret	= 0;

	for (;;)	// A convenience loop.
	{
		if (dev == NULL)
		{
			ret	= -EINVAL;
			break;
		}

		found	= _check_id(dev);

		if (found == 0)	// The device was not ours and was not added.
			break;

		ret	= _dev_data_t_add(dev);

		if (ret == 0)
			ret	= dev_device_create(dev);

		if (ret == 0)
			ret	= GSC_PLX_EEPROM_ACCESS(dev);

		if (ret)	// ret < 0, meaning there was an error.
		{
			gsc_init_remove_device(dev);
		}
		else
		{
			ret	= 1;	// The device was added.
		}

		break;
	}

	return(ret);
}



//*****************************************************************************
void gsc_init_remove_device(dev_data_t* dev)
{
	int	i;

	for (i = 0; i < (int) ARRAY_ELEMENTS(gsc_global.dev_list); i++)
	{
		if (gsc_global.dev_list[i] == dev)
		{
			dev_device_destroy(dev);
			gsc_global.dev_list[i]	= NULL;
			gsc_global.dev_qty--;
			break;
		}
	}
}


