// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/utils/util_query.c $
// $Rev: 32482 $
// $Date: 2015-07-04 15:00:15 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>

#include "24dsi16wrc.h"
#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi16wrc_query_quiet
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI16WRC_IOCTL_QUERY service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		set		This is the value to apply.
*
*		get		The results are reported here. This may be NULL.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi16wrc_query_quiet(int fd, s32 set, s32* get)
{
	int	errs;

	errs	= dsi16wrc_dsl_ioctl(fd, DSI16WRC_IOCTL_QUERY, &set);

	if (get)
		get[0]	= set;

	return(errs);
}



//*****************************************************************************
int	dsi16wrc_query_fref_default(int fd, int index, s32* get)
{
	int	errs;
	s32	set;

	gsc_label_index("Fref Default", index);
	errs	= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_FREF_DEFAULT, &set);
	printf("%s  (%ld Hz)\n", errs ? "FAIL <---" : "PASS", (long) set);

	if (get)
		get[0]	= set;

	return(errs);
}


