// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/utils/util_config.c $
// $Rev: 32482 $
// $Date: 2015-07-04 15:00:15 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>
#include <string.h>

#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi16wrc_config_board
*
*	Purpose:
*
*		Configure the given board using common defaults.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		fref	This is the PLL Fref value, or -1 to use the default.
*
*		fsamp	This is the desired Fsamp rate, of -1 to use the default.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int	dsi16wrc_config_board(int fd, int index, s32 fref, s32 fsamp)
{
	int	errs	= 0;
	s32	mca;
	s32	mode;
	s32	ndiv;
	s32	nref;
	s32	nvco;

	errs	+= dsi16wrc_initialize				(fd, index);
	errs	+= dsi16wrc_rx_io_mode				(fd, index, GSC_IO_MODE_DMA, NULL);
	errs	+= dsi16wrc_rx_io_overflow			(fd, index, DSI16WRC_IO_OVERFLOW_CHECK, NULL);
	errs	+= dsi16wrc_rx_io_timeout			(fd, index, 30, NULL);
	errs	+= dsi16wrc_rx_io_underflow			(fd, index, DSI16WRC_IO_UNDERFLOW_CHECK, NULL);
	errs	+= dsi16wrc_ai_mode					(fd, index, DSI16WRC_AI_MODE_DIFF, NULL);
	errs	+= dsi16wrc_ai_buf_thresh			(fd, index, 128L * 1024, NULL);
	errs	+= dsi16wrc_ai_channel_tag			(fd, index, DSI16WRC_AI_CHANNEL_TAG_ENABLE, NULL);
	errs	+= dsi16wrc_sw_sync_mode			(fd, index, DSI16WRC_SW_SYNC_MODE_CLR_BUF, NULL);
	errs	+= dsi16wrc_data_format				(fd, index, DSI16WRC_DATA_FORMAT_OFF_BIN, NULL);
	errs	+= dsi16wrc_data_width				(fd, index, DSI16WRC_DATA_WIDTH_16, NULL);
	errs	+= dsi16wrc_dio_dir_out				(fd, index, 0x0, NULL);
	errs	+= dsi16wrc_control_mode			(fd, index, DSI16WRC_CONTROL_MODE_INITIATOR, NULL);
	errs	+= dsi16wrc_ext_clk_src				(fd, index, DSI16WRC_EXT_CLK_SRC_GRP_0, NULL);
	errs	+= dsi16wrc_irq_sel					(fd, index, DSI16WRC_IRQ_INIT_DONE, NULL);
	errs	+= dsi16wrc_query_fref_default		(fd, index, &fref);
	errs	+= dsi16wrc_ch_grp_0_src			(fd, index, DSI16WRC_CH_GRP_SRC_RATE_GEN, NULL);
	errs	+= dsi16wrc_ch_grp_1_src			(fd, index, DSI16WRC_CH_GRP_SRC_RATE_GEN, NULL);
	errs	+= dsi16wrc_range					(fd, index, DSI16WRC_RANGE_10V, NULL);
	errs	+= dsi16wrc_fsamp_compute			(fd, index, 0, fref, &fsamp, &mode, &nvco, &nref, &ndiv, &mca);
	errs	+= dsi16wrc_master_clk_adj			(fd, index, mca, NULL);
	errs	+= dsi16wrc_adc_mode				(fd, index, mode, NULL);
	errs	+= dsi16wrc_nvco					(fd, index, nvco, NULL);
	errs	+= dsi16wrc_nref					(fd, index, nref, NULL);
	errs	+= dsi16wrc_ndiv					(fd, index, ndiv, NULL);
	errs	+= dsi16wrc_fsamp_report_all		(fd, index);
	errs	+= dsi16wrc_xcvr_type				(fd, index, DSI16WRC_XCVR_TYPE_LVDS, NULL);
	errs	+= dsi16wrc_aux_clk_ctl_mode		(fd, index, DSI16WRC_AUX_CLK_CTL_MODE_INACTIVE, NULL);
	errs	+= dsi16wrc_aux_sync_ctl_mode		(fd, index, DSI16WRC_AUX_SYNC_CTL_MODE_INACTIVE, NULL);
	errs	+= dsi16wrc_burst					(fd, index, DSI16WRC_BURST_DISABLE, NULL);
	errs	+= dsi16wrc_burst_rate_div			(fd, index, 1, NULL);
	errs	+= dsi16wrc_burst_size				(fd, index, 1, NULL);
	errs	+= dsi16wrc_burst_timer				(fd, index, DSI16WRC_BURST_TIMER_DISABLE, NULL);
	errs	+= dsi16wrc_ai_buf_enable			(fd, index, DSI16WRC_AI_BUF_ENABLE_YES, NULL);
//	errs	+= dsi16wrc_auto_calibrate			(fd, index);
	errs	+= dsi16wrc_auto_cal_sts			(fd, index, NULL);
	errs	+= dsi16wrc_ai_buf_clear_at_boundary(fd, index);
	errs	+= dsi16wrc_ai_buf_overflow			(fd, index, DSI16WRC_AI_BUF_OVERFLOW_TEST, NULL);
	errs	+= dsi16wrc_ai_buf_underflow		(fd, index, DSI16WRC_AI_BUF_UNDERFLOW_TEST, NULL);

	return(errs);
}


