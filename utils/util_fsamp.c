// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/utils/util_fsamp.c $
// $Rev: 32482 $
// $Date: 2015-07-04 15:00:15 -0500 (Sat, 04 Jul 2015) $

#include <limits.h>
#include <stdio.h>
#include <string.h>

#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"
#include "gsc_utils.h"



// #defines	*******************************************************************

#define	ABS(v)					(((v) < 0) ? (-(v)) : (v))
#define	LIMIT_MAX(v,max)		(((v) > (max)) ? (max) : (v))
#define	LIMIT_MIN(v,min)		(((v) < (min)) ? (min) : (v))
#define	LIMIT_RANGE(v,min,max)	(LIMIT_MIN((LIMIT_MAX((v),(max))),(min)))



// data types *****************************************************************

typedef struct
{
	s32		fref;
	s32		fsamp_want;
	float	fsamp_got;
	float	delta;
	long	fsamp_min;
	long	fsamp_max;
	long	divisor;
	s32		mode;
	s32		nvco;
	s32		nref;
	s32		ndiv;
	s32		mca;		// Master Clock Adjust
} fsamp_t;



//*****************************************************************************
static int _dsi16wrc_fsamp_compute_mca(int fd, fsamp_t* data)
{
	float	delta;
	s32		div;
	s32		div_max;
	s32		div_min;
	int		errs	= 0;
	float	fgen;
	s32		fgen_max;
	s32		fgen_min;
	float	fref;
	float	fsamp;
	float	mca;
	s32		ndiv_max;
	s32		ndiv_min;
	s32		nref_max;
	s32		nref_min;
	s32		nvco_max;
	s32		nvco_min;
	s32		ref;
	s32		ref_max;
	s32		ref_min;
	float	samp;
	s32		vco;
	s32		vco_max;
	s32		vco_min;

	if (data->mca >= 0x8000)
		mca	= + (((float) (data->mca - 0x8000)) / ((float) (0xFFFF - 0x8000))) * 80;
	else
		mca	= - (((float) (0x8000 - data->mca)) / ((float) (0x8000 - 0x0000))) * 80;

	fref	= mca + (float) data->fref;

	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_FGEN_MIN, &fgen_min);
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_FGEN_MAX, &fgen_max);
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_NDIV_MIN, &ndiv_min);
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_NDIV_MAX, &ndiv_max);
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_NREF_MIN, &nref_min);
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_NREF_MAX, &nref_max);
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_NVCO_MIN, &nvco_min);
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_NVCO_MAX, &nvco_max);

	fsamp	= LIMIT_RANGE((s32) data->fsamp_want, data->fsamp_min, data->fsamp_max);

	div_max	= (s32) ((float) fgen_max / ((float) data->fsamp_want * data->divisor));
	div_min	= (s32) ((float) fgen_min / ((float) data->fsamp_want * data->divisor));
	div_max	= LIMIT_RANGE(div_max, ndiv_min, ndiv_max);
	div_min	= LIMIT_RANGE(div_min, ndiv_min, ndiv_max);

	for (div = div_min - 1; div <= (div_max + 1); div++)
	{
		if ((div < ndiv_min) || (div > ndiv_max))
			continue;

		fgen	= (float) fsamp * div * data->divisor;

		if ((fgen < fgen_min) || (fgen > fgen_max))
			continue;

		ref_max	= (s32) (fref * nvco_max / fgen);
		ref_min	= (s32) (fref * nvco_min / fgen);
		ref_max	= LIMIT_RANGE(ref_max, nref_min, nref_max);
		ref_min	= LIMIT_RANGE(ref_min, nref_min, nref_max);

		for (ref = ref_min - 1; ref <= (ref_max + 1); ref++)
		{
			if ((ref < nref_min) || (ref > nref_max))
				continue;

			vco		= (s32) (fgen * ref / fref);
			vco_max	= vco;
			vco_min	= vco;

			for (vco = vco_min - 1; vco <= (vco_max + 1); vco++)
			{
				if ((vco < nvco_min) || (vco > nvco_max))
					continue;

				fgen	= fref * vco / ref;
				samp	= fgen / div / data->divisor;
				delta	= samp - fsamp;

				if (ABS(delta) < ABS(data->delta))
				{
					data->fsamp_got	= samp;
					data->delta		= delta;
					data->nvco		= vco;
					data->nref		= ref;
					data->ndiv		= div;
				}
			}
		}
	}

	return(errs);
}



//*****************************************************************************
static int _dsi16wrc_fsamp_compute_w_limits(int fd, fsamp_t* data)
{
	float	delta;
	int		errs;
	float	fgen;
	float	fref;
	int		i;
	s32		mca;
	fsamp_t	tmp;

	for (i = 0; i < 4; i++)
	{
		if (i == 0)
		{
			data->mca	= 0x8000;
			errs		= _dsi16wrc_fsamp_compute_mca(fd, data);
		}
		else
		{
			fgen	= (float) data->fsamp_want * data->ndiv * data->divisor;
			fref	= fgen * data->nref / data->nvco;
			delta	= fref - data->fref;
			delta	= LIMIT_RANGE(delta, -80, 80);
			mca		= (float) 0xFFFF / 160 * delta + 0x8000;
			mca		= LIMIT_RANGE(mca, 0x0000, 0xFFFF);
			tmp		= data[0];
			tmp.mca	= mca;
			errs	= _dsi16wrc_fsamp_compute_mca(fd, &tmp);

			if (ABS(tmp.delta) < ABS(data->delta))
				data[0]	= tmp;
		}

		if (data->delta == 0)
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _dsi16wrc_fsamp_compute_hi_res(int fd, fsamp_t* data)
{
	int	errs;

	data->mode		= DSI16WRC_ADC_MODE_HI_RES;
	data->fsamp_min	= 200;
	data->fsamp_max	= 52500L;
	data->divisor	= 1024;
	errs			= _dsi16wrc_fsamp_compute_w_limits(fd, data);
	return(errs);
}



//*****************************************************************************
static int _dsi16wrc_fsamp_compute_hi_speed(int fd, fsamp_t* data)
{
	int	errs;

	data->mode		= DSI16WRC_ADC_MODE_HI_SPEED;
	data->fsamp_min	= 400;
	data->fsamp_max	= 105000L;
	data->divisor	= 512;
	errs			= _dsi16wrc_fsamp_compute_w_limits(fd, data);
	return(errs);
}



//*****************************************************************************
static int _dsi16wrc_fsamp_compute_pll(int fd, fsamp_t* data)
{
	int		errs	= 0;
	fsamp_t	res;
	fsamp_t	speed;

	if (data->fsamp_want < 400)
	{
		errs	+= _dsi16wrc_fsamp_compute_hi_res(fd, data);
	}
	else if (data->fsamp_want > 52500)
	{
		errs	+= _dsi16wrc_fsamp_compute_hi_speed(fd, data);
	}
	else
	{
		res		= data[0];
		errs	+= _dsi16wrc_fsamp_compute_hi_res(fd, &res);

		speed	= data[0];
		errs	+= _dsi16wrc_fsamp_compute_hi_speed(fd, &speed);

		if (ABS(res.delta) <= ABS(speed.delta))
			data[0]	= res;
		else
			data[0]	= speed;
	}

	return(errs);
}



//*****************************************************************************
static int _fsamp_report_rate_gen(
	s32*	sps,
	s32		fref,
	s32		mca,
	s32		mode,
	s32		ndiv,
	s32		nref,
	s32		nvco)
{
	int		errs	= 0;
	s32		divisor	= (mode == DSI16WRC_ADC_MODE_HI_RES) ? 1024 : 512;
	float	fgen;
	s32		fsamp;
	float	rate;
	float	ref;

	if (mca >= 0x8000)
		ref	= + (((float) (mca - 0x8000)) / ((float) (0xFFFF - 0x8000))) * 80;
	else
		ref	= - (((float) (0x8000 - mca)) / ((float) (0x8000 - 0x0000))) * 80;

	ref		+= fref;
	fgen	= ref * nvco / nref;
	rate	= fgen / ndiv / divisor;
	rate	+= (rate >= 0) ? +0.5 : -0.5;
	fsamp	= (s32) rate;

	gsc_label_long_comma((long) fsamp);
	printf(" S/S  (Fref ");
	gsc_label_long_comma((long) fref);
	printf(", MCA 0x%lX", (long) mca);
	printf(", %s", (mode == DSI16WRC_ADC_MODE_HI_RES) ? "Hi Res" : "Hi Speed");
	printf(", Nvco %ld", (long) nvco);
	printf(", Nref %ld", (long) nref);
	printf(", Ndiv %ld)\n", (long) ndiv);

	if (sps)
		sps[0]	= fsamp;

	return(errs);
}



//*****************************************************************************
static int _fsamp_report_grp_src(
	s32*	sps,
	s32		g_src,
	s32		fref,
	s32		mca,
	s32		mode,
	s32		ndiv,
	s32		nref,
	s32		nvco)
{
	int	errs	= 0;
	s32	fsamp;

	switch (g_src)
	{
		default:

			fsamp	= 0;
			errs++;
			printf("FAIL <---  (Unrecodnized source option: %ld)\n", (long) g_src);
			break;

		case DSI16WRC_CH_GRP_SRC_RATE_GEN:

			errs	+= _fsamp_report_rate_gen(&fsamp, fref, mca, mode, ndiv, nref, nvco);
			break;

		case DSI16WRC_CH_GRP_SRC_EXTERN:
		case DSI16WRC_CH_GRP_SRC_DIR_EXTERN:

			fsamp	= 0;
			printf("0 S/S  (External Source)\n");
			break;

		case DSI16WRC_CH_GRP_SRC_DISABLE:

			fsamp	= 0;
			printf("0 S/S  (Disabled)\n");
			break;
	}

	if (sps)
		sps[0]	= fsamp;

	return(errs);
}



/******************************************************************************
*
*	Function:	dsi16wrc_fsamp_compute
*
*	Purpose:
*
*		Calculate the best values for the specified sample rate.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		quiet	Work quietly?
*
*		fref	The is the Fref value.
*
*		fsamp	The desired sample rate.
*
*		mode	Put the computed mode here. (Hi Speed vd Hi Res)
*
*		nvco	Put the resulting Nvco value here.
*
*		nref	Put the resulting Nref value here.
*
*		ndiv	Put the resulting Ndiv value here.
*
*		mca		Put the Master Clock Adjust value here.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi16wrc_fsamp_compute(
	int		fd,
	int		index,
	int		quiet,
	s32		fref,
	s32*	fsamp,
	s32*	mode,
	s32*	nvco,
	s32*	nref,
	s32*	ndiv,
	s32*	mca)
{
	fsamp_t	data;
	int		errs	= 0;
	char*	psz;

	if (quiet == 0)
	{
		gsc_label_index("Rate Calculation", index);
		gsc_label_long_comma((long) fsamp[0]);
	}

	errs	+= dsi16wrc_fsamp_validate(fd, fsamp);

	if (errs)
	{
		errs	= 1;

		if (quiet == 0)
			printf("SKIPPED  (errors reported)\n");
	}
	else
	{
		data.fref		= fref;
		data.fsamp_want	= fsamp[0];
		data.fsamp_got	= LONG_MAX;
		data.delta		= LONG_MAX;
		data.mode		= 0;
		data.nvco		= 100;
		data.nref		= 100;
		data.ndiv		= 100;
		data.mca		= 0x8000;

		errs			= _dsi16wrc_fsamp_compute_pll(fd, &data);

		fsamp[0]		= (s32) data.fsamp_got;
		mode[0]			= data.mode;
		nvco[0]			= data.nvco;
		nref[0]			= data.nref;
		ndiv[0]			= data.ndiv;
		mca[0]			= data.mca;

		if (errs == 0)
		{
			printf("  (");
			printf("Ref ");
			gsc_label_long_comma(data.fref);
			printf(", MCA 0x%04lX", (long) data.mca);
			psz	= (data.mode == DSI16WRC_ADC_MODE_HI_RES) ? "Hi Res" : "Hi Speed";
			printf(", %s", psz);
			printf(", Nvco %ld", (long) data.nvco);
			printf(", Nref %ld", (long) data.nref);
			printf(", Ndiv %ld", (long) data.ndiv);
			printf(", Rate %.3f", (float) data.fsamp_got);
			printf(", delta %.3f", (float) data.delta);
			printf(")\n");

			fsamp[0]	= (s32) data.fsamp_got;
			mode[0]		= data.mode;
			nvco[0]		= data.nvco;
			nref[0]		= data.nref;
			ndiv[0]		= data.ndiv;
			mca[0]		= data.mca;
		}
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	dsi16wrc_fsamp_report
*
*	Purpose:
*
*		Determine and report the sample rate for the specified channel.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		chan	The index of the channel of interest.
*
*		sps		Store the sample rate here, if non-NULL.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int dsi16wrc_fsamp_report(int fd, int index, int chan, s32* sps)
{
	char	buf[64];
	int		errs		= 0;
	s32		fref;
	s32		fsamp		= 0;
	s32		g_src;
	s32		g0_src;
	s32		g1_src;
	s32		group;
	s32		mca;		// Master Clock Adjust
	s32		mode;
	s32		ndiv;
	s32		nref;
	s32		nvco;

	sprintf(buf, "Channel %d", chan);
	gsc_label_index(buf, index);

	errs	+= dsi16wrc_channel_group(fd, chan, &group);;
	errs	+= dsi16wrc_ch_grp_0_src_quiet(fd, -1, &g0_src);
	errs	+= dsi16wrc_ch_grp_1_src_quiet(fd, -1, &g1_src);
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_FREF_DEFAULT, &fref);
	errs	+= dsi16wrc_master_clk_adj_quiet(fd, -1, &mca);
	errs	+= dsi16wrc_adc_mode_quiet(fd, -1, &mode);
	errs	+= dsi16wrc_ndiv_quiet(fd, -1, &ndiv);
	errs	+= dsi16wrc_nref_quiet(fd, -1, &nref);
	errs	+= dsi16wrc_nvco_quiet(fd, -1, &nvco);

	g_src	= group ? g1_src : g0_src;
	errs	+= _fsamp_report_grp_src(&fsamp, g_src, fref, mca, mode, ndiv, nref, nvco);

	if (sps)
		sps[0]	= fsamp;

	return(errs);
}



/******************************************************************************
*
*	Function:	dsi16wrc_fsamp_report_all
*
*	Purpose:
*
*		Determine and report the sample rate for all channels.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int dsi16wrc_fsamp_report_all(int fd, int index)
{
	s32	chans;
	int	chan;
	int	errs	= 0;
	s32	sps;
	s32	total	= 0;

	gsc_label_index("Sample Rates", index);
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_CHANNEL_QTY, &chans);

	if (errs == 0)
	{
		printf("\n");
		gsc_label_level_inc();

		for (chan = 0; chan < chans; chan++)
		{
			errs	+= dsi16wrc_fsamp_report(fd, -1, chan, &sps);
			total	+= sps;
		}

		gsc_label("Overall Rate");
		gsc_label_long_comma(total);
		printf(" S/S\n");

		gsc_label_level_dec();
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	dsi16wrc_fsamp_validate
*
*	Purpose:
*
*		Modify the target sample rate, if needed and report the results.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		fsamp	The desired sample rate is given here. We modifiy this if
*				needed.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi16wrc_fsamp_validate(int fd, s32* fsamp)
{
	int	errs	= 0;
	s32	max;
	s32	min;

	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_FSAMP_MIN, &min);
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_FSAMP_MAX, &max);

	if (fsamp[0] < min)
	{
		errs++;
		printf("FAIL <---  (The minimum is ");
		gsc_label_long_comma((long) min);
		printf(" S/S.)\n");
	}
	else if (fsamp[0] > max)
	{
		errs++;
		printf("FAIL <---  (The maximum is ");
		gsc_label_long_comma((long) max);
		printf(" S/S.)\n");
	}

	return(errs);
}


