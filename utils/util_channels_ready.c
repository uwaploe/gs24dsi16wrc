// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/utils/util_channels_ready.c $
// $Rev: 32102 $
// $Date: 2015-06-16 16:29:33 -0500 (Tue, 16 Jun 2015) $

#include <errno.h>
#include <stdio.h>

#include "24dsi16wrc.h"
#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi16wrc_channels_ready
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI16WRC_IOCTL_CHANNELS_READY service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		set		This is the value to apply.
*
*		get		The results are reported here. This may be NULL.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi16wrc_channels_ready(int fd, int index, s32 set, s32* get)
{
	char		buf[128];
	int			errs	= 0;
	const char*	ptr;
	int			status;

	gsc_label_index("Channels Ready", index);
	status	= ioctl(fd, DSI16WRC_IOCTL_CHANNELS_READY, (void*) &set);

	if (status == -1)
	{
		if (errno == ETIMEDOUT)
		{
			set	= DSI16WRC_CHANNELS_READY_NO;
		}
		else
		{
			errs++;
			printf("ERROR: ioctl() failure, errno = %d\n", errno);
		}
	}

	switch (set)
	{
		default:

			errs++;
			ptr	= buf;
			sprintf(buf, "Unrecognized option: 0x%lX", (long) set);
			break;

		case DSI16WRC_CHANNELS_READY_NO:

			ptr	= "No, timed out";
			break;

		case DSI16WRC_CHANNELS_READY_YES:

			ptr	= "Yes";
			break;
	}

	printf("%s  (%s)\n", errs ? "FAIL <---" : "PASS", ptr);

	if (get)
		get[0]	= set;

	return(errs);
}



/******************************************************************************
*
*	Function:	dsi16wrc_channels_ready_quiet
*
*	Purpose:
*
*		Provide a silent wrapper for the DSI16WRC_IOCTL_CHANNELS_READY service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		set		This is the value to apply.
*
*		get		The results are reported here. This may be NULL.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi16wrc_channels_ready_quiet(int fd, s32 set, s32* get)
{
	int	errs	= 0;
	int	status;

	status	= ioctl(fd, DSI16WRC_IOCTL_CHANNELS_READY, (void*) &set);

	if (status == -1)
	{
		if (errno == ETIMEDOUT)
		{
			set	= DSI16WRC_CHANNELS_READY_NO;
		}
		else
		{
			errs++;
			printf("ERROR: ioctl() failure, errno = %d\n", errno);
		}
	}

	if (get)
		get[0]	= set;

	return(errs);
}


