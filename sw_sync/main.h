// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/sw_sync/main.h $
// $Rev: 32584 $
// $Date: 2015-07-04 17:28:47 -0500 (Sat, 04 Jul 2015) $

#ifndef __MAIN_H__
#define __MAIN_H__

#include "24dsi16wrc.h"
#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"
#include "gsc_utils.h"



// #defines	*******************************************************************

#define	VERSION			"1.7"
// 1.7	Updated to use the newer common Linux driver sources.
// 1.6	Additional LINTing.
// 1.5	Corrected the text of an output message.
// 1.4	Corrected "Working" output at completion.
// 1.3	Updated the parameter list to gsc_id_driver().
// 1.2	Split the utility code into two libraries: common and device specific.
// 1.1	Corrected output spelling.
// 1.0	initial release.



// prototypes	***************************************************************

int	sw_sync(int fd);



#endif
