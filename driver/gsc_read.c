// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/gsc_read.c $
// $Rev: 31783 $
// $Date: 2015-06-09 10:42:50 -0500 (Tue, 09 Jun 2015) $

#include "main.h"



// #defines *******************************************************************

#if defined(DEV_SUPPORTS_READ)

#define	_IO_ABORT	GSC_WAIT_IO_RX_ABORT
#define	_IO_DONE	GSC_WAIT_IO_RX_DONE
#define	_IO_ERROR	GSC_WAIT_IO_RX_ERROR
#define	_IO_TIMEOUT	GSC_WAIT_IO_RX_TIMEOUT

#endif


/******************************************************************************
*
*	Function:	_read_work
*
*	Purpose:
*
*		Implement the mode independent working portion of the read()
*		procedure. If a timeout is permitted and called for, we wait a single
*		timer tick and check again.
*
*	Arguments:
*
*		alt		The device data structure.
*
*		usr_buf	The data read is placed here.
*
*		count	The number of bytes requested. This is positive.
*
*		st_end	Timeout at this point in time (in system ticks).
*
*		fn_aval	This is the mode specific function that determines the number
*				of bytes that may be read.
*
*		fn_work	This is the function implementing the mode specific read
*				functionality.
*
*	Returned:
*
*		>= 0	The number of bytes transferred.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

#if defined(DEV_SUPPORTS_READ)
static long _read_work(
	GSC_ALT_STRUCT_T*	alt,
	char*				usr_buf,
	size_t				count,	// bytes
	os_time_tick_t		st_end,	// The "system tick" time that we timeout.
	long				(*fn_aval)(
							GSC_ALT_STRUCT_T*	alt,
							size_t				count),	// bytes
	long				(*fn_work)(
							GSC_ALT_STRUCT_T*	alt,
							char*				buf,
							size_t				count,	// bytes
							os_time_tick_t		st_end))
{
	dev_io_t*	io		= &alt->rx;
	int			ret;
	long		total	= 0;	// The number of bytes transferred.
	long		xfer_did;		// Number of bytes we got this time.
	long		xfer_do;		// Number of bytes to get this time.

	for (;;)
	{
		// See how much data we can transfer.
		xfer_do	= (fn_aval)(alt, count);

		if (xfer_do > (long) io->mem.bytes)
			xfer_do	= io->mem.bytes;	// The size of the transfer buffer.

		if (xfer_do > (long) count)
			xfer_do	= count;			// Try to complete the request.

		// Either transfer the data or see what to do next.

		if (io->abort)
		{
			// We've been told to quit.
			io->abort	= 0;
			gsc_wait_resume_io(alt, _IO_DONE | _IO_ABORT);
			break;
		}

		if (xfer_do > 0)
			xfer_did	= (fn_work)(alt, io->mem.ptr, xfer_do, st_end);
		else
			xfer_did	= 0;

		if (xfer_did < 0)
		{
			// There was a problem.
			total	= xfer_did;
			gsc_wait_resume_io(alt, _IO_DONE | _IO_ERROR);
			break;
		}
		else if (xfer_did > 0)
		{
			// Copy the data to the application buffer.
			ret	= os_mem_copy_to_user(usr_buf, io->mem.ptr, xfer_did);

			if (ret)
			{
				// We couldn't copy out the data.
				total	= -EFAULT;
				gsc_wait_resume_io(alt, _IO_DONE | _IO_ERROR);
				break;
			}

			// House keeping.
			count	-= xfer_did;
			total	+= xfer_did;
			usr_buf	+= xfer_did;
		}

		// Now what do we do?

		if (count == 0)
		{
			// The request has been fulfilled.
			gsc_wait_resume_io(alt, _IO_DONE);
			break;
		}
		else if (io->non_blocking)
		{
			// We can't block.

			if (xfer_did > 0)
			{
				// See if we can transfer more data.
				continue;
			}
			else
			{
				// We can't wait to transfer more data.
				// We have essentially timed out.
				gsc_wait_resume_io(alt, _IO_DONE | _IO_TIMEOUT);
				break;
			}
		}
		else if (os_time_tick_timedout(st_end))
		{
			// We've timed out.
			gsc_wait_resume_io(alt, _IO_DONE | _IO_TIMEOUT);
			break;
		}
		else if (xfer_did)
		{
			// Some data was transferred, so go back for more.
			continue;
		}

		if (io->abort)
		{
			// We've been told to quit.
			io->abort	= 0;
			gsc_wait_resume_io(alt, _IO_DONE | _IO_ABORT);
			break;
		}

		// Wait for 1 timer tick before checking again.
		os_time_tick_sleep(1);
	}

	return(total);
}
#endif



/******************************************************************************
*
*	Function:	gsc_read
*
*	Purpose:
*
*		Implement the read() procedure. Only one call active at a time.
*
*	Arguments:
*
*		alt		The structure for the device to read from.
*
*		buf		The data requested goes here.
*
*		count	The number of bytes requested.
*
*	Returned:
*
*		>= 0	The number of bytes transferred.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

#if defined(DEV_SUPPORTS_READ)
long gsc_read(GSC_ALT_STRUCT_T* alt, void* buf, size_t count)
{
	dev_io_t*		io		= &alt->rx;
	long			ret;
	size_t			samples;
	os_time_tick_t	st_end;	// The "system tick" time that we timeout.

	for (;;)	// We'll use a loop for convenience.
	{
		// Gain exclusive access to the device structure.
		ret	= os_sem_lock(&alt->sem);

		if (ret)
		{
			// We didn't get the lock.
			break;
		}

		// Perform argument validation.

		if (count <= 0)
		{
			// There is no work to do.
			ret	= 0;
			gsc_wait_resume_io(alt, _IO_DONE);
			os_sem_unlock(&alt->sem);
			break;
		}

		if (io->bytes_per_sample == 4)
		{
		}
		else if (io->bytes_per_sample == 2)
		{
		}
		else if (io->bytes_per_sample == 1)
		{
		}
		else
		{
			// This is an internal error.
			ret	= -EINVAL;
			gsc_wait_resume_io(alt, _IO_DONE | _IO_ERROR);
			os_sem_unlock(&alt->sem);
			break;
		}

		if (count % io->bytes_per_sample)
		{
			// Requests must be in sample size increments.
			ret	= -EINVAL;
			gsc_wait_resume_io(alt, _IO_DONE | _IO_ERROR);
			os_sem_unlock(&alt->sem);
			break;
		}

		if (buf == NULL)
		{
			// No buffer provided.
			ret	= -EINVAL;
			gsc_wait_resume_io(alt, _IO_DONE | _IO_ERROR);
			os_sem_unlock(&alt->sem);
			break;
		}

		// Compute the I/O timeout end.
		st_end	= os_time_tick_get() + io->timeout_s * os_time_tick_rate();

		// Transfer access control to the read lock.
		ret	= os_sem_lock(&io->sem);

		if (ret)
		{
			// We didn't get the lock.
			gsc_wait_resume_io(alt, _IO_DONE | _IO_ERROR);
			os_sem_unlock(&alt->sem);
			break;
		}

		os_sem_unlock(&alt->sem);
		ret	= dev_read_startup(alt);

		if (ret)
		{
			// There was a problem.
			gsc_wait_resume_io(alt, _IO_DONE | _IO_ERROR);
			os_sem_unlock(&io->sem);
			break;
		}

		// Perform the operation.
		samples	= count / io->bytes_per_sample;

		if ((samples <= (size_t) io->pio_threshold) ||
			(io->io_mode == GSC_IO_MODE_PIO))
		{
			ret	= _read_work(	alt,
								buf,
								count,	// bytes
								st_end,
								DEV_PIO_READ_AVAILABLE,
								DEV_PIO_READ_WORK);
		}
		else if (io->io_mode == GSC_IO_MODE_DMA)
		{
			ret	= _read_work(	alt,
								buf,
								count,	// bytes
								st_end,
								DEV_DMA_READ_AVAILABLE,
								DEV_DMA_READ_WORK);
		}
		else if (io->io_mode == GSC_IO_MODE_DMDMA)
		{
			ret	= _read_work(	alt,
								buf,
								count,	// bytes
								st_end,
								DEV_DMDMA_READ_AVAILABLE,
								DEV_DMDMA_READ_WORK);
		}
		else
		{
			ret	= -EINVAL;
			gsc_wait_resume_io(alt, _IO_DONE | _IO_ERROR);
		}

		//	Clean up.

		os_sem_unlock(&io->sem);
		break;
	}

	return(ret);
}
#endif



/******************************************************************************
*
*	Function:	gsc_read_abort_active_xfer
*
*	Purpose:
*
*		Abort an active read.
*
*	Arguments:
*
*		alt		The device data structure.
*
*	Returned:
*
*		1		An active read was aborted.
*		0		A read was not in progress.
*
******************************************************************************/

#if defined(DEV_SUPPORTS_READ)
int gsc_read_abort_active_xfer(GSC_ALT_STRUCT_T* alt)
{
	dev_data_t*	dev	= GSC_ALT_DEV_GET(alt);
	int			i;
	int			ret;

	alt->rx.abort	= 1;
	ret				= gsc_dma_abort_active_xfer(dev, &alt->rx);
	i				= os_sem_lock(&alt->rx.sem);
	ret				= ret ? ret : (alt->rx.abort ? 0 : 1);
	alt->rx.abort	= 0;

	if (i == 0)
		os_sem_unlock(&alt->rx.sem);

	return(ret);
}
#endif



/******************************************************************************
*
*	Function:	gsc_read_pio_work_8_bit
*
*	Purpose:
*
*		Perform PIO based reads of 8-bit values.
*
*	Arguments:
*
*		alt		The device data structure.
*
*		buff	The destination for the data read.
*
*		count	The number of bytes to read.
*
*		st_end	The "system ticks" time at which we timeout.
*
*	Returned:
*
*		>= 0	The number of bytes transferred.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

#if defined(DEV_SUPPORTS_READ)
#if defined(GSC_READ_PIO_WORK) || defined(GSC_READ_PIO_WORK_8_BIT)
long gsc_read_pio_work_8_bit(
	GSC_ALT_STRUCT_T*	alt,
	char*				buff,
	size_t				count,
	os_time_tick_t		st_end)
{
	long	qty	= count;
	VADDR_T	src	= alt->rx.io_reg_vaddr;
	u8*		dst	= (u8*) buff;

	for (; count > 0; )
	{
		count	-= 1;
		dst[0]	= os_reg_mem_rx_u8(NULL, src);
		dst++;

		if (alt->rx.timeout_s)
		{
			if (os_time_tick_timedout(st_end))
			{
				// We've timed out.
				break;
			}
		}
	}

	qty	-= count;
	return(qty);
}
#endif
#endif



/******************************************************************************
*
*	Function:	gsc_read_pio_work_16_bit
*
*	Purpose:
*
*		Perform PIO based reads of 16-bit values.
*
*	Arguments:
*
*		alt		The device data structure.
*
*		buff	The destination for the data read.
*
*		count	The number of bytes to read.
*
*		st_end	The "system ticks" time at which we timeout.
*
*	Returned:
*
*		>= 0	The number of bytes transferred.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

#if defined(DEV_SUPPORTS_READ)
#if defined(GSC_READ_PIO_WORK) || defined(GSC_READ_PIO_WORK_16_BIT)
long gsc_read_pio_work_16_bit(
	GSC_ALT_STRUCT_T*	alt,
	char*				buff,
	size_t				count,
	os_time_tick_t		st_end)
{
	long	qty	= count;
	VADDR_T	src	= alt->rx.io_reg_vaddr;
	u16*	dst	= (u16*) buff;

	for (; count > 0; )
	{
		count	-= 2;
		dst[0]	= os_reg_mem_rx_u16(NULL, src);
		dst++;

		if (alt->rx.timeout_s)
		{
			if (os_time_tick_timedout(st_end))
			{
				// We've timed out.
				break;
			}
		}
	}

	qty	-= count;
	return(qty);
}
#endif
#endif



/******************************************************************************
*
*	Function:	gsc_read_pio_work_32_bit
*
*	Purpose:
*
*		Perform PIO based reads of 32-bit values.
*
*	Arguments:
*
*		alt		The device data structure.
*
*		buff	The destination for the data read.
*
*		count	The number of bytes to read.
*
*		st_end	The "system ticks" time at which we timeout.
*
*	Returned:
*
*		>= 0	The number of bytes transferred.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

#if defined(DEV_SUPPORTS_READ)
#if defined(GSC_READ_PIO_WORK) || defined(GSC_READ_PIO_WORK_32_BIT)
long gsc_read_pio_work_32_bit(
	GSC_ALT_STRUCT_T*	alt,
	char*				buff,
	size_t				count,
	os_time_tick_t		st_end)
{
	long	qty	= count;
	VADDR_T	src	= alt->rx.io_reg_vaddr;
	u32*	dst	= (u32*) buff;

	for (; count > 0; )
	{
		count	-= 4;
		dst[0]	= os_reg_mem_rx_u32(NULL, src);
		dst++;

		if (alt->rx.timeout_s)
		{
			if (os_time_tick_timedout(st_end))
			{
				// We've timed out.
				break;
			}
		}
	}

	qty	-= count;
	return(qty);
}
#endif
#endif



/******************************************************************************
*
*	Function:	gsc_read_pio_work
*
*	Purpose:
*
*		Perform PIO based reads of 32, 16 or 8 bit values.
*
*	Arguments:
*
*		alt		The device data structure.
*
*		buff	The destination for the data read.
*
*		count	The number of bytes to read.
*
*		st_end	The "system ticks" time at which we timeout.
*
*	Returned:
*
*		>= 0	The number of bytes transferred.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

#if defined(DEV_SUPPORTS_READ)
#if defined(GSC_READ_PIO_WORK)
long gsc_read_pio_work(
	GSC_ALT_STRUCT_T*	alt,
	char*				buff,
	size_t				count,
	os_time_tick_t		st_end)
{
	long	qty;

	if (alt->rx.bytes_per_sample == 4)
		qty	= gsc_read_pio_work_32_bit(alt, buff, count, st_end);
	else if (alt->rx.bytes_per_sample == 2)
		qty	= gsc_read_pio_work_16_bit(alt, buff, count, st_end);
	else if (alt->rx.bytes_per_sample == 1)
		qty	= gsc_read_pio_work_8_bit(alt, buff, count, st_end);
	else
		qty	= -EINVAL;

	return(qty);
}
#endif
#endif


