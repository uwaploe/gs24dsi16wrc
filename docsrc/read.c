#include <errno.h>
#include <stdio.h>

#include "24dsi16wrc_dsl.h"

int dsi16wrc_dsl_read(int fd, u32* buf, size_t samples)
{
	size_t	bytes;
	int		status;

	bytes	= samples * 4;
	status	= read(fd, buf, bytes);

	if (status == -1)
		printf("ERROR: read() failure, errno = %d\n", errno);
	else
		status	/= 4;

	return(status);
}
