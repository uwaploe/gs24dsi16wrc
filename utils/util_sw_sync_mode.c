// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/utils/util_sw_sync_mode.c $
// $Rev: 32102 $
// $Date: 2015-06-16 16:29:33 -0500 (Tue, 16 Jun 2015) $

#include <stdio.h>

#include "24dsi16wrc.h"
#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi16wrc_sw_sync_mode
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI16WRC_IOCTL_SW_SYNC_MODE service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		set		This is the value to apply.
*
*		get		The results are reported here. This may be NULL.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi16wrc_sw_sync_mode(int fd, int index, s32 set, s32* get)
{
	char		buf[128];
	int			errs;
	const char*	ptr;

	gsc_label_index("SW Sync Mode", index);
	errs	= dsi16wrc_dsl_ioctl(fd, DSI16WRC_IOCTL_SW_SYNC_MODE, &set);

	switch (set)
	{
		default:

			errs++;
			ptr	= buf;
			sprintf(buf, "Unrecognized option: 0x%lX", (long) set);
			break;

		case DSI16WRC_SW_SYNC_MODE_SYNC:

			ptr	= "SYNC";
			break;

		case DSI16WRC_SW_SYNC_MODE_CLR_BUF:

			ptr	= "Clear Buffer";
			break;
	}

	printf("%s  (%s)\n", errs ? "FAIL <---" : "PASS", ptr);

	if (get)
		get[0]	= set;

	return(errs);
}



/******************************************************************************
*
*	Function:	dsi16wrc_sw_sync_mode_quiet
*
*	Purpose:
*
*		Provide a silent wrapper for the DSI16WRC_IOCTL_SW_SYNC_MODE service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		set		This is the value to apply.
*
*		get		The results are reported here. This may be NULL.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi16wrc_sw_sync_mode_quiet(int fd, s32 set, s32* get)
{
	int	errs;

	errs	= dsi16wrc_dsl_ioctl(fd, DSI16WRC_IOCTL_SW_SYNC_MODE, &set);

	if (get)
		get[0]	= set;

	return(errs);
}


