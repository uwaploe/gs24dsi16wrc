// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/docsrc/24dsi16wrc_dsl.h $
// $Rev: 32452 $
// $Date: 2015-07-04 14:44:52 -0500 (Sat, 04 Jul 2015) $

#ifndef __24DSI16WRC_DSL_H__
#define __24DSI16WRC_DSL_H__

#include "24dsi16wrc.h"



// prototypes	***************************************************************

int	dsi16wrc_dsl_close(int fd);
int	dsi16wrc_dsl_ioctl(int fd, int request, void *arg);
int	dsi16wrc_dsl_open(unsigned int board);
int	dsi16wrc_dsl_read(int fd, u32* buf, size_t samples);



#endif
