// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/sw_sync/sw_sync.c $
// $Rev: 32584 $
// $Date: 2015-07-04 17:28:47 -0500 (Sat, 04 Jul 2015) $

#include <errno.h>
#include <stdio.h>

#include "main.h"



/******************************************************************************
*
*	Function:	sw_sync
*
*		Repetatively generate a SW Sync pulse.
*
*	Arguments:
*
*		fd		The handle to the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int sw_sync(int fd)
{
	char	buf[64];
	time_t	end;
	int		errs	= 0;
	s32		fref;
	time_t	now;
	int		seconds	= 15;
	int		test;

	errs	+= dsi16wrc_initialize			(fd, -1);
	errs	+= dsi16wrc_control_mode		(fd, -1, DSI16WRC_CONTROL_MODE_INITIATOR, NULL);
	errs	+= dsi16wrc_xcvr_type			(fd, -1, DSI16WRC_XCVR_TYPE_TTL, NULL);
	errs	+= dsi16wrc_query_fref_default	(fd, -1, &fref);
	errs	+= dsi16wrc_master_clk_adj		(fd, -1, 0x8000, NULL);
	errs	+= dsi16wrc_nvco				(fd, -1, 30, NULL);
	errs	+= dsi16wrc_nref				(fd, -1, 30, NULL);
	errs	+= dsi16wrc_ext_clk_src			(fd, -1, DSI16WRC_EXT_CLK_SRC_GEN, NULL);

	sprintf(buf, "Working (%d seconds)", seconds);
	gsc_label(buf);
	end	= time(NULL) + seconds;

	for (; errs == 0;)
	{
		test	= dsi16wrc_dsl_ioctl(fd, DSI16WRC_IOCTL_SW_SYNC, NULL);
		errs	+= (test == -1) ? 1 : 0;
		now		= time(NULL);

		if (now > end)
			break;

		usleep(2);
	}

	if (errs == 0)
		printf("PASS\n");

	return(errs);
}



