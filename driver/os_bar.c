// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/linux/os_bar.c $
// $Rev: 25705 $
// $Date: 2014-03-31 16:48:39 -0500 (Mon, 31 Mar 2014) $

// Linux driver module

#include "main.h"



//*****************************************************************************
static int _bar_reqion_io_acquire(os_bar_t* bar)
{
	int		i;
	int		ret;
	void*	vp;

	for (;;)	// A convenience loop.
	{
		if ((bar->flags & D0) == 0)
		{
			// The region is not I/O mapped.
			ret	= -EINVAL;
			break;
		}

		i	= REGION_IO_CHECK(bar->phys_adrs, bar->size);

		if (i)
		{
			// This BAR region is already in use.
			ret	= -EALREADY;
			break;
		}

		bar->requested	= 1;

		vp	= REGION_IO_REQUEST(bar->phys_adrs, bar->size, DEV_NAME);

		if (vp == NULL)
		{
			// This BAR region request failed.
			ret	= -EFAULT;
			break;
		}

		// All went well.
		ret				= 0;
		bar->vaddr		= (VADDR_T) bar->phys_adrs;
		break;
	}

	return(ret);
}




//*****************************************************************************
static int _bar_reqion_mem_acquire(os_bar_t* bar)
{
	int		i;
	int		ret;
	void*	vp;

	for (;;)	// A convenience loop.
	{
		if (bar->flags & D0)
		{
			// The region is not memory mapped.
			ret	= -EINVAL;
			break;
		}

		i	= REGION_MEM_CHECK(bar->phys_adrs, bar->size);

		if (i)
		{
			// This BAR region is already in use.
			ret	= -EALREADY;
			break;
		}

		bar->requested	= 1;

		vp	= (void*) REGION_MEM_REQUEST(bar->phys_adrs, bar->size, DEV_NAME);

		if (vp == NULL)
		{
			// This BAR region request failed.
			ret	= EFAULT;
			break;
		}

		// All went well.
		ret			= 0;
		bar->vaddr	= (VADDR_T) ioremap(bar->phys_adrs, bar->size);
		break;
	}

	return(ret);
}



//*****************************************************************************
int os_bar_region_acquire(os_bar_t* bar)
{
	int	ret;

	if (bar->io_mapped)
		ret	= _bar_reqion_io_acquire(bar);
	else
		ret	= _bar_reqion_mem_acquire(bar);

	return(ret);
}



//*****************************************************************************
void os_bar_region_release(os_bar_t* bar)
{
	if (bar->requested == 0)
	{
	}
	else if (bar->io_mapped)
	{
		REGION_IO_RELEASE(bar->phys_adrs, bar->size);
		bar->requested	= 0;
		bar->vaddr		= 0;
	}
	else
	{
		if (bar->vaddr)
			iounmap((void*) bar->vaddr);

		REGION_MEM_RELEASE(bar->phys_adrs, bar->size);
		bar->requested	= 0;
		bar->vaddr		= 0;
	}
}


