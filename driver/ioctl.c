// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/driver/ioctl.c $
// $Rev: 32062 $
// $Date: 2015-06-15 19:38:02 -0500 (Mon, 15 Jun 2015) $

#include "main.h"



// data types *****************************************************************

typedef struct
{
	int	sw;
	int	hw;
} sw_hw_t;



//*****************************************************************************
static void _bctlr_mod(dev_data_t* dev, u32 value, u32 mask)
{
	if (mask != BCTLR_IRQ_REQUEST)
	{
		value	|= BCTLR_IRQ_REQUEST;
		mask	|= BCTLR_IRQ_REQUEST;
	}

	os_reg_mem_mx_u32(dev, dev->vaddr.gsc_bctlr_32, value, mask);
}



//*****************************************************************************
static int _wait_for_ready(dev_data_t* dev, long ms_limit)
{
	int	ret;

	ret	= gsc_poll_u32(dev, ms_limit, dev->vaddr.gsc_bctlr_32, D13, D13);
	return(ret);
}



//*****************************************************************************
static int _xlat_hw_2_sw(s32* arg, const sw_hw_t* list)
{
	int	i;
	int	ret	= 0;

	for (i = 0;; i++)
	{
		if (list[i].sw < 0)
		{
			ret	= -EINVAL;
			break;
		}

		if (list[i].hw == arg[0])
		{
			arg[0]	= list[i].sw;
			break;
		}
	}

	return(ret);
}



//*****************************************************************************
static int _xlat_sw_2_hw(s32* arg, const sw_hw_t* list)
{
	int	i;
	int	ret	= 0;

	if (arg[0] == -1)
	{
	}
	else
	{
		for (i = 0;; i++)
		{
			if (list[i].sw < 0)
			{
				ret	= -EINVAL;
				break;
			}

			if (list[i].sw == arg[0])
			{
				arg[0]	= list[i].hw;
				break;
			}
		}
	}

	return(ret);
}



//*****************************************************************************
static int _query(dev_data_t* dev, void* arg)
{
	int		ret	= 0;
	s32*	ptr	= (s32*) arg;

	switch (ptr[0])
	{
		default:	ptr[0]	= DSI16WRC_IOCTL_QUERY_ERROR;
					ret		= -EINVAL;
					break;

		case DSI16WRC_QUERY_AUTO_CAL_MS:	ptr[0]	= dev->cache.auto_cal_ms;	break;
		case DSI16WRC_QUERY_CHANNEL_MAX:	ptr[0]	= dev->cache.channels_max;	break;
		case DSI16WRC_QUERY_CHANNEL_QTY:	ptr[0]	= dev->cache.channel_qty;	break;
		case DSI16WRC_QUERY_COUNT:			ptr[0]	= DSI16WRC_QUERY_LAST;		break;
		case DSI16WRC_QUERY_DEVICE_TYPE:	ptr[0]	= dev->board_type;			break;
		case DSI16WRC_QUERY_FGEN_MAX:		ptr[0]	= dev->cache.fgen_max;		break;
		case DSI16WRC_QUERY_FGEN_MIN:		ptr[0]	= dev->cache.fgen_min;		break;
		case DSI16WRC_QUERY_FIFO_SIZE:		ptr[0]	= dev->cache.fifo_size;		break;
		case DSI16WRC_QUERY_FILTER_FREQ:	ptr[0]	= dev->cache.filter_freq;	break;
		case DSI16WRC_QUERY_FREF_DEFAULT:	ptr[0]	= dev->cache.fref_default;	break;
		case DSI16WRC_QUERY_FSAMP_MAX:		ptr[0]	= dev->cache.fsamp_max;		break;
		case DSI16WRC_QUERY_FSAMP_MIN:		ptr[0]	= dev->cache.fsamp_min;		break;
		case DSI16WRC_QUERY_INIT_MS:		ptr[0]	= dev->cache.initialize_ms;	break;
		case DSI16WRC_QUERY_NDIV_MAX:		ptr[0]	= dev->cache.ndiv_max;		break;
		case DSI16WRC_QUERY_NDIV_MIN:		ptr[0]	= dev->cache.ndiv_min;		break;
		case DSI16WRC_QUERY_NREF_MAX:		ptr[0]	= dev->cache.nref_max;		break;
		case DSI16WRC_QUERY_NREF_MIN:		ptr[0]	= dev->cache.nref_min;		break;
		case DSI16WRC_QUERY_NVCO_MAX:		ptr[0]	= dev->cache.nvco_max;		break;
		case DSI16WRC_QUERY_NVCO_MIN:		ptr[0]	= dev->cache.nvco_min;		break;
		case DSI16WRC_QUERY_V_RANGE:		ptr[0]	= dev->cache.voltage_range;	break;
	}

	return(ret);
}



//*****************************************************************************
int initialize_ioctl(dev_data_t* dev, void* arg)
{
	long	ms;
	u32		reg;
	int		ret;

	// Manually wait for completion to insure that initialization completes
	// even if we get a signal, such as ^c.
	os_reg_mem_tx_u32(dev, dev->vaddr.gsc_bctlr_32, INIT_START);
	ms	= dev->cache.initialize_ms + 5000;
	ret	= gsc_poll_u32(dev, ms, dev->vaddr.gsc_bctlr_32, INIT_START, 0);
	reg	= os_reg_mem_rx_u32(dev, dev->vaddr.gsc_bctlr_32);

	if (ret)
	{
		printk(	"%s: INITIALIZATION TIMED OUT (%ld ms)\n",
				dev->model,
				(long) ms);
	}

	if (reg & INIT_START)
	{
		ret	= -EIO;
		printk(	"%s: INITIALIZATION DID NOT COMPLETE (%ld ms)\n",
				dev->model,
				(long) ms);
	}

	// Wait for settling.
	_wait_for_ready(dev, 20);

	// Initialize the software settings.
	dev->rx.bytes_per_sample	= 4;
	dev->rx.io_mode				= DSI16WRC_IO_MODE_DEFAULT;
	dev->rx.overflow_check		= DSI16WRC_IO_OVERFLOW_DEFAULT;
	dev->rx.pio_threshold		= 32;
	dev->rx.timeout_s			= DSI16WRC_IO_TIMEOUT_DEFAULT;
	dev->rx.underflow_check		= DSI16WRC_IO_UNDERFLOW_DEFAULT;
	return(ret);
}



//*****************************************************************************
static int _auto_cal_start(dev_data_t* dev, unsigned long arg)
{
	// Initiate auto-calibration.
	_bctlr_mod(NULL, BCTLR_AUTO_CAL_START, BCTLR_AUTO_CAL_START);
	return(0);
}



//*****************************************************************************
static int _auto_calibrate(dev_data_t* dev, void* arg)
{
	long		ms;
	u32			reg;
	int			ret		= 0;
	os_sem_t	sem;
	int			test;
	gsc_wait_t	wt;

	// Safely select the Auto-Cal Done interrupt.
	_bctlr_mod(dev, BCTLR_IRQ_AUTO_CAL_DONE, BCTLR_IRQ_MASK);
	_bctlr_mod(dev, 0, BCTLR_IRQ_REQUEST);

	// Wait for the local interrupt.
	os_sem_create(&sem);	// dummy, required for wait operations.
	ms				= dev->cache.auto_cal_ms + 5000;
	memset(&wt, 0, sizeof(wt));
	wt.flags		= GSC_WAIT_FLAG_INTERNAL;
	wt.gsc			= DSI16WRC_WAIT_GSC_AUTO_CAL_DONE;
	wt.timeout_ms	= ms;
	test			= gsc_wait_event(dev, &wt, _auto_cal_start, (unsigned long) NULL, &sem);
	os_sem_destroy(&sem);

	if (wt.flags & GSC_WAIT_FLAG_TIMEOUT)
	{
		ret	= test ? test : -ETIMEDOUT;
		printk(	"%s: AUTO-CALIBRATE TIMED OUT (%ld ms).\n",
				dev->model,
				(long) ms);
	}

	// Manually wait for completion in case we got a signal or other event.
	test	= gsc_poll_u32(dev, ms, dev->vaddr.gsc_bctlr_32, BCTLR_AUTO_CAL_START, 0);

	if (test)
	{
		ret	= ret ? ret : (test ? test : -ETIMEDOUT);
		printk(	"%s: AUTO-CALIBRATE DID NOT COMPLETE: %ld ms\n",
				dev->model,
				ms);
	}

	// Check the completion status.
	reg	= os_reg_mem_rx_u32(dev, dev->vaddr.gsc_bctlr_32);

	if (ret)
	{
	}
	else if (reg & BCTLR_AUTO_CAL_START)
	{
		ret	= ret ? ret : -EIO;
		printk(	"%s: AUTO-CALIBRATION DID NOT COMPLETE (%ld ms)\n",
				dev->model,
				(long) ms);
	}
	else if ((reg & BCTLR_AUTO_CAL_PASS) == 0)
	{
		ret	= ret ? ret : -EIO;
		printk(	"%s: AUTO-CALIBRATION FAILED (%ld ms)\n",
				dev->model,
				(long) ms);
	}
	else
	{
		// Wait for settling.
		os_time_sleep_ms(10);
	}

	return(ret);
}



//*****************************************************************************
static int _auto_cal_sts(dev_data_t* dev, s32* arg)
{
	u32	reg;

	reg	= os_reg_mem_rx_u32(dev, dev->vaddr.gsc_bctlr_32);

	if (reg & BCTLR_AUTO_CAL_START)
		arg[0]	= DSI16WRC_AUTO_CAL_STS_ACTIVE;
	else if (reg & BCTLR_AUTO_CAL_PASS)
		arg[0]	= DSI16WRC_AUTO_CAL_STS_PASS;
	else
		arg[0]	= DSI16WRC_AUTO_CAL_STS_FAIL;

	return(0);
}



//*****************************************************************************
static int _adc_mode(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_ADC_MODE_HI_RES,
		DSI16WRC_ADC_MODE_HI_SPEED,
		-1	// terminate list
	};

	int	ret;

	ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_bctlr_32, 19, 19);

	// Wait for settling.
	_wait_for_ready(dev, 700);
	return(ret);
}



//*****************************************************************************
static int _ai_buf_clear(dev_data_t* dev, void* arg)
{
	os_reg_mem_mx_u32(dev, dev->vaddr.gsc_bufcr_32, D20, D20 | D24 | D25);

	// Wait for settling.
	_wait_for_ready(dev, 20);
	return(0);
}



//*****************************************************************************
static int _ai_buf_enable(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_AI_BUF_ENABLE_NO,
		DSI16WRC_AI_BUF_ENABLE_YES,
		-1	// terminate list
	};

	int	ret;

	ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_bufcr_32, 19, 19);
	return(ret);
}



//*****************************************************************************
static int _ai_buf_fill_lvl(dev_data_t* dev, s32* arg)
{
	int	ret;

	arg[0]	= -1;
	ret		= gsc_s32_range_reg(dev, arg, 0, 0x40000, dev->vaddr.gsc_bufsr_32, 18, 0);
	return(ret);
}



//*****************************************************************************
static int _ai_buf_overflow(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_AI_BUF_UNDERFLOW_NO,
		DSI16WRC_AI_BUF_UNDERFLOW_YES,
		-1	// terminate list
	};

	int	ret;

	if (arg[0] == 1)
		ret	= -EINVAL;
	else
		ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_bufcr_32, 24, 24);

	return(ret);
}



//*****************************************************************************
static int _ai_buf_thresh(dev_data_t* dev, void* arg)
{
	int	ret;

	ret		= gsc_s32_range_reg(dev, arg, 0, 0x40000, dev->vaddr.gsc_bufcr_32, 18, 0);
	return(ret);
}



//*****************************************************************************
static int _ai_buf_thr_sts(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_AI_BUF_THR_STS_IDLE,
		DSI16WRC_AI_BUF_THR_STS_ACTIVE,
		-1	// terminate list
	};

	int	ret;

	arg[0]	= -1;
	ret		= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_bctlr_32, 14, 14);
	return(ret);
}



//*****************************************************************************
static int _ai_buf_underflow(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_AI_BUF_OVERFLOW_NO,
		DSI16WRC_AI_BUF_OVERFLOW_YES,
		-1	// terminate list
	};

	int	ret;

	if (arg[0] == 1)
		ret	= -EINVAL;
	else
		ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_bufcr_32, 25, 25);

	return(ret);
}



//*****************************************************************************
static int _ai_channel_tag(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_AI_CHANNEL_TAG_ENABLE,
		DSI16WRC_AI_CHANNEL_TAG_DISABLE,
		-1	// terminate list
	};

	int	ret;

	ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_bufcr_32, 23, 23);
	return(ret);
}



//*****************************************************************************
static int _ai_mode(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_AI_MODE_DIFF,
		DSI16WRC_AI_MODE_ZERO,
		DSI16WRC_AI_MODE_VREF,
		-1	// terminate list
	};

	int	ret;

	ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_bctlr_32, 1, 0);
	return(ret);
}



//*****************************************************************************
static int _aux_clk_ctl_mode(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_AUX_CLK_CTL_MODE_INACTIVE,
		DSI16WRC_AUX_CLK_CTL_MODE_INPUT,
		DSI16WRC_AUX_CLK_CTL_MODE_OUTPUT,
		-1	// terminate list
	};

	int	ret;

	ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_asiocr_32, 1, 0);
	return(ret);
}



//*****************************************************************************
static int _aux_sync_ctl_mode(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_AUX_SYNC_CTL_MODE_INACTIVE,
		DSI16WRC_AUX_SYNC_CTL_MODE_INPUT,
		DSI16WRC_AUX_SYNC_CTL_MODE_OUTPUT,
		-1	// terminate list
	};

	int	ret;

	ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_asiocr_32, 3, 2);
	return(ret);
}



//*****************************************************************************
static int _burst(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_BURST_DISABLE,
		DSI16WRC_BURST_ENABLE,
		-1	// terminate list
	};

	int	ret;

	ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_bctlr_32, 21, 21);
	return(ret);
}



//*****************************************************************************
static int _burst_rate_div(dev_data_t* dev, s32* arg)
{
	int	ret;

	ret	= gsc_s32_range_reg(dev, arg, 0, 0xFFFFFF, dev->vaddr.gsc_bttr_32, 23, 0);
	return(ret);
}



//*****************************************************************************
static int _burst_size(dev_data_t* dev, s32* arg)
{
	int	ret;

	ret	= gsc_s32_range_reg(dev, arg, 0, 0xFFFFFF, dev->vaddr.gsc_bbsr_32, 23, 0);
	return(ret);
}



//*****************************************************************************
static int _burst_timer(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_BURST_TIMER_DISABLE,
		DSI16WRC_BURST_TIMER_ENABLE,
		-1	// terminate list
	};

	int	ret;

	ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_bctlr_32, 16, 16);
	return(ret);
}



//*****************************************************************************
static int _burst_trigger(dev_data_t* dev, void* arg)
{
	os_reg_mem_mx_u32(dev, dev->vaddr.gsc_bctlr_32, D22, D22);

	// Wait for settling.
	_wait_for_ready(dev, 20);
	return(0);
}



//*****************************************************************************
static int _ch_grp_0_src(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_CH_GRP_SRC_RATE_GEN,
		DSI16WRC_CH_GRP_SRC_EXTERN,
		DSI16WRC_CH_GRP_SRC_DIR_EXTERN,
		DSI16WRC_CH_GRP_SRC_DISABLE,
		-1	// terminate list
	};

	int	ret;

	ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_csar_32, 3, 0);

	// Wait for settling.
	_wait_for_ready(dev, 20);
	return(ret);
}



//*****************************************************************************
static int _ch_grp_1_src(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_CH_GRP_SRC_RATE_GEN,
		DSI16WRC_CH_GRP_SRC_EXTERN,
		DSI16WRC_CH_GRP_SRC_DIR_EXTERN,
		DSI16WRC_CH_GRP_SRC_DISABLE,
		-1	// terminate list
	};

	s32	g0	= -1;
	s32	g1	= -1;
	int	ret;
	int	ret0;
	int	ret1;

	if (arg[0] == -1)
	{
		ret0	= gsc_s32_list_reg(dev, &g0, options, dev->vaddr.gsc_csar_32, 3, 0);
		ret1	= gsc_s32_list_reg(dev, &g1, options, dev->vaddr.gsc_csar_32, 7, 4);

		arg[0]	= (g1 == DSI16WRC_CH_GRP_SRC_DISABLE) ? g1 : g0;
		ret		= ret0 ? ret0 : ret1;
	}
	else
	{
		ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_csar_32, 7, 4);
	}

	// Wait for settling.
	_wait_for_ready(dev, 20);
	return(ret);
}



//*****************************************************************************
static int _channels_ready(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_CHANNELS_READY_NO,
		DSI16WRC_CHANNELS_READY_YES,
		-1	// terminate list
	};

	int	ret	= 0;

	if (arg[0] == DSI16WRC_CHANNELS_READY_WAIT)
	{
		gsc_poll_u32(dev, 1000, dev->vaddr.gsc_bctlr_32, D13, D13);
		arg[0]	= DSI16WRC_CHANNELS_READY_TEST;
	}

	if (arg[0] == DSI16WRC_CHANNELS_READY_TEST)
		ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_bctlr_32, 13, 13);
	else
		ret	= -EINVAL;

	return(ret);
}



//*****************************************************************************
static int _control_mode(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_CONTROL_MODE_TARGET,
		DSI16WRC_CONTROL_MODE_INITIATOR,
		-1	// terminate list
	};

	int	ret;

	ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_bctlr_32, 5, 5);
	return(ret);
}



//*****************************************************************************
static int _data_format(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_DATA_FORMAT_2S_COMP,
		DSI16WRC_DATA_FORMAT_OFF_BIN,
		-1	// terminate list
	};

	int	ret;

	ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_bctlr_32, 4, 4);
	return(ret);
}



//*****************************************************************************
static int _data_width(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_DATA_WIDTH_16,
		DSI16WRC_DATA_WIDTH_18,
		DSI16WRC_DATA_WIDTH_20,
		DSI16WRC_DATA_WIDTH_24,
		-1	// terminate list
	};

	int	ret;

	ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_bufcr_32, 22, 21);
	return(ret);
}



//*****************************************************************************
static int _dio_dir_out(dev_data_t* dev, s32* arg)
{
	int	ret;

	ret	= gsc_s32_range_reg(dev, arg, 0, 0xF, dev->vaddr.gsc_diopr_32, 11, 8);
	return(ret);
}



//*****************************************************************************
static int _dio_read(dev_data_t* dev, s32* arg)
{
	int	ret;

	arg[0]	= -1;
	ret		= gsc_s32_range_reg(dev, arg, 0, 0xF, dev->vaddr.gsc_diopr_32, 3, 0);
	return(ret);
}



//*****************************************************************************
static int _dio_write(dev_data_t* dev, s32* arg)
{
	int	ret;

	ret	= gsc_s32_range_reg(dev, arg, 0, 0xF, dev->vaddr.gsc_diopr_32, 3, 0);
	return(ret);
}



//*****************************************************************************
static int _ext_clk_src(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_EXT_CLK_SRC_GRP_0,
		DSI16WRC_EXT_CLK_SRC_GEN,
		-1	// terminate list
	};

	int	ret;

	ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_bctlr_32, 18, 18);
	return(ret);
}



//*****************************************************************************
static int _irq_sel(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_IRQ_INIT_DONE,
		DSI16WRC_IRQ_AUTO_CAL_DONE,
		DSI16WRC_IRQ_CHAN_READY,
		DSI16WRC_IRQ_AI_BUF_THRESH_L2H,
		DSI16WRC_IRQ_AI_BUF_THRESH_H2L,
		DSI16WRC_IRQ_AI_BURST_DONE,
		-1	// terminate list
	};

	int	ret;

	ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_bctlr_32, 10, 8);
	return(ret);
}



//*****************************************************************************
static int _master_clk_adj(dev_data_t* dev, void* arg)
{
	int	ret;

	ret		= gsc_s32_range_reg(dev, arg, 0, 0xFFFF, dev->vaddr.gsc_mcar_32, 15, 0);
	return(ret);
}



//*****************************************************************************
static int _ndiv(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= gsc_s32_range_reg(dev, arg, 1, 300, dev->vaddr.gsc_rdr_32, 8, 0);

	// Wait for settling.
	_wait_for_ready(dev, 20);
	return(ret);
}



//*****************************************************************************
static int _nref(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= gsc_s32_range_reg(dev, arg, 25, 300, dev->vaddr.gsc_rcr_32, 23, 12);

	// Wait for settling.
	_wait_for_ready(dev, 20);
	return(ret);
}



//*****************************************************************************
static int _nvco(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= gsc_s32_range_reg(dev, arg, 25, 300, dev->vaddr.gsc_rcr_32, 11, 0);

	// Wait for settling.
	_wait_for_ready(dev, 20);
	return(ret);
}



//*****************************************************************************
static int _range(dev_data_t* dev, s32* arg)
{
	static const sw_hw_t	list_10_by_2[]	=
	{
		// sw					hw
		{ DSI16WRC_RANGE_1_25V,	0	},
		{ DSI16WRC_RANGE_2_5V,	1	},
		{ DSI16WRC_RANGE_5V,	2	},
		{ DSI16WRC_RANGE_10V,	3	},
		{ -1, -1, }
	};

	static const sw_hw_t	list_10_by_10[]	=
	{
		// sw					hw
		{ DSI16WRC_RANGE_10MV,	0	},
		{ DSI16WRC_RANGE_100MV,	1	},
		{ DSI16WRC_RANGE_1V,	2	},
		{ DSI16WRC_RANGE_10V,	3	},
		{ -1, -1, }
	};

	const sw_hw_t*	list;
	int				ret	= 0;

	if (dev->cache.voltage_range == DSI16WRC_QUERY_V_RANGE_10_BY_2)
		list	= list_10_by_2;
	else
		list	= list_10_by_10;

	if (arg[0] == -1)
	{
		ret	= gsc_s32_range_reg(dev, arg, 0, 3, dev->vaddr.gsc_bctlr_32, 3, 2);
		_xlat_hw_2_sw(arg, list);
	}
	else
	{
		ret	= _xlat_sw_2_hw(arg, list);

		if (ret == 0)
			gsc_s32_range_reg(dev, arg, 0, 3, dev->vaddr.gsc_bctlr_32, 3, 2);

		_xlat_hw_2_sw(arg, list);
	}

	return(ret);
}



//*****************************************************************************
static int _rx_io_abort(dev_data_t* dev, s32* arg)
{
	arg[0]	= gsc_read_abort_active_xfer(dev);
	return(0);
}



//*****************************************************************************
static int _rx_io_mode(dev_data_t* dev, s32* arg)
{
	static const s32	list[]	=
	{
		GSC_IO_MODE_PIO,
		GSC_IO_MODE_DMA,
		GSC_IO_MODE_DMDMA,
		-1
	};

	int	ret;

	ret	= gsc_s32_list_var(arg, list, &dev->rx.io_mode);
	return(ret);
}



//*****************************************************************************
static int _rx_io_overflow(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_IO_OVERFLOW_IGNORE,
		DSI16WRC_IO_OVERFLOW_CHECK,
		-1	// terminate list
	};

	int	ret;

	ret	= gsc_s32_list_var(arg, options, &dev->rx.overflow_check);
	return(ret);
}



//*****************************************************************************
static int _rx_io_timeout(dev_data_t* dev, s32* arg)
{
	int	ret;

	ret	= gsc_s32_range_var(
			arg,
			DSI16WRC_IO_TIMEOUT_MIN,
			DSI16WRC_IO_TIMEOUT_MAX,
			&dev->rx.timeout_s);
	return(ret);
}



//*****************************************************************************
static int _rx_io_underflow(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_IO_UNDERFLOW_IGNORE,
		DSI16WRC_IO_UNDERFLOW_CHECK,
		-1	// terminate list
	};

	int	ret;

	ret	= gsc_s32_list_var(arg, options, &dev->rx.underflow_check);
	return(ret);
}



//*****************************************************************************
static int _sw_sync(dev_data_t* dev, void* arg)
{
	_bctlr_mod(dev, D6, D6);
	return(0);
}



//*****************************************************************************
static int _sw_sync_mode(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_SW_SYNC_MODE_SYNC,
		DSI16WRC_SW_SYNC_MODE_CLR_BUF,
		-1	// terminate list
	};

	int	ret;

	ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_bctlr_32, 17, 17);
	return(ret);
}



//*****************************************************************************
static int _xcvr_type(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI16WRC_XCVR_TYPE_LVDS,
		DSI16WRC_XCVR_TYPE_TTL,
		-1	// terminate list
	};

	int	ret;

	ret	= gsc_s32_list_reg(dev, arg, options, dev->vaddr.gsc_bctlr_32, 20, 20);
	return(ret);
}



// variables	***************************************************************

const gsc_ioctl_t	dev_ioctl_list[]	=
{
	{ DSI16WRC_IOCTL_REG_READ,			(void*) gsc_reg_read_ioctl		},
	{ DSI16WRC_IOCTL_REG_WRITE,			(void*) gsc_reg_write_ioctl		},
	{ DSI16WRC_IOCTL_REG_MOD,			(void*) gsc_reg_mod_ioctl		},
	{ DSI16WRC_IOCTL_QUERY,				(void*) _query					},
	{ DSI16WRC_IOCTL_INITIALIZE,		(void*) initialize_ioctl		},
	{ DSI16WRC_IOCTL_AUTO_CALIBRATE,	(void*) _auto_calibrate			},
	{ DSI16WRC_IOCTL_AUTO_CAL_STS,		(void*) _auto_cal_sts			},
	{ DSI16WRC_IOCTL_ADC_MODE,			(void*) _adc_mode				},
	{ DSI16WRC_IOCTL_AI_BUF_CLEAR,		(void*) _ai_buf_clear			},
	{ DSI16WRC_IOCTL_AI_BUF_ENABLE,		(void*) _ai_buf_enable			},
	{ DSI16WRC_IOCTL_AI_BUF_FILL_LVL,	(void*) _ai_buf_fill_lvl		},
	{ DSI16WRC_IOCTL_AI_BUF_OVERFLOW,	(void*) _ai_buf_overflow		},
	{ DSI16WRC_IOCTL_AI_BUF_THRESH,		(void*) _ai_buf_thresh			},
	{ DSI16WRC_IOCTL_AI_BUF_THR_STS,	(void*) _ai_buf_thr_sts			},
	{ DSI16WRC_IOCTL_AI_BUF_UNDERFLOW,	(void*) _ai_buf_underflow		},
	{ DSI16WRC_IOCTL_AI_CHANNEL_TAG,	(void*) _ai_channel_tag			},
	{ DSI16WRC_IOCTL_AI_MODE,			(void*) _ai_mode				},
	{ DSI16WRC_IOCTL_AUX_CLK_CTL_MODE,	(void*) _aux_clk_ctl_mode		},
	{ DSI16WRC_IOCTL_AUX_SYNC_CTL_MODE,	(void*) _aux_sync_ctl_mode		},
	{ DSI16WRC_IOCTL_BURST,				(void*) _burst					},
	{ DSI16WRC_IOCTL_BURST_RATE_DIV,	(void*) _burst_rate_div			},
	{ DSI16WRC_IOCTL_BURST_SIZE,		(void*) _burst_size				},
	{ DSI16WRC_IOCTL_BURST_TIMER,		(void*) _burst_timer			},
	{ DSI16WRC_IOCTL_BURST_TRIGGER,		(void*) _burst_trigger			},
	{ DSI16WRC_IOCTL_CH_GRP_0_SRC,		(void*) _ch_grp_0_src			},
	{ DSI16WRC_IOCTL_CH_GRP_1_SRC,		(void*) _ch_grp_1_src			},
	{ DSI16WRC_IOCTL_CHANNELS_READY,	(void*) _channels_ready			},
	{ DSI16WRC_IOCTL_CONTROL_MODE,		(void*) _control_mode			},
	{ DSI16WRC_IOCTL_DATA_FORMAT,		(void*) _data_format			},
	{ DSI16WRC_IOCTL_DATA_WIDTH,		(void*) _data_width				},
	{ DSI16WRC_IOCTL_DIO_DIR_OUT,		(void*) _dio_dir_out			},
	{ DSI16WRC_IOCTL_DIO_READ,			(void*) _dio_read				},
	{ DSI16WRC_IOCTL_DIO_WRITE,			(void*) _dio_write				},
	{ DSI16WRC_IOCTL_EXT_CLK_SRC,		(void*) _ext_clk_src			},
	{ DSI16WRC_IOCTL_IRQ_SEL,			(void*) _irq_sel				},
	{ DSI16WRC_IOCTL_MASTER_CLK_ADJ,	(void*) _master_clk_adj			},
	{ DSI16WRC_IOCTL_NDIV,				(void*) _ndiv					},
	{ DSI16WRC_IOCTL_NREF,				(void*) _nref					},
	{ DSI16WRC_IOCTL_NVCO,				(void*) _nvco					},
	{ DSI16WRC_IOCTL_RANGE,				(void*) _range					},
	{ DSI16WRC_IOCTL_RX_IO_ABORT,		(void*) _rx_io_abort			},
	{ DSI16WRC_IOCTL_RX_IO_MODE,		(void*) _rx_io_mode				},
	{ DSI16WRC_IOCTL_RX_IO_OVERFLOW,	(void*) _rx_io_overflow			},
	{ DSI16WRC_IOCTL_RX_IO_TIMEOUT,		(void*) _rx_io_timeout			},
	{ DSI16WRC_IOCTL_RX_IO_UNDERFLOW,	(void*) _rx_io_underflow		},
	{ DSI16WRC_IOCTL_SW_SYNC,			(void*) _sw_sync				},
	{ DSI16WRC_IOCTL_SW_SYNC_MODE,		(void*) _sw_sync_mode			},
	{ DSI16WRC_IOCTL_WAIT_EVENT,		(void*) gsc_wait_event_ioctl	},
	{ DSI16WRC_IOCTL_WAIT_CANCEL,		(void*) gsc_wait_cancel_ioctl	},
	{ DSI16WRC_IOCTL_WAIT_STATUS,		(void*) gsc_wait_status_ioctl	},
	{ DSI16WRC_IOCTL_XCVR_TYPE,			(void*) _xcvr_type				},
	{ -1, NULL }
};


