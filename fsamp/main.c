// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/fsamp/main.c $
// $Rev: 32584 $
// $Date: 2015-07-04 17:28:47 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "main.h"



// variables	***************************************************************

static	s32	_fsamp	= 10000;
static	int	_index	= -1;
static	s32	_range	= -1;



/******************************************************************************
*
*	Function:	_parse_args
*
*	Purpose:
*
*		Parse the command line arguments.
*
*	Arguments:
*
*		argc	The number of command line arguments given.
*
*		argv	The list of command line arguments given.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

static int _parse_args(int argc, char** argv)
{
	char	c;
	int		errs	= 0;
	int		i;
	int		j;
	int		k;
	long	l;

	printf("USAGE: fsamp <-r#> <-s#> <index>\n");
	printf("   -r#    Display all computed rates from this # to the below sample rate.\n");
	printf("   -s#    The desired sample rate. The default is %ld.\n", (long) _fsamp);
	printf("   index  The index of the board to access.\n");

	for (i = 1; i < argc; i++)
	{
		if ((argv[i][0] == '-') && (argv[i][1] == 'r') && (argv[i][2]))
		{
			j	= sscanf(&argv[i][2], "%ld%c", &l, &c);

			if (j == 1)
			{
				_range	= l;
				continue;
			}

			errs	= 1;
			printf("ERROR: invalid argument: %s\n", argv[i]);
			break;
		}

		if ((argv[i][0] == '-') && (argv[i][1] == 's') && (argv[i][2]))
		{
			j	= sscanf(&argv[i][2], "%ld%c", &l, &c);

			if ((j == 1) && (l >= 0))
			{
				_fsamp	= l;
				continue;
			}

			errs	= 1;
			printf("ERROR: invalid argument: %s\n", argv[i]);
			break;
		}

		if (_index < 0)
		{
			j	= sscanf(&argv[i][0], "%d%c", &k, &c);

			if ((j == 1) && (k >= 0))
			{
				_index	= k;
				continue;
			}

			errs	= 1;
			printf("ERROR: invalid argument: %s\n", argv[i]);
			break;
		}

		errs	= 1;
		printf("ERROR: invalid argument: #%d, %s\n", i, argv[i]);
		break;
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	_perform_tests
*
*	Purpose:
*
*		Perform the appropriate testing.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

static int _perform_tests(int fd)
{
	int			errs	= 0;
	const char*	psz;
	struct tm*	stm;
	time_t		tt;

	time(&tt);
	stm	= localtime(&tt);
	psz	= asctime(stm);
	gsc_label("Performing Operation");
	printf("%s", psz);

	errs	+= os_id_driver(DSI16WRC_BASE_NAME);
	errs	+= dsi16wrc_id_board(fd, -1, NULL);

	errs	+= fsamp_compute(fd, _fsamp, _range);

	return(errs);
}



/******************************************************************************
*
*	Function:	main
*
*	Purpose:
*
*		Control the overall flow of the application.
*
*	Arguments:
*
*		argc			The number of command line arguments.
*
*		argv			The list of command line arguments.
*
*	Returned:
*
*		EXIT_SUCCESS	We tested a device.
*		EXIT_FAILURE	We didn't test a device.
*
******************************************************************************/

int main(int argc, char *argv[])
{
	int		errs;
	long	failures	= 0;
	int		fd			= 0;
	long	hours;
	long	mins;
	time_t	now;
	long	passes		= 0;
	int		qty;
	int		ret			= EXIT_FAILURE;
	long	secs;
	time_t	test;

	gsc_label_init(24);

	for (;;)
	{
		test	= time(NULL);
		printf("fsamp - Compute Ndiv and Nrate for a desired sample rate (Version %s)\n", VERSION);
		errs	= _parse_args(argc, argv);

		if (errs)
			break;

		os_id_host();
		qty		= os_count_boards(DSI16WRC_BASE_NAME);
		errs	= gsc_select_1_dev(qty, &_index);

		if ((qty <= 0) || (errs))
			break;

		gsc_label("Accessing Board Index");
		printf("%d\n", _index);
		fd	= gsc_dev_open(_index, DSI16WRC_BASE_NAME);

		if (fd == -1)
		{
			errs	= 1;
			printf(	"ERROR: Unable to access device %d.", _index);
		}

		if (errs == 0)
		{
			ret		= EXIT_SUCCESS;
			errs	= _perform_tests(fd);
		}

		gsc_dev_close(_index, fd);

		now	= time(NULL);

		if (errs)
		{
			failures++;
			printf(	"\nRESULTS: FAIL <---  (%d error%s)",
					errs,
					(errs == 1) ? "" : "s");
		}
		else
		{
			passes++;
			printf("\nRESULTS: PASS");
		}

		secs	= now - test;
		hours	= secs / 3600;
		secs	= secs % 3600;
		mins	= secs / 60;
		secs	= secs % 60;
		printf(" (duration %ld:%ld:%02ld)\n", hours, mins, secs);
		break;
	}

	return(ret);
}



