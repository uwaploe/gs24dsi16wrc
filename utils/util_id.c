// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/utils/util_id.c $
// $Rev: 32482 $
// $Date: 2015-07-04 15:00:15 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>

#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"



/******************************************************************************
*
*	Function:	_id_board_pci
*
*	Purpose:
*
*		Identify the board using the PCI registers.
*
*	Arguments:
*
*		fd		The handle used to access the device.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

static int _id_board_pci(int fd)
{
	int	errs	= 0;
	u32	reg;

	gsc_label("Vendor ID");
	errs	+= dsi16wrc_reg_read(fd, GSC_PCI_9056_VIDR, &reg);
	printf("0x%04lX      ", (long) reg);

	if (reg == 0x10B5)
	{
		printf("(PLX)\n");
	}
	else
	{
		errs++;
		printf("(UNKNOWN) FAIL <---\n");
	}

	gsc_label("Device ID");
	errs	+= dsi16wrc_reg_read(fd, GSC_PCI_9056_DIDR, &reg);
	printf("0x%04lX      ", (long) reg);

	if (reg == 0x9056)
	{
		printf("(PCI9056)\n");
	}
	else
	{
		errs++;
		printf("(UNKNOWN) FAIL <---\n");
	}

	gsc_label("Sub Vendor ID");
	errs	+= dsi16wrc_reg_read(fd, GSC_PCI_9056_SVID, &reg);
	printf("0x%04lX      ", (long) reg);

	if (reg == 0x10B5)
	{
		printf("(PLX)\n");
	}
	else
	{
		errs++;
		printf("(UNKNOWN) FAIL <---\n");
	}

	gsc_label("Subsystem ID");
	errs	+= dsi16wrc_reg_read(fd, GSC_PCI_9056_SID, &reg);
	printf("0x%04lX      ", (long) reg);

	if (reg == 0x3466)
	{
		printf("(24DSI16WRC)\n");
	}
	else
	{
		errs++;
		printf("(UNKNOWN) FAIL <---\n");
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	dsi16wrc_id_board
*
*	Purpose:
*
*		Identify the board.
*
*	Arguments:
*
*		fd		The handle to use to access the board.
*
*		index	The index of the board or -1 if we don't care.
*
*		desc	A description of the board or NULL if we don't care.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int dsi16wrc_id_board(int fd, int index, const char* desc)
{
	s32	device_type;
	int	errs	= 0;

	gsc_label_index("Board", index);

	if (desc == NULL)
	{
		errs	= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_DEVICE_TYPE, &device_type);

		if (errs == 0)
		{
			switch (device_type)
			{
				default:

					errs	= 1;
					printf(	"FAIL <---  (unexpected device type: %ld)",
							(long) device_type);
					break;

				case GSC_DEV_TYPE_24DSI16WRC:

					desc	= "24DSI16WRC";
					break;
			}
		}
	}

	printf("%s\n", desc ? desc : "");
	gsc_label_level_inc();

	errs	+= _id_board_pci(fd);

	gsc_label_level_dec();

	return(errs);
}



