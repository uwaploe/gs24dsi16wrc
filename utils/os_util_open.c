// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/utils/linux/os_util_open.c $
// $Rev: 32034 $
// $Date: 2015-06-15 19:08:24 -0500 (Mon, 15 Jun 2015) $

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>

#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	os_open
*
*	Purpose:
*
*		Perform an open on the device with the specified index.
*
*	Arguments:
*
*		index	The index of the board to accessed.
*
*		base	The base name for the /proc file entry.
*
*	Returned:
*
*		>= 0	The handle to the board to access.
*		-1		There was a problem. Consult errno.
*
******************************************************************************/

int os_open(unsigned int index, const char* base)
{
	int		fd;
	char	name[128];

	sprintf(name, "/dev/%s.%u", base, index);
	fd	= open(name, O_RDWR);
	return(fd);
}


