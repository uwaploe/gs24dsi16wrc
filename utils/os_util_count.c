// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/utils/linux/os_util_count.c $
// $Rev: 32034 $
// $Date: 2015-06-15 19:08:24 -0500 (Mon, 15 Jun 2015) $

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>

#include "os_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	os_count_boards
*
*	Purpose:
*
*		Count the number of installed boards of the given name.
*
*	Arguments:
*
*		base	The base name for the /proc file entry.
*
*	Returned:
*
*		> 0		The number of boards found.
*		== 0	No boards were found.
*
******************************************************************************/

int os_count_boards(const char* base)
{
	char		buf[1024];
	int			fd;
	const char*	key		= "boards: ";
	char*		ptr;
	int			qty		= 0;

	for (;;)	// A convenience loop.
	{
		sprintf(buf, "/proc/%s", base);
		fd	= open(buf, S_IRUSR);

		if (fd == -1)
		{
			qty	= 0;
			break;
		}

		memset(buf, 0, sizeof(buf));
		read(fd, buf, sizeof(buf));
		buf[sizeof(buf) - 1]	= 0;
		close(fd);
		ptr	= strstr(buf, key);

		if (ptr)
		{
			ptr	+= strlen(key);
			qty	= atoi(ptr);
		}

		if (qty <= 0)
			qty	= 0;

		break;
	}

	return(qty);
}



