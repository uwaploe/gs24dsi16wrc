// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/gsc_irq.c $
// $Rev: 32767 $
// $Date: 2015-09-07 15:18:09 -0500 (Mon, 07 Sep 2015) $

#include "main.h"



// #defines *******************************************************************

#ifndef GSC_IRQ_NOT_USED
#if (GSC_DEVS_PER_BOARD > 1)
	#define	GSC_WAIT_RESUME_IRQ_MAIN(dev,flags)			_wait_resume_irq_main(dev,flags)
	#define	GSC_WAIT_RESUME_IRQ_MAIN_DMA(dev,dma,flags)	_wait_resume_irq_main_dma(dev,dma,flags)
#else
	#define	GSC_WAIT_RESUME_IRQ_MAIN(dev,flags)			gsc_wait_resume_irq_main(dev,flags)
	#define	GSC_WAIT_RESUME_IRQ_MAIN_DMA(dev,dma,flags)	gsc_wait_resume_irq_main(dev,flags)
#endif

#ifndef	GSC_DMA0_INT_EXTERN
#define	GSC_DMA0_INT_EXTERN(dev)
#endif

#ifndef	GSC_DMA1_INT_EXTERN
#define	GSC_DMA1_INT_EXTERN(dev)
#endif
#endif



//*****************************************************************************
#ifndef GSC_IRQ_NOT_USED
#ifndef PRINTF_ISR
static void PRINTF_ISR(const char* format, ...)
{
}
#endif
#endif



//*****************************************************************************
#ifndef GSC_IRQ_NOT_USED
#if (GSC_DEVS_PER_BOARD > 1)
static void _wait_resume_irq_main(dev_data_t* dev, u32 flags)
{
	int	i;

	for (i = 0; i < GSC_DEVS_PER_BOARD; i++)
		gsc_wait_resume_irq_main(&dev->channel[i], flags);
}
#endif
#endif



//*****************************************************************************
#ifndef GSC_IRQ_NOT_USED
#if (GSC_DEVS_PER_BOARD > 1)
static void _wait_resume_irq_main_dma(dev_data_t* dev, gsc_dma_ch_t* dma, u32 flags)
{
	GSC_ALT_STRUCT_T*	alt		= NULL;
	int					i;
	int					found	= 0;
	u32					type;

	for (i = 0; i < GSC_DEVS_PER_BOARD; i++)
	{
		alt	= &dev->channel[i];

#ifdef DEV_SUPPORTS_READ
		if (alt->rx.dma_channel == dma)
		{
			found	= 1;
			break;
		}
#endif

#ifdef DEV_SUPPORTS_WRITE
		if (alt->tx.dma_channel == dma)
		{
			found	= 1;
			break;
		}
#endif
	}

	if (found)
		type	= flags;
	else
		type	= GSC_WAIT_MAIN_PCI | GSC_WAIT_MAIN_SPURIOUS;

	gsc_wait_resume_irq_main(alt, type);
}
#endif
#endif



/******************************************************************************
*
*	Function:	gsc_irq_access_lock
*
*	Purpose:
*
*		Apply a locking mechanism to prevent simultaneous access to the
*		device's IRQ substructure.
*
*	Arguments:
*
*		dev		The data for the device of interest.
*
*	Returned:
*
*		None.
*
******************************************************************************/

void gsc_irq_access_lock(dev_data_t* dev)
{
	os_spinlock_lock(&dev->spinlock);
}



/******************************************************************************
*
*	Function:	gsc_irq_access_unlock
*
*	Purpose:
*
*		Remove the locking mechanism that prevented simultaneous access to the
*		device's IRQ substructure.
*
*	Arguments:
*
*		dev		The data for the device of interest.
*
*	Returned:
*
*		None.
*
******************************************************************************/

void gsc_irq_access_unlock(dev_data_t* dev)
{
	os_spinlock_unlock(&dev->spinlock);
}



/******************************************************************************
*
*	Function:	gsc_irq_close
*
*	Purpose:
*
*		Perform IRQ actions appropriate for closing a device.
*
*	Arguments:
*
*		dev		The device of interest.
*
*		index	The index of the device channel being referenced.
*
*	Returned:
*
*		None.
*
******************************************************************************/

#ifndef GSC_IRQ_NOT_USED
void gsc_irq_close(dev_data_t* dev, int index)
{
	u32	map;

	if ((index >= 0) && (index < GSC_DEVS_PER_BOARD))
	{
		// Adjust the usage map.
		gsc_irq_access_lock(dev);
		map	= dev->irq.usage_map;
		map	&= ~ ((u32) 0x1 << index);
		dev->irq.usage_map	= map;

		if (map == 0)
		{
			// Reset the device interrupts.
			os_reg_mem_tx_u32(NULL, dev->vaddr.plx_intcsr_32, 0);

			if (dev->irq.acquired)
			{
				// We're the last user so we now release the interrupt. We have
				// to release the lock before we release the interrupt, as the
				// release call causes the ISR to be called.
				gsc_irq_access_unlock(dev);
				os_irq_release(dev);
				gsc_irq_access_lock(dev);
				dev->irq.acquired	= 0;
			}
		}

		gsc_irq_access_unlock(dev);
	}
}
#endif



/******************************************************************************
*
*	Function:	gsc_irq_create
*
*	Purpose:
*
*		Perform a one time initialization of the structure.
*
*	Arguments:
*
*		dev		The device of interest.
*
*	Returned:
*
*		0		All went well.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

#ifndef GSC_IRQ_NOT_USED
int gsc_irq_create(dev_data_t* dev)
{
	memset(&dev->irq, 0, sizeof(dev->irq));
	os_sem_create(&dev->irq.sem);

	dev->vaddr.plx_intcsr_32	= PLX_VADDR(dev, 0x68);

	os_reg_mem_tx_u32(dev, dev->vaddr.plx_intcsr_32, 0);

	// What type device is this?
	dev->irq.did	= os_reg_pci_rx_u16(dev, 0x02);

	switch (dev->irq.did)
	{
		default:
		case 0x906E:
		case 0x9080:

			dev->irq.abort_adrs	= 0;
			break;

		case 0x9056:
		case 0x9656:

			dev->irq.abort_adrs	= 0x104;
			break;
	}

	return(0);
}
#endif



/******************************************************************************
*
*	Function:	gsc_irq_destroy
*
*	Purpose:
*
*		Perform a one time tear down of the structure.
*
*	Arguments:
*
*		dev		The device of interest.
*
*	Returned:
*
*		None.
*
******************************************************************************/

#ifndef GSC_IRQ_NOT_USED
void gsc_irq_destroy(dev_data_t* dev)
{
	if (dev->vaddr.plx_intcsr_32)
		os_reg_mem_tx_u32(NULL, dev->vaddr.plx_intcsr_32, 0);

	os_sem_destroy(&dev->irq.sem);
	memset(&dev->irq, 0, sizeof(dev->irq));
}
#endif



/******************************************************************************
*
*	Function:	gsc_irq_open
*
*	Purpose:
*
*		Perform IRQ actions appropriate for closing a device.
*
*	Arguments:
*
*		dev		The device of interest.
*
*		index	The index of the device channel being referenced.
*
*	Returned:
*
*		0		All went well.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

#ifndef GSC_IRQ_NOT_USED
int gsc_irq_open(dev_data_t* dev, int index)
{
	u32	map;
	int	ret	= 0;
	u32	val;

	if ((index < 0) || (index >= GSC_DEVS_PER_BOARD))
	{
		ret	= -EINVAL;
	}
	else
	{
		// Adjust the usage map.
		gsc_irq_access_lock(dev);
		map	= dev->irq.usage_map;
		dev->irq.usage_map	|= 0x1 << index;

		if (map == 0)
		{
			// We're the first user so we now acquire the interrupt. We have
			// to release the lock before we acquire the interrupt, as the
			// acquire call causes the ISR to be called.
			os_reg_mem_tx_u32(NULL, dev->vaddr.plx_intcsr_32, 0);
			gsc_irq_access_unlock(dev);
			ret	= os_irq_acquire(dev);
			gsc_irq_access_lock(dev);

			if (ret == 0)
			{
				dev->irq.acquired	= 1;
				// Initialize the device interrupts.
				val	= GSC_INTCSR_PCI_INT_ENABLE
					| GSC_INTCSR_LOCAL_INT_ENABLE
					| GSC_INTCSR_ABORT_INT_ENABLE;
				os_reg_mem_tx_u32(NULL, dev->vaddr.plx_intcsr_32, val);
			}
		}

		gsc_irq_access_unlock(dev);
	}

	return(ret);
}
#endif



//*****************************************************************************
#ifndef GSC_IRQ_NOT_USED
static void _isr_abort_service(dev_data_t* dev, u32 intcsr)
{
	u16		mod		= 0;
	int		offset;
	VADDR_T	va;
	u16		val;

	// Report the PCI Abort address.
	va		= PLX_VADDR(dev, dev->irq.abort_adrs);
	val		= os_reg_mem_rx_u32(NULL, va);
	PRINTF_ISR(	"%s: Abort occurred: PCI address 0x%lX\n",
				DEV_NAME,
				(long) val);

	// Clasify the Abort, and clear it.
	offset	= 0x06;
	val		= os_reg_pci_rx_u16(dev, offset);

	if (val & D11)
	{
		mod	|= D11;
		PRINTF_ISR("%s: Target Abort signaled\n", DEV_NAME);
	}

	if (val & D12)
	{
		mod	|= D12;
		PRINTF_ISR("%s: Target Abort received\n", DEV_NAME);
	}

	if (val & D13)
	{
		mod	|= D13;
		PRINTF_ISR("%s: Master Abort received\n", DEV_NAME);
	}

	if (mod)
		os_reg_pci_tx_u16(dev, offset, mod);

	// Report the party involved in the Abort.

	if ((intcsr & D24) == 0)
		PRINTF_ISR("%s: Abort from Direct Master activity\n", DEV_NAME);

	if ((intcsr & D25) == 0)
	{
		dev->dma.channel[0].error	= 1;
		PRINTF_ISR("%s: Abort from DMA Channel 0 activity\n", DEV_NAME);
	}

	if ((intcsr & D26) == 0)
	{
		dev->dma.channel[1].error	= 1;
		PRINTF_ISR("%s: Abort from DMA Channel 1 activity\n", DEV_NAME);
	}

	if ((intcsr & D27) == 0)
		PRINTF_ISR("%s: Target Abort from 256 Master Retrys\n", DEV_NAME);
}
#endif



/******************************************************************************
*
*	Function:	gsc_irq_isr_common
*
*	Purpose:
*
*		Service an interrupt. For safety sake, each detected interrupt is
*		cleared then disabled.
*
*	Arguments:
*
*		dev_id	The structure for the device with the interrupt.
*
*	Returned:
*
*		0		The interrupt was NOT ours.
*		1		The interrupt was ours.
*
******************************************************************************/

#ifndef GSC_IRQ_NOT_USED
int gsc_irq_isr_common(void* dev_id)
{
	dev_data_t*		dev	= (void*) dev_id;
	gsc_dma_ch_t*	dma;
	int				handled;
	u32				intcsr;	// PLX Interrupt Control/Status Register

	gsc_irq_access_lock(dev);

	for (;;)
	{
		intcsr	= os_reg_mem_rx_u32(NULL, dev->vaddr.plx_intcsr_32);

		// PCI ****************************************************************

		if ((intcsr & GSC_INTCSR_PCI_INT_ENABLE) == 0)
		{
			// We don't have interrupts enabled. This isn't ours.
			handled	= 0;
			GSC_WAIT_RESUME_IRQ_MAIN(dev, GSC_WAIT_MAIN_PCI | GSC_WAIT_MAIN_OTHER);
			break;
		}

		// DMA 0 **************************************************************

		if ((intcsr & GSC_INTCSR_DMA_0_INT_ENABLE) &&
			(intcsr & GSC_INTCSR_DMA_0_INT_ACTIVE))
		{
			// This is a DMA0 interrupt.
			handled	= 1;
			dma		= &dev->dma.channel[0];

			// Clear the DMA DONE interrupt.
			os_reg_mem_tx_u8(NULL, dma->vaddr.csr_8, GSC_DMA_CSR_CLEAR);

			// Allow any external processing.
			GSC_DMA0_INT_EXTERN(dev);

			// Disable the DMA 0 interrupt.
			intcsr	&= ~GSC_INTCSR_DMA_0_INT_ENABLE;
			os_reg_mem_tx_u32(NULL, dev->vaddr.plx_intcsr_32, intcsr);

			// Resume any blocked threads.
			GSC_WAIT_RESUME_IRQ_MAIN_DMA(dev, dma, GSC_WAIT_MAIN_PCI | GSC_WAIT_MAIN_DMA0);
			break;
		}

		// DMA 1 **************************************************************

		if ((intcsr & GSC_INTCSR_DMA_1_INT_ENABLE) &&
			(intcsr & GSC_INTCSR_DMA_1_INT_ACTIVE))
		{
			// This is a DMA1 interrupt.
			handled	= 1;
			dma		= &dev->dma.channel[1];

			// Clear the DMA DONE interrupt.
			os_reg_mem_tx_u8(NULL, dma->vaddr.csr_8, GSC_DMA_CSR_CLEAR);

			// Allow any external processing.
			GSC_DMA1_INT_EXTERN(dev);

			// Disable the DMA 1 interrupt.
			intcsr	&= ~GSC_INTCSR_DMA_1_INT_ENABLE;
			os_reg_mem_tx_u32(NULL, dev->vaddr.plx_intcsr_32, intcsr);

			// Resume any blocked threads.
			GSC_WAIT_RESUME_IRQ_MAIN_DMA(dev, dma, GSC_WAIT_MAIN_PCI | GSC_WAIT_MAIN_DMA1);
			break;
		}

		// LOCAL **************************************************************

		if ((intcsr & GSC_INTCSR_LOCAL_INT_ENABLE) &&
			(intcsr & GSC_INTCSR_LOCAL_INT_ACTIVE))
		{
			// This is a LOCAL interrupt.
			handled	= 1;

			// Let device specific code process local interrupts.
			dev_irq_isr_local_handler(dev);

			// Resume any blocked threads.
			GSC_WAIT_RESUME_IRQ_MAIN(dev, GSC_WAIT_MAIN_PCI | GSC_WAIT_MAIN_GSC);
			break;
		}

		// MAIL BOX ***********************************************************

		if ((intcsr & GSC_INTCSR_MAILBOX_INT_ENABLE) &&
			(intcsr & GSC_INTCSR_MAILBOX_INT_ACTIVE))
		{
			// This is an unexpected MAIL BOX interrupt.
			// We should never receive this interrupt.
			handled	= 1;

			// Disable the MAIL BOX interrupt.
			intcsr	&= ~GSC_INTCSR_MAILBOX_INT_ENABLE;
			os_reg_mem_tx_u32(NULL, dev->vaddr.plx_intcsr_32, intcsr);

			// Resume any blocked threads.
			GSC_WAIT_RESUME_IRQ_MAIN(dev, GSC_WAIT_MAIN_PCI | GSC_WAIT_MAIN_SPURIOUS);
			break;
		}

		// PCI DOORBELL *******************************************************

		if ((intcsr & GSC_INTCSR_PCI_DOOR_INT_ENABLE) &&
			(intcsr & GSC_INTCSR_PCI_DOOR_INT_ACTIVE))
		{
			// This is an unexpected PCI DOORBELL interrupt.
			// We should never receive this interrupt.
			handled	= 1;

			// Disable the PCI DOORBELL interrupt.
			intcsr	&= ~GSC_INTCSR_PCI_DOOR_INT_ENABLE;
			os_reg_mem_tx_u32(NULL, dev->vaddr.plx_intcsr_32, intcsr);

			// Resume any blocked threads.
			GSC_WAIT_RESUME_IRQ_MAIN(dev, GSC_WAIT_MAIN_PCI | GSC_WAIT_MAIN_SPURIOUS);
			break;
		}

		// ABORT **************************************************************

		if ((intcsr & GSC_INTCSR_ABORT_INT_ENABLE) &&
			(intcsr & GSC_INTCSR_ABORT_INT_ACTIVE) &&
			(dev->irq.abort_adrs))
		{
			// This is an unexpected ABORT interrupt.
			// We hope to never receive any of these.
			handled	= 1;

			_isr_abort_service(dev, intcsr);

			// Resume any blocked threads.
			GSC_WAIT_RESUME_IRQ_MAIN(dev, GSC_WAIT_MAIN_PCI | GSC_WAIT_MAIN_SPURIOUS);
			break;
		}

		// LOCAL DOORBELL *****************************************************

		if ((intcsr & GSC_INTCSR_LOC_DOOR_INT_ENABLE) &&
			(intcsr & GSC_INTCSR_LOC_DOOR_INT_ACTIVE))
		{
			// This is an unexpected Local DOORBELL interrupt.
			// We should never receive this interrupt.
			handled	= 1;

			// Disable the Local DOORBELL interrupt.
			intcsr	&= ~GSC_INTCSR_LOC_DOOR_INT_ENABLE;
			os_reg_mem_tx_u32(NULL, dev->vaddr.plx_intcsr_32, intcsr);

			// Resume any blocked threads.
			GSC_WAIT_RESUME_IRQ_MAIN(dev, GSC_WAIT_MAIN_PCI | GSC_WAIT_MAIN_SPURIOUS);
			break;
		}

		// BIST ***************************************************************

		if (intcsr & GSC_INTCSR_BIST_INT_ACTIVE)
		{
			// This is an unexpected BIST interrupt.
			// We should never receive this interrupt.
			handled	= 1;

			// Service the interrupt.
			os_reg_pci_tx_u8(dev, 0x0F, 0);

			// Resume any blocked threads.
			GSC_WAIT_RESUME_IRQ_MAIN(dev, GSC_WAIT_MAIN_PCI | GSC_WAIT_MAIN_SPURIOUS);
			break;
		}

		// LOCAL PARITY ERROR *************************************************

		if ((intcsr & D7) &&
			((dev->irq.did == 0x9056) || (dev->irq.did == 0x9656)))
		{
			// This looks like a local parity error.
			handled	= 1;

			// Service the interrupt.
			intcsr	&= ~D7;
			os_reg_mem_tx_u32(NULL, dev->vaddr.plx_intcsr_32, intcsr);

			// Resume any blocked threads.
			GSC_WAIT_RESUME_IRQ_MAIN(dev, GSC_WAIT_MAIN_PCI | GSC_WAIT_MAIN_SPURIOUS);
			break;
		}

		// This is not one of our interrupts.
		handled	= 0;

		// Resume any blocked threads.
		GSC_WAIT_RESUME_IRQ_MAIN(dev, GSC_WAIT_MAIN_PCI | GSC_WAIT_MAIN_OTHER);
		break;
	}

	gsc_irq_access_unlock(dev);
	return(handled);
}
#endif



/******************************************************************************
*
*	Function:	gsc_irq_local_disable
*
*	Purpose:
*
*		Disable local interrupts. This is for non-ISR use only.
*
*	Arguments:
*
*		dev		The data for the device of interest.
*
*	Returned:
*
*		0		All went well.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

#ifndef GSC_IRQ_NOT_USED
int gsc_irq_local_disable(dev_data_t* dev)
{
	u32	mask	= GSC_INTCSR_LOCAL_INT_ENABLE;

	os_reg_mem_mx_u32(dev, dev->vaddr.plx_intcsr_32, 0, mask);
	return(0);
}
#endif



/******************************************************************************
*
*	Function:	gsc_irq_local_enable
*
*	Purpose:
*
*		Enable local interrupts. This is for non-ISR use only.
*
*	Arguments:
*
*		dev		The data for the device of interest.
*
*	Returned:
*
*		0		All went well.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

#ifndef GSC_IRQ_NOT_USED
int gsc_irq_local_enable(dev_data_t* dev)
{
	u32	mask	= GSC_INTCSR_LOCAL_INT_ENABLE;

	os_reg_mem_mx_u32(dev, dev->vaddr.plx_intcsr_32, mask, mask);
	return(0);
}
#endif


