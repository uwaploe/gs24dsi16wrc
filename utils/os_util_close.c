// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/utils/linux/os_util_close.c $
// $Rev: 32034 $
// $Date: 2015-06-15 19:08:24 -0500 (Mon, 15 Jun 2015) $

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>

#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	os_close
*
*	Purpose:
*
*		Perform a close on the device with the specified access handle.
*
*	Arguments:
*
*		fd		The file descriptor used to access the device.
*
*	Returned:
*
*		None.
*
******************************************************************************/

void os_close(int fd)
{
	close(fd);
}



