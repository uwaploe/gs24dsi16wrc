// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/utils/util_channel_group.c $
// $Rev: 32482 $
// $Date: 2015-07-04 15:00:15 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>

#include "24dsi16wrc.h"
#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi16wrc_channel_group
*
*	Purpose:
*
*		Report the channel group for the specified channel.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		chan	The channel index.
*
*		group	The channel's group index.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi16wrc_channel_group(int fd, s32 chan, s32* group)
{
	s32	chans;
	int	errs;

	group[0]	= 0;
	errs		= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_CHANNEL_QTY, &chans);

	if (errs)
	{
	}
	else if (chan < 0)
	{
		errs++;
		printf(	"FAIL <---  (%d. invalid channel index: %ld, min is %ld)\n",
				__LINE__,
				(long) chan,
				(long) 0);
	}
	else if (chan >= chans)
	{
		errs++;
		printf(	"FAIL <---  (%d. invalid channel index: %ld, max is %ld)\n",
				__LINE__,
				(long) chan,
				(long) chans - 1);
	}
	else
	{
		switch (chans)
		{
			default:

				errs++;
				printf(	"FAIL <---  (%d. unexpected channel count: %ld)\n",
						__LINE__,
						(long) chans);
				break;

			case 4:		group[0]	= (chan <= 1) ? 0 : 1;	break;
			case 8:		group[0]	= (chan <= 3) ? 0 : 1;	break;
			case 16:	group[0]	= (chan <= 7) ? 0 : 1;	break;
		}
	}

	return(errs);
}


