// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/dout/dio.c $
// $Rev: 32584 $
// $Date: 2015-07-04 17:28:47 -0500 (Sat, 04 Jul 2015) $

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "main.h"



//*****************************************************************************
static void _bin_show(u8 data)
{
	u8	bit		= 0x8;
	int	i;

	for (i = 0; i <= 3; i++, bit /= 2)
	{
		if (bit & data)
			printf("1");
		else
			printf("0");
	}
}



//*****************************************************************************
static int _dev_dio_out(int fd, u16 data)
{
	int	errs;
	u32	val		= data;

	_bin_show(data);
	errs	= ioctl(fd, DSI16WRC_IOCTL_DIO_WRITE, &val);
	errs	= (errs == -1) ? 1 : 0;
	return(errs);
}



//*****************************************************************************
static int _dev_dio_out_4(int fd, u8 data)
{
	int	errs	= 0;

	printf("    ");
	errs	+= _dev_dio_out(fd, data);
	printf("\n");
	usleep(250000L);
	return(errs);
}



/******************************************************************************
*
*	Function:	dio_out
*
*	Purpose:
*
*		Perform a GPIO output test.
*
*	Arguments:
*
*		fd		The file descriptor for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors seen.
*
******************************************************************************/

int dio_out(int fd)
{
	int	errs	= 0;
	int	i;

	for (;;)	// A convenience loop
	{
		errs	+= dsi16wrc_initialize(fd, -1);
		errs	+= dsi16wrc_dio_dir_out(fd, -1, 0xF, NULL);

		if (errs)
			break;

		printf("    DIO Out\n");
		printf("    3210\n");
		printf("    ====\n");

		errs	+= _dev_dio_out_4(fd, 0x0);
		errs	+= _dev_dio_out_4(fd, 0xF);
		errs	+= _dev_dio_out_4(fd, 0x0);
		errs	+= _dev_dio_out_4(fd, 0xF);
		errs	+= _dev_dio_out_4(fd, 0x0);

		for (i = 0; i <= 3; i++)
			errs	+= _dev_dio_out_4(fd, 0x1 << i);

		errs	+= _dev_dio_out_4(fd, 0x0);

		for (i = 3; i >= 0; i--)
			errs	+= _dev_dio_out_4(fd, 0x1 << i);

		errs	+= _dev_dio_out_4(fd, 0x0);
		errs	+= _dev_dio_out_4(fd, 0xF);
		errs	+= _dev_dio_out_4(fd, 0x0);
		errs	+= _dev_dio_out_4(fd, 0xF);
		errs	+= _dev_dio_out_4(fd, 0x0);

		gsc_label_level_inc();
		gsc_label("Status");
		printf("%s\n", errs ? "FAIL <---" : "PASS");
		gsc_label_level_dec();
		break;
	}

	return(errs);
}


