// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/fsamp/fsamp.c $
// $Rev: 32584 $
// $Date: 2015-07-04 17:28:47 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>
#include <string.h>

#include "main.h"



// #defines	*******************************************************************

#define	LIMIT_MAX(v,max)		(((v) > (max)) ? (max) : (v))
#define	LIMIT_MIN(v,min)		(((v) < (min)) ? (min) : (v))
#define	LIMIT_RANGE(v,min,max)	(LIMIT_MIN((LIMIT_MAX((v),(max))),(min)))



/******************************************************************************
*
*	Function:	fsamp_compute
*
*	Purpose:
*
*		Configure the board, then capture data.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		fsamp	The requested sample rate.
*
*		options	Display all computed options within this range of the requested
*				sample rate. This is supressed in quiet mode.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int fsamp_compute(int fd, s32 fsamp, s32 range)
{
	int	errs		= 0;
	s32	fref;
	s32	max;
	s32	mca;
	s32	min;
	s32	mode;
	s32	ndiv;
	s32	nref;
	s32	nvco;
	s32	start;
	s32	stop;
	s32	v;

	gsc_label("Fsamp Computation");
	printf("\n");
	gsc_label_level_inc();

	errs	+= dsi16wrc_query_fref_default(fd, -1, &fref);
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_FSAMP_MAX, &max);
	errs	+= dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_FSAMP_MIN, &min);

	if (range < 0)
	{
		errs	+= dsi16wrc_fsamp_compute(fd, -1, 0, fref, &fsamp, &mode, &nvco, &nref, &ndiv, &mca);
	}
	else
	{
		fsamp	= LIMIT_RANGE(fsamp, min, max);
		range	= LIMIT_RANGE(range, min, max);

		if (fsamp < range)
		{
			start	= fsamp;
			stop	= range;
		}
		else
		{
			start	= range;
			stop	= fsamp;
		}

		for (fsamp = start; fsamp <= stop; fsamp++)
		{
			v		= fsamp;
			errs	+= dsi16wrc_fsamp_compute(fd, -1, 0, fref, &v, &mode, &nvco, &nref, &ndiv, &mca);
		}
	}

	gsc_label_level_dec();
	return(errs);
}


