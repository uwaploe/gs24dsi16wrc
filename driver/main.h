// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/driver/main.h $
// $Rev: 32699 $
// $Date: 2015-07-09 17:02:50 -0500 (Thu, 09 Jul 2015) $

#ifndef __MAIN_H__
#define __MAIN_H__

#define	DEV_BAR_SHOW						0
#define	DEV_PCI_ID_SHOW						0

#include "24dsi16wrc.h"

#include "gsc_common.h"
#include "gsc_main.h"



// #defines	*******************************************************************

#define	DEV_MODEL							"24DSI16WRC"	// Upper case form of the below.
#define	DEV_NAME							"24dsi16wrc"	// MUST AGREE WITH DSI16WRC_BASE_NAME

#define DEV_VERSION							"2.0"			// FOR DEVICE SPECIFIC CODE ONLY!
// 2.0	Updated to use the newer common driver sources.
//		Removed GNU notice from non-Linux specific files.
//		Removed Linux specific content from non-Linux specific source files.
//		White space cleanup.
//		Now using a spinlock rather than enabling and disabling interrupts.
//		Updated gsc_irq_open() and gsc_irq_close().
//		Updated gsc_dma_open() and gsc_dma_close().
// 1.5	Reduced #include list in driver interface header.
// 1.4	Corrected system timer count rollover bug in ioctl.c.
// 1.3	Corrected bugs in the overflow/underflow IOCTL services.
//		Corrected a bug in the Buffer Clear IOCTL service.
// 1.2	Updated for the 3.x kernel.
//		Moved the #defines for D0-D31 to common code.
//		Eliminated the global dev_check_id() routine.
//		BAR0 and BAR2 are now the only BAR regions used.
//		Include all common source, though not all are used.
//		Corrected documentation errors in 24dsi16wrc.h.
//		Changed the upper Buffer Threshold value limit to 0x40000.
//		Fixed a DSI16WRC_IOCTL_AI_BUF_CLEAR bug: accessed wrong bit
//		Fixed a DSI16WRC_IOCTL_AI_BUF_ENABLE bug: accessed wrong register/bit
//		Fixed a DSI16WRC_IOCTL_AI_BUF_THRESH bug: access too small a field
//		Fixed a DSI16WRC_IOCTL_AI_CHANNEL_TAG bug: accessed wrong bit
//		Fixed a DSI16WRC_IOCTL_DATA_WIDTH bug: accessed wrong field
//		Fixed a DSI16WRC_IOCTL_IRQ_SEL bug: a value option was missing
// 1.1	Removed a compile bug in ioctl.c.
//		Updated to 122210 board user manual - removed some services, added others.
// 1.0	Initial release.

// I/O services
#define	DEV_PIO_READ_AVAILABLE				pio_read_available
#define	DEV_PIO_READ_WORK					gsc_read_pio_work_32_bit
#define	DEV_DMA_READ_AVAILABLE				dma_read_available
#define	DEV_DMA_READ_WORK					dma_read_work
#define	DEV_DMDMA_READ_AVAILABLE			dmdma_read_available
#define	DEV_DMDMA_READ_WORK					dmdma_read_work

#define	DEV_SUPPORTS_READ
#define	GSC_READ_PIO_WORK_32_BIT

// WAIT services
#define	DEV_WAIT_GSC_ALL					DSI16WRC_WAIT_GSC_ALL
#define	DEV_WAIT_ALT_ALL					DSI16WRC_WAIT_ALT_ALL

// Board COntrol Register definitions
#define	BCTLR_IRQ_AUTO_CAL_DONE				0x0100
#define	BCTLR_IRQ_MASK						0x0700
#define	BCTLR_IRQ_REQUEST					0x0800
#define	BCTLR_AUTO_CAL_START				D7
#define	BCTLR_AUTO_CAL_PASS					D12
#define	INIT_START							D15



// data types	***************************************************************

struct _dev_io_t
{
	os_sem_t			sem;				// Only one Tx or Rx at a time.

	int					abort;
	int					bytes_per_sample;	// Sample size in bytes.
	gsc_dma_ch_t*		dma_channel;		// Use this channel for DMA.
	s32					io_mode;			// PIO, DMA, DMDMA
	u32					io_reg_offset;		// Offset of board's I/O FIFO.
	VADDR_T				io_reg_vaddr;		// Address of board's I/O FIFO.
	os_mem_t			mem;				// I/O buffer.
	int					non_blocking;		// Is this non-blocking I/O?
	s32					overflow_check;		// Check overflow when reading?
	s32					pio_threshold;		// Use PIO if samples <= this.
	s32					timeout_s;			// I/O timeout in seconds.
	s32					underflow_check;	// Check underflow when reading?
};

struct _dev_data_t
{
	os_pci_t*			pci;			// The kernel PCI device descriptor.
	os_spinlock_t		spinlock;		// Control ISR access.
	os_sem_t			sem;			// Control thread access.
	gsc_dev_type_t		board_type;		// Corresponds to basic board type.
	const char*			model;			// Base model number in upper case.
	int					board_index;	// Index of the board being accessed.
	int					in_use;			// Only one user at a time.

	os_bar_t			plx;			// PLX registers in memory space.
	os_bar_t			gsc;			// GSC registers in memory space.

	gsc_dma_t			dma;			// For DMA based I/O.

	gsc_irq_t			irq;			// For interrut support.

	dev_io_t			rx;				// For read operations.

	gsc_wait_node_t*	wait_list;

	struct
	{
		VADDR_T			plx_intcsr_32;	// Interrupt Control/Status Register
		VADDR_T			plx_dmaarb_32;	// DMA Arbitration Register
		VADDR_T			plx_dmathr_32;	// DMA Threshold Register

		VADDR_T			gsc_bctlr_32;	// 0x00 Board Control Register
		VADDR_T			gsc_rcr_32;		// 0x04 Rate Control Register
		VADDR_T			gsc_diopr_32;	// 0x08 Digital I/O Port Register
		VADDR_T			gsc_csar_32;	// 0x0C Clock Source Assignment Register
		VADDR_T			gsc_rdr_32;		// 0x10 Rate Divisors Register
										// 0x14 Reserved
										// 0x18 Reserved
		VADDR_T			gsc_bbsr_32;	// 0x1C Burst Block Size Register
		VADDR_T			gsc_bufcr_32;	// 0x20 Buffer Control Register
		VADDR_T			gsc_bcfgr_32;	// 0x24 Board Configuration Register
		VADDR_T			gsc_bufsr_32;	// 0x28 Buffer Size Register
		VADDR_T			gsc_avr_32;		// 0x2C Auto Cal Values Register
		VADDR_T			gsc_idbr_32;	// 0x30 Input Data Buffer Register
		VADDR_T			gsc_asiocr_32;	// 0x34 Aux Sync I/O Control Register
		VADDR_T			gsc_mcar_32;	// 0x38 Master Clock Adjust Register
		VADDR_T			gsc_bttr_32;	// 0x3C Burst Trigger Timer Register

	} vaddr;

	struct
	{
		u32				gsc_bcfgr_32;	// Board Configuration Register

		s32				auto_cal_ms;	// Maximum ms for auto-cal

		s32				channels_max;	// Maximum channels supported by model.
		s32				channel_qty;	// The number of channels on the board.

		s32				fifo_size;		// Size of FIFO - not the fill level.
		s32				filter_freq;	// 0, 150000, -1, ...
		s32				fref_default;	// 40,000,000
		s32				fsamp_max;		// The maximum Fsamp rate per channel.
		s32				fsamp_min;		// The minimum Fsamp rate per channel.

		s32				initialize_ms;	// Maximum ms for initialize

		s32				ndiv_max;		// Ndiv maximum
		s32				ndiv_min;		// Ndiv minimum
		s32				nref_max;		// Nref maximum
		s32				nref_min;		// Nref minimum
		s32				nvco_max;		// Nvco maximum
		s32				nvco_min;		// Nvco minimum

		s32				fgen_max;		// Rate Generator maximum output rate.
		s32				fgen_min;		// Rate Generator minimum output rate.

		s32				voltage_range;	// 10/5/2.5/1.25 or 10/1/.1/.01

	} cache;
};



// prototypes	***************************************************************

void		dev_io_close(dev_data_t* dev);
int			dev_io_create(dev_data_t* dev);
void		dev_io_destroy(dev_data_t* dev);
int			dev_io_open(dev_data_t* dev);
int			dev_irq_create(dev_data_t* dev);
void		dev_irq_destroy(dev_data_t* dev);
long		dma_read_available(dev_data_t* dev, size_t samples);
long		dma_read_work(dev_data_t* dev, char* buff, size_t samples, os_time_tick_t st_end);
long		dmdma_read_available(dev_data_t* dev, size_t samples);
long		dmdma_read_work(dev_data_t* dev, char* buff, size_t samples, os_time_tick_t st_end);

int			initialize_ioctl(dev_data_t* dev, void* arg);

long		pio_read_available(dev_data_t* dev, size_t samples);



#endif
