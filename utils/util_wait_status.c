// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/utils/util_wait_status.c $
// $Rev: 25371 $
// $Date: 2014-02-26 13:25:18 -0600 (Wed, 26 Feb 2014) $

#include <stdio.h>

#include "24dsi16wrc.h"
#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi16wrc_wait_status
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI16WRC_IOCTL_WAIT_STATUS service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		wait	This is the crieteria to use.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi16wrc_wait_status(int fd, int index, gsc_wait_t* wait)
{
	int	errs;

	gsc_label_index("Wait Status", index);
	errs	= dsi16wrc_dsl_ioctl(fd, DSI16WRC_IOCTL_WAIT_STATUS, wait);
	printf(	"%s  (%ld awaiting threads)\n",
			errs ? "FAIL <---" : "PASS",
			(long) wait->count);
	return(errs);
}



