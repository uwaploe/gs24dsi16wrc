// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/utils/util_ai_buf_clear.c $
// $Rev: 32482 $
// $Date: 2015-07-04 15:00:15 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>

#include "24dsi16wrc.h"
#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi16wrc_ai_buf_clear
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI16WRC_IOCTL_AI_BUF_CLEAR service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi16wrc_ai_buf_clear(int fd, int index)
{
	int	errs;

	gsc_label_index("AI Buffer Clear", index);
	errs	= dsi16wrc_dsl_ioctl(fd, DSI16WRC_IOCTL_AI_BUF_CLEAR, NULL);
	printf("%s\n", errs ? "FAIL <---" : "PASS");
	return(errs);
}



/******************************************************************************
*
*	Function:	dsi16wrc_ai_buf_clear_at_boundary
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI16WRC_IOCTL_AI_BUF_CLEAR IOCTL service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int	dsi16wrc_ai_buf_clear_at_boundary(int fd, int index)
{
	int	errs	= 0;
	s32	mode;

	// Use the SW Sync feature to
	gsc_label_index("ADC Buffer Clear", index);
	errs	+= dsi16wrc_sw_sync_mode_quiet(fd, -1, &mode);

	// Clear the buffer with any over or under flow status.
	errs	+= dsi16wrc_dsl_ioctl(fd, DSI16WRC_IOCTL_AI_BUF_CLEAR, NULL);

	// Clear the buffer on a scan boundary.
	errs	+= dsi16wrc_sw_sync_mode_quiet(fd, DSI16WRC_SW_SYNC_MODE_SYNC, NULL);
	errs	+= dsi16wrc_sw_sync_quiet(fd);
	errs	+= dsi16wrc_channels_ready_quiet(fd, DSI16WRC_CHANNELS_READY_WAIT, NULL);

	errs	+= dsi16wrc_sw_sync_mode_quiet(fd, DSI16WRC_SW_SYNC_MODE_CLR_BUF, NULL);
	errs	+= dsi16wrc_sw_sync_quiet(fd);
	errs	+= dsi16wrc_channels_ready_quiet(fd, DSI16WRC_CHANNELS_READY_WAIT, NULL);

	// Restore the SW Sync Mode.
	errs	+= dsi16wrc_sw_sync_mode_quiet(fd, mode, NULL);

	if (errs == 0)
		printf("PASS  (Cleared on a scan boundary.)\n");

	return(errs);
}



