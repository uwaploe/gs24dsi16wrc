// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/driver/device.c $
// $Rev: 32659 $
// $Date: 2015-07-08 17:09:33 -0500 (Wed, 08 Jul 2015) $

#include "main.h"



// variables	***************************************************************

const gsc_dev_id_t	dev_id_list[]	=
{
	// model		Vendor	Device	SubVen	SubDev	type
	{ "24DSI16WRC",	0x10B5, 0x9056, 0x10B5, 0x3466,	GSC_DEV_TYPE_24DSI16WRC	},
	{ NULL }
};



//*****************************************************************************
static void _channels_compute(dev_data_t* dev)
{
	dev->cache.channels_max	= 16;

	switch (dev->cache.gsc_bcfgr_32 & 0x30000)
	{
		default:
		case 0x30000:

			printk(	"%s, #%d: Board Config Register 0x%lX, D16-D17 unrecognized\n",
					DEV_NAME,
					dev->board_index,
					(long) dev->cache.gsc_bcfgr_32);

		case 0x20000:	dev->cache.channel_qty	= 4;	break;
		case 0x10000:	dev->cache.channel_qty	= 8;	break;
		case 0x00000:	dev->cache.channel_qty	= 16;	break;
	}
}



//*****************************************************************************
static void _filter_compute(dev_data_t* dev)
{
	switch (dev->cache.gsc_bcfgr_32 & 0x1C0000)
	{
		default:

			printk(	"%s, #%d: Board Config Register 0x%lX, D18-D20 unrecognized\n",
					DEV_NAME,
					dev->board_index,
					(long) dev->cache.gsc_bcfgr_32);

		case 0x040000:	dev->cache.filter_freq	= 0;		break;
		case 0x000000:	dev->cache.filter_freq	= 150000;	break;
	}
}



//*****************************************************************************
static void _range_compute(dev_data_t* dev)
{
	if (dev->cache.gsc_bcfgr_32 & D21)
		dev->cache.voltage_range	= DSI16WRC_QUERY_V_RANGE_10_BY_2;
	else
		dev->cache.voltage_range	= DSI16WRC_QUERY_V_RANGE_10_BY_10;
}



/******************************************************************************
*
*	Function:	dev_device_create
*
*	Purpose:
*
*		Do everything needed to setup and use the given device.
*
*	Arguments:
*
*		dev		The structure to initialize.
*
*	Returned:
*
*		0		All is well.
*		< 0		An appropriate error status.
*
******************************************************************************/

int dev_device_create(dev_data_t* dev)
{
	u32	dma;
	int	ret;

	for (;;)	// A convenience loop.
	{
		// Verify some macro contents.
		ret	= gsc_macro_test_base_name(DSI16WRC_BASE_NAME);
		if (ret)	break;

		ret	= gsc_macro_test_model();
		if (ret)	break;

		// PCI setup.
		ret	= os_pci_dev_enable(dev->pci);
		if (ret)	break;

		ret	= os_pci_set_master(dev->pci);
		if (ret)	break;

		// Control ISR access to the device and data structure.
		ret	= os_spinlock_create(&dev->spinlock);
		if (ret)	break;

		// Control access to the device and data structure.
		ret	= os_sem_create(&dev->sem);
		if (ret)	break;

		// Access the BAR regions.
		ret	= gsc_bar_create(dev, 0, &dev->plx, 1, 0);	// memory
		if (ret)	break;

		ret	= gsc_bar_create(dev, 2, &dev->gsc, 1, 0);	// memory
		if (ret)	break;

		// Firmware access.
		dev->vaddr.gsc_asiocr_32	= GSC_VADDR(dev, GSC_REG_OFFSET(DSI16WRC_GSC_ASIOCR));
		dev->vaddr.gsc_avr_32		= GSC_VADDR(dev, GSC_REG_OFFSET(DSI16WRC_GSC_AVR));
		dev->vaddr.gsc_bbsr_32		= GSC_VADDR(dev, GSC_REG_OFFSET(DSI16WRC_GSC_BBSR));
		dev->vaddr.gsc_bcfgr_32		= GSC_VADDR(dev, GSC_REG_OFFSET(DSI16WRC_GSC_BCFGR));
		dev->vaddr.gsc_bctlr_32		= GSC_VADDR(dev, GSC_REG_OFFSET(DSI16WRC_GSC_BCTLR));
		dev->vaddr.gsc_bttr_32		= GSC_VADDR(dev, GSC_REG_OFFSET(DSI16WRC_GSC_BTTR));
		dev->vaddr.gsc_bufcr_32		= GSC_VADDR(dev, GSC_REG_OFFSET(DSI16WRC_GSC_BUFCR));
		dev->vaddr.gsc_bufsr_32		= GSC_VADDR(dev, GSC_REG_OFFSET(DSI16WRC_GSC_BUFSR));
		dev->vaddr.gsc_csar_32		= GSC_VADDR(dev, GSC_REG_OFFSET(DSI16WRC_GSC_CSAR));
		dev->vaddr.gsc_diopr_32		= GSC_VADDR(dev, GSC_REG_OFFSET(DSI16WRC_GSC_DIOPR));
		dev->vaddr.gsc_idbr_32		= GSC_VADDR(dev, GSC_REG_OFFSET(DSI16WRC_GSC_IDBR));
		dev->vaddr.gsc_mcar_32		= GSC_VADDR(dev, GSC_REG_OFFSET(DSI16WRC_GSC_MCAR));
		dev->vaddr.gsc_rcr_32		= GSC_VADDR(dev, GSC_REG_OFFSET(DSI16WRC_GSC_RCR));
		dev->vaddr.gsc_rdr_32		= GSC_VADDR(dev, GSC_REG_OFFSET(DSI16WRC_GSC_RDR));

		// Data cache initialization.
		dev->cache.gsc_bcfgr_32		= os_reg_mem_rx_u32(dev, dev->vaddr.gsc_bcfgr_32);

		dev->model					= dev_id_list[0].model;
		dev->cache.auto_cal_ms		= 35000;
		dev->cache.fgen_max			= 54000000L;
		dev->cache.fgen_min			= 27000000L;
		dev->cache.fifo_size		= _256K;
		dev->cache.fref_default		= 40000000L;;
		dev->cache.fsamp_max		= 105000;
		dev->cache.fsamp_min		= 200;
		dev->cache.initialize_ms	= 20;
		dev->cache.ndiv_max			= 300;
		dev->cache.ndiv_min			= 1;
		dev->cache.nref_max			= 300;
		dev->cache.nref_min			= 25;
		dev->cache.nvco_max			= 300;
		dev->cache.nvco_min			= 25;

		_channels_compute(dev);
		_filter_compute(dev);
		_range_compute(dev);

		// Initialize additional resources.
		ret	= dev_irq_create(dev);
		if (ret)	break;

		ret	= dev_io_create(dev);
		if (ret)	break;

		dma	= GSC_DMA_SEL_STATIC
			| GSC_DMA_CAP_DMA_READ
			| GSC_DMA_CAP_DMDMA_READ;
		ret	= gsc_dma_create(dev, dma, dma);
		break;
	}

	return(ret);
}



/******************************************************************************
*
*	Function:	dev_device_destroy
*
*	Purpose:
*
*		Do everything needed to release the referenced device.
*
*	Arguments:
*
*		dev		The partial data for the device of interest.
*
*	Returned:
*
*		None.
*
******************************************************************************/

void dev_device_destroy(dev_data_t* dev)
{
	if (dev)
	{
		gsc_dma_destroy(dev);
		dev_io_destroy(dev);
		dev_irq_destroy(dev);
		gsc_bar_destroy(&dev->plx);
		gsc_bar_destroy(&dev->gsc);
		os_sem_destroy(&dev->sem);
		os_spinlock_destroy(&dev->spinlock);

		if (dev->pci)
		{
			os_pci_clear_master(dev->pci);
			os_pci_dev_disable(dev->pci);
		}
	}
}


