// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/din/dio.c $
// $Rev: 32584 $
// $Date: 2015-07-04 17:28:47 -0500 (Sat, 04 Jul 2015) $

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "main.h"



//*****************************************************************************
static void _bin_show(u8 data)
{
	u16	bit		= 0x8;
	int	i;

	for (i = 0; i <= 3; i++, bit /= 2)
	{
		if (bit & data)
			printf("1");
		else
			printf("0");
	}
}



//*****************************************************************************
static int _dev_dio_in(int fd)
{
	u8	data;
	int	errs;
	u32	val;

	errs	= ioctl(fd, DSI16WRC_IOCTL_DIO_READ, &val);
	errs	= (errs == -1) ? 1 : 0;
	data	= (u8) (0xF & val);
	_bin_show(data);
	return(errs);
}



/******************************************************************************
*
*	Function:	dio_in
*
*	Purpose:
*
*		Perform a GPIO input test.
*
*	Arguments:
*
*		fd		The file descriptor for the board to access.
*
*		seconds	Input data for this number of seconds.
*
*	Returned:
*
*		>= 0	The number of errors seen.
*
******************************************************************************/

int dio_in(int fd, int seconds)
{
	int		errs	= 0;
	time_t	limit;
	time_t	now;

	for (;;)
	{
		errs	+= dsi16wrc_initialize(fd, -1);
		errs	+= dsi16wrc_dio_dir_out(fd, -1, 0x0, NULL);

		if (errs)
			break;

		printf("    DIO In\n");
		printf("    3210\n");
		printf("    ====\n");
		limit	= time(NULL) + seconds;

		for (;;)
		{
			printf("    ");
			errs	+= _dev_dio_in(fd);
			printf("\n");
			usleep(300000);

			now	= time(NULL);

			if (now >= limit)
				break;
		}

		gsc_label("Status");
		printf("%s\n", errs ? "FAIL <---" : "PASS");
		break;
	}

	return(errs);
}


