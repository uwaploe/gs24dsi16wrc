// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/utils/util_reg.c $
// $Rev: 32482 $
// $Date: 2015-07-04 15:00:15 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>

#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"
#include "gsc_utils.h"



// #defines	*******************************************************************

#define	_GSC_REG(a)		"GSC " #a, DSI16WRC_GSC_##a
#define	NAME_WIDTH		24

#define	FIELD_SHOW(beg,end,eol,lst,dsc)		\
			gsc_reg_field_show(width+10,NAME_WIDTH,value,(beg),(end),(eol),(lst),(dsc))



//*****************************************************************************
static int _decode_bctlr(int fd, int supported, u32 value, int width)
{
	static const char*	_aim[]	=
	{
		"Differential",
		"Reserved",
		"Zero Test",
		"Vref Test"
	};

	static const char*	_range_2[]	=
	{
		"+-2.5 Volts",
		"+-2.5 Volts",
		"+-5 Volts",
		"+-10 Volts"
	};

	static const char*	_range_10[]	=
	{
		"+-0.01 Volts",
		"+-0.1 Volts",
		"+-1 Volts",
		"+-10 Volts"
	};

	static const char*	_irq[]	=
	{
		"Initialization Complete",
		"Auto-Calibration Complete",
		"Channels Ready",
		"Threshold Flag Low-to-High",
		"Threshold Flag High-to-Low",
		"Burst Complete",
		"Reserved",
		"Reserved"
	};

	static const char*	_adc[]		= { "High Resolution",	"High Speed"		};
	static const char*	_control[]	= { "Target",			"Initiator"			};
	static const char*	_disable[]	= { "Disabled",			"Enabled"			};
	static const char*	_extern[]	= { "Group 0 Source",	"Rate generator"	};
	static const char*	_fail[]		= { "Fail",				"Pass"				};
	static const char*	_format[]	= { "Twos Compliment",	"Offset Binary"		};
	static const char*	_idle[]		= { "Idle",				"Initializing"		};
	static const char*	_not_rdy[]	= { "Not Ready",		"Ready"				};
	static const char*	_sync[]		= { "Sync",				"Clear Buffer"		};
	static const char*	_xcvr[]		= { "LVDS",				"TTL"				};

	const char**	_range;
	s32				range	= 0;

	dsi16wrc_query_quiet(fd, DSI16WRC_QUERY_V_RANGE, &range);
	_range	= (range == DSI16WRC_QUERY_V_RANGE_10_BY_10) ? _range_10 : _range_2;

	FIELD_SHOW( 1,  0, 1, _aim,		"Analog Input Mode"		);
	FIELD_SHOW( 3,  2, 1, _range,	"Analog Input Range"	);
	FIELD_SHOW( 4,  4, 1, _format,	"Data Format"			);
	FIELD_SHOW( 5,  5, 1, _control,	"Control Mode"			);
	FIELD_SHOW( 6,  6, 1, _idle,	"SW Sync"				);
	FIELD_SHOW( 7,  7, 1, _idle,	"Auto-Calibration"		);
	FIELD_SHOW(10,  8, 1, _irq,		"IRQ Select"			);
	FIELD_SHOW(11, 11, 1, _idle,	"IRQ Request"			);
	FIELD_SHOW(12, 12, 1, _fail,	"Auto-Cal Status"		);
	FIELD_SHOW(13, 13, 1, _not_rdy,	"Channels Ready"		);
	FIELD_SHOW(14, 14, 1, _idle,	"Buffer Threshold Flag"	);
	FIELD_SHOW(15, 15, 1, _idle,	"Initialize"			);
	FIELD_SHOW(16, 16, 1, _disable,	"Burst Trigger Timer"	);
	FIELD_SHOW(17, 17, 1, _sync,	"Sync Mode"				);
	FIELD_SHOW(18, 18, 1, _extern,	"External Clock Source"	);
	FIELD_SHOW(19, 19, 1, _adc,		"ADC Mode"				);
	FIELD_SHOW(20, 20, 1, _xcvr,	"Transceivers"			);
	FIELD_SHOW(21, 21, 1, _disable,	"Bursting"				);
	FIELD_SHOW(22, 22, 1, _idle,	"Burst Trigger"			);
	FIELD_SHOW(31, 23, 1, NULL,		"Reserved"				);
	return(0);
}



//*****************************************************************************
static int _decode_rcr(int fd, int supported, u32 value, int width)
{
	FIELD_SHOW(11,  0, 1, NULL,	"Nvco"		);
	FIELD_SHOW(23, 12, 1, NULL,	"Nref"		);
	FIELD_SHOW(31, 24, 1, NULL,	"Reserved"	);
	return(0);
}



//*****************************************************************************
static int _decode_diopr(int fd, int supported, u32 value, int width)
{
	static const char*	_in[]	= { "Input",	"Output"	};

	FIELD_SHOW( 3,  0, 1, NULL,	"Digital Data"		);
	FIELD_SHOW( 7,  4, 1, NULL,	"Reserved"			);
	FIELD_SHOW( 8,  8, 1, _in,	"DIO 0 Direction"	);
	FIELD_SHOW( 9,  9, 1, _in,	"DIO 1 Direction"	);
	FIELD_SHOW(10, 10, 1, _in,	"DIO 2 Direction"	);
	FIELD_SHOW(11, 11, 1, _in,	"DIO 3 Direction"	);
	FIELD_SHOW(31, 12, 1, NULL,	"Reserved"			);
	return(0);
}



//*****************************************************************************
static int _decode_csar(int fd, int supported, u32 value, int width)
{
	static const char*	_src[]	=
	{
		"Rate Generator",
		"Reserved",
		"Reserved",
		"Reserved",
		"External Sample Clock",
		"Direct External Sample Clock",
		"Disable",
		"Disable",
		"Reserved",
		"Reserved",
		"Reserved",
		"Reserved",
		"Reserved",
		"Reserved",
		"Reserved",
		"Reserved"
	};

	FIELD_SHOW( 3,  0, 1, _src,	"Group 0 Clock Source"	);
	FIELD_SHOW( 7,  4, 1, _src,	"Group 1 Clock Source"	);
	FIELD_SHOW(31,  8, 1, NULL,	"Reserved"				);
	return(0);
}



//*****************************************************************************
static int _decode_rdr(int fd, int supported, u32 value, int width)
{
	FIELD_SHOW( 8,  0, 1, NULL,	"Ndiv"		);
	FIELD_SHOW(31,  9, 1, NULL,	"Reserved"	);
	return(0);
}



//*****************************************************************************
static int _decode_bbsr(int fd, int supported, u32 value, int width)
{
	FIELD_SHOW(23,  0, 1, NULL,	"Size"		);
	FIELD_SHOW(31, 24, 1, NULL,	"Reserved"	);
	return(0);
}



//*****************************************************************************
static int _decode_bufcr(int fd, int supported, u32 value, int width)
{
	static const char*	_width[]	=
	{
		"16-bits",
		"18-bits",
		"20-bits",
		"24-bits"
	};

	static const char*	_enable[]	= { "Enabled",	"Disabled"	};
	static const char*	_idle[]		= { "Idle",		"Active"	};
	static const char*	_no[]		= { "No",		"Yes"		};

	FIELD_SHOW(18,  0, 1, NULL,		"Threshold"			);
	FIELD_SHOW(19, 19, 1, _enable,	"Buffer Enable"		);
	FIELD_SHOW(20, 20, 1, _idle,	"Clear Buffer"		);
	FIELD_SHOW(22, 21, 1, _width,	"Data Width"		);
	FIELD_SHOW(23, 23, 1, _enable,	"Channel Tag"		);
	FIELD_SHOW(24, 24, 1, _no,		"Buffer Overflow"	);
	FIELD_SHOW(25, 25, 1, _no,		"Buffer Underflow"	);
	FIELD_SHOW(31, 26, 1, NULL,		"Reserved"			);
	return(0);
}



//*****************************************************************************
static int _decode_bcgfr(int fd, int supported, u32 value, int width)
{
	static const char*	_cannels[]	=
	{
		"16 Channels",
		"8 Channels",
		"4 Channels",
		"Reserved"
	};

	static const char*	_filter[]	=
	{
		"150KHz",
		"No Filter",
		"Reserved",
		"Reserved",
		"Reserved",
		"Reserved",
		"Reserved",
		"Reserved"
	};

	static const char*	_range[]	=
	{
		"+-10V, +-1V, +-100mV,+-10mV",
		"+-10V, +-5V, +-2.5V, +-1.25V"
	};

	u32	field;

	FIELD_SHOW(11,  0, 0, NULL,		"Firmware Revision"	);
	field	= GSC_FIELD_DECODE(value, 11, 0);
	printf("%03lX\n", (long) field);

	FIELD_SHOW(15, 12, 1, NULL,		"Reserved"			);
	FIELD_SHOW(17, 16, 1, _cannels,	"Channels"			);
	FIELD_SHOW(20, 18, 1, _filter,	"Filter Frequency"	);
	FIELD_SHOW(21, 21, 1, _range,	"Input Ranges"		);
	FIELD_SHOW(31, 22, 1, NULL,		"Reserved"			);
	return(0);
}



//*****************************************************************************
static int _decode_bufsr(int fd, int supported, u32 value, int width)
{
	u32	field;

	FIELD_SHOW(18,  0, 0, NULL,	"Buffer Fill Level"	);
	field	= GSC_FIELD_DECODE(value, 18, 0);
	printf("%ld samples\n", (long) field);

	FIELD_SHOW(31, 19, 1, NULL,	"Reserved"			);
	return(0);
}



//*****************************************************************************
static int _decode_asiocr(int fd, int supported, u32 value, int width)
{
	static const char*	_clock[]	=
	{
		"Inactive",
		"Active Input",
		"Active Output",
		"Active Output"
	};

	static const char*	_sync[]	=
	{
		"Inactive",
		"Active Input (Low-to-High edge)",
		"Active Output (Positive Pulse)",
		"Reserved"
	};

	FIELD_SHOW( 1,  0, 1, _clock,	"Aux Clock Control Mode"	);
	FIELD_SHOW( 3,  2, 1, _sync,	"Aux SYNC Control Mode"		);
	FIELD_SHOW(31,  4, 1, NULL,		"Reserved"					);
	return(0);
}



//*****************************************************************************
static int _decode_mcar(int fd, int supported, u32 value, int width)
{
	FIELD_SHOW(15,  0, 1, NULL,	"Adjustment"	);
	FIELD_SHOW(31, 16, 1, NULL,	"Reserved"		);
	return(0);
}



//*****************************************************************************
static int _decode_bttr(int fd, int supported, u32 value, int width)
{
	FIELD_SHOW(23,  0, 1, NULL,	"Divisor"	);
	FIELD_SHOW(31, 24, 1, NULL,	"Reserved"	);
	return(0);
}



// variables	***************************************************************

static gsc_reg_def_t	_gsc[]	=
{
	// name	reg			err	value	ask_support	decode			desc
	{ _GSC_REG(BCTLR),	0,	0,		0,			_decode_bctlr,	"Board Control Register"			},
	{ _GSC_REG(RCR),	0,	0,		0,			_decode_rcr,	"Rate Control Register"				},
	{ _GSC_REG(DIOPR),	0,	0,		0,			_decode_diopr,	"Digital I/O Port Register"			},
	{ _GSC_REG(CSAR),	0,	0,		0,			_decode_csar,	"Clock Source Assignment Register"	},
	{ _GSC_REG(RDR),	0,	0,		0,			_decode_rdr,	"Rate Divisors Register"			},
	{ _GSC_REG(BBSR),	0,	0,		0,			_decode_bbsr,	"Burst Block Size Register"			},
	{ _GSC_REG(BUFCR),	0,	0,		0,			_decode_bufcr,	"Buffer Control Register"			},
	{ _GSC_REG(BCFGR),	0,	0,		0,			_decode_bcgfr,	"Board Configuration Register"		},
	{ _GSC_REG(BUFSR),	0,	0,		0,			_decode_bufsr,	"Buffer Size Register"				},
	{ _GSC_REG(AVR),	0,	0,		0,			NULL,			"Auto-Cal Values Register"			},
	{ _GSC_REG(IDBR),	0,	0,		0,			NULL,			"Input Data Buffer Register"		},
	{ _GSC_REG(ASIOCR),	0,	0,		0,			_decode_asiocr,	"Aux Sync I/O Control Register"		},
	{ _GSC_REG(MCAR),	0,	0,		0,			_decode_mcar,	"Master Clock Adjust Register"		},
	{ _GSC_REG(BTTR),	0,	0,		0,			_decode_bttr,	"Burst Trigger Timer Register"		},
	{ NULL }
};



//*****************************************************************************
static const gsc_reg_def_t* _find_reg(u32 reg, const gsc_reg_def_t* list)
{
	const gsc_reg_def_t*	def	= NULL;
	int						i;

	for (i = 0; list[i].name; i++)
	{
		if (reg == list[i].reg)
		{
			def	= &list[i];
			break;
		}
	}

	return(def);
}



/*****************************************************************************
*
*	Function:	dsi16wrc_reg_get_def_id
*
*	Purpose:
*
*		Retrieve the register definition structure given the register id.
*
*	Arguments:
*
*		reg		The id of the register to access.
*
*	Returned:
*
*		NULL	The register id wasn't found.
*		else	A pointer to the register definition.
*
*****************************************************************************/

const gsc_reg_def_t* dsi16wrc_reg_get_def_id(u32 reg)
{
	const gsc_reg_def_t*	def;

	def	= _find_reg(reg, _gsc);
	return(def);
}



/*****************************************************************************
*
*	Function:	dsi16wrc_reg_get_def_index
*
*	Purpose:
*
*		Retrieve the register definition structure based on an index.
*
*	Arguments:
*
*		index	The index of the register to access.
*
*	Returned:
*
*		NULL	The index doesn't correspond to a known register.
*		else	A pointer to the register definition.
*
*****************************************************************************/

const gsc_reg_def_t* dsi16wrc_reg_get_def_index(int index)
{
	const gsc_reg_def_t*	def;

	if (index < 0)
		def	= NULL;
	else if (index >= (ARRAY_ELEMENTS(_gsc) - 1))
		def	= NULL;
	else
		def	= &_gsc[index];

	return(def);
}



/*****************************************************************************
*
*	Function:	dsi16wrc_reg_get_desc
*
*	Purpose:
*
*		Retrieve the description of the specified register.
*
*	Arguments:
*
*		reg		The register whose description is desired.
*
*	Returned:
*
*		!NULL	The register's name.
*
*****************************************************************************/

const char* dsi16wrc_reg_get_desc(u32 reg)
{
	const gsc_reg_def_t*	def;
	const char*				desc;

	def	= _find_reg(reg, _gsc);

	if (def)
		desc	= def->desc;
	else
		desc	= "UNKNOWN";

	return(desc);
}



/*****************************************************************************
*
*	Function:	dsi16wrc_reg_get_name
*
*	Purpose:
*
*		Retrieve the name of the specified register.
*
*	Arguments:
*
*		reg		The register whose name is desired.
*
*	Returned:
*
*		!NULL	The register's name.
*
*****************************************************************************/

const char* dsi16wrc_reg_get_name(u32 reg)
{
	const gsc_reg_def_t*	def;
	const char*				name;

	def	= _find_reg(reg, _gsc);

	if (def)
		name	= def->name;
	else
		name	= "UNKNOWN";

	return(name);
}



/*****************************************************************************
*
*	Function:	dsi16wrc_reg_list
*
*	Purpose:
*
*		List the GSC registers and their values.
*
*	Arguments:
*
*		fd		The handle to access the device.
*
*		detail	List the register details?
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
*****************************************************************************/

int dsi16wrc_reg_list(int fd, int detail)
{
	int	errs;

	errs	= gsc_reg_list(fd, _gsc, detail, dsi16wrc_reg_read);
	return(errs);
}



