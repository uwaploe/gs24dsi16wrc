// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/linux/os_init.c $
// $Rev: 31781 $
// $Date: 2015-06-09 10:27:08 -0500 (Tue, 09 Jun 2015) $

// Linux driver module

#include "main.h"



/******************************************************************************
*
*	Function:	cleanup_module
*
*	Purpose:
*
*		Clean things up when the kernel is about to unload the module.
*
*	Arguments:
*
*		None.
*
*	Returned:
*
*		None.
*
******************************************************************************/

void cleanup_module(void)
{
	dev_data_t*	dev;
	int			i;

	if (gsc_global.major_number >= 0)
	{
		unregister_chrdev(gsc_global.major_number, DEV_NAME);
		gsc_global.major_number	= -1;
	}

	for (i = 0; i < (int) ARRAY_ELEMENTS(gsc_global.dev_list); i++)
	{
		if (gsc_global.dev_list[i])
		{
			dev	= gsc_global.dev_list[i];
			gsc_init_remove_device(dev);
			os_mem_data_free(dev);
		}
	}

	os_ioctl_reset();
	os_proc_stop();
	gsc_global.dev_qty	= 0;

	if (gsc_global.driver_loaded)
	{
		gsc_global.driver_loaded	= 0;
		printk(	"%s: driver version %s successfully removed.\n",
				DEV_NAME,
				GSC_DRIVER_VERSION);
	}
}



/******************************************************************************
*
*	Function:	init_module
*
*	Purpose:
*
*		Initialize the driver upon loading.
*
*	Arguments:
*
*		None.
*
*	Returned:
*
*		0		All went well.
*		< 0		An appropriate error status.
*
******************************************************************************/

int init_module(void)
{
	dev_data_t*	dev		= NULL;
	os_pci_t*	pci		= NULL;
	int			ret		= 0;
	int			size	= sizeof(dev_data_t);

	sprintf(gsc_global.built, "%s, %s", __DATE__, __TIME__);
	gsc_global.major_number	= -1;
	printk(	"%s: driver loading: version %s, built %s\n",
			DEV_NAME,
			GSC_DRIVER_VERSION,
			gsc_global.built);

	// Locate the devices and add them to our list.

	PCI_DEVICE_LOOP(pci)
	{
		if (dev == NULL)
		{
			dev	= os_mem_data_alloc(size);

			if (dev == NULL)
				break;
		}

		memset(dev, 0, size);
		dev->pci	= pci;
		ret			= gsc_init_add_device(dev);

		if (ret > 0)
		{
			// The referenced device was added.
			printk("%s: device loaded: %s\n", DEV_NAME, dev->model);
			dev	= NULL;
		}
		else if (ret == 0)
		{
			// The referenced device was not added.
			// This is not an error condition.
		}
		else
		{
			// The referenced device was not added.
			// An error may have been reported.
		}
	}

	if (dev)
	{
		os_mem_data_free(dev);
		dev	= NULL;
	}

	// Perform initialization following device discovery.

	for (;;)	// A convenience loop.
	{
		if (gsc_global.dev_qty <= 0)
		{
			ret	= -ENODEV;
			cleanup_module();
			printk(	"%s: driver load failure: version %s, built %s\n",
					DEV_NAME,
					GSC_DRIVER_VERSION,
					gsc_global.built);
			break;
		}

		gsc_global.fops.open	= os_open;
		gsc_global.fops.release	= os_close;
		gsc_global.fops.read	= os_read;
		gsc_global.fops.write	= os_write;
		IOCTL_SET_BKL(&gsc_global.fops, os_ioctl_bkl);
		IOCTL_SET_COMPAT(&gsc_global.fops, os_ioctl_compat);
		IOCTL_SET_UNLOCKED(&gsc_global.fops, os_ioctl_unlocked);
		SET_MODULE_OWNER(&gsc_global.fops);

		gsc_global.major_number	= register_chrdev(	0,
													DEV_NAME,
													&gsc_global.fops);

		if (gsc_global.major_number < 0)
		{
			ret	= -ENODEV;
			printk(	"%s: init_module, line %d:"
					" register_chrdev failed.\n",
					DEV_NAME,
					__LINE__);
			cleanup_module();
			break;
		}

		ret	= os_proc_start();

		if (ret == 0)
			ret	= gsc_ioctl_init();

		if (ret == 0)
			ret	= os_ioctl_init();

		if (ret)
		{
			cleanup_module();
			break;
		}

		gsc_global.driver_loaded	= 1;
		printk(	"%s: driver loaded: version %s, built %s\n",
				DEV_NAME,
				GSC_DRIVER_VERSION,
				gsc_global.built);
		ret	= 0;
		break;
	}

	return(ret);
}


