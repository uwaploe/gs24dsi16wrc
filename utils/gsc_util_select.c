// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/utils/gsc_util_select.c $
// $Rev: 32077 $
// $Date: 2015-06-16 16:16:20 -0500 (Tue, 16 Jun 2015) $

#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	gsc_select_1_dev
*
*	Purpose:
*
*		Select the device to utilize when more than one is present.
*
*	Arguments:
*
*		qty		The number of installed devices.
*
*		index	The device to use. This starts as the default, which we adjust
*				here as appropriate.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int gsc_select_1_dev(int qty, int* index)
{
	char	buf[128];
	int		errs	= 0;

	if (qty < 2)
	{
		if (index[0] > 0)
		{
			errs	= 1;
			printf("FAIL <---  (%d is an invalid selection)\n", index[0]);
		}

		index[0]	= 0;
	}
	else if ((index[0] >= 0) && (index[0] < qty))
	{
	}
	else
	{
		printf("Enter board index 0 to %d: ", qty - 1);
		fgets(buf, sizeof(buf), stdin);
		sscanf(buf, "%d", index);

		if ((index[0] < 0) || (index[0] >= qty))
		{
			errs	= 1;
			printf("%d is an invalid index.\n", index[0]);
		}
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	gsc_select_2_devs
*
*	Purpose:
*
*		Select two devices to utilize when more than two are present.
*
*	Arguments:
*
*		qty		The number of installed devices.
*
*		index_1	The first device to use.
*
*		index_2	The second device to use.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int gsc_select_2_devs(int qty, int* index_1, int* index_2)
{
	char	buf[128];
	int		errs	= 0;

	for (;;)	// A convenience loop.
	{
		if (qty == 0)
		{
			errs	= 1;
			printf("FAIL <--- (no devices present, two are required)\n");
			break;
		}

		if (qty == 1)
		{
			errs	= 1;
			printf("FAIL <--- (only one device present, two are required)\n");
			break;
		}

		if ((qty == 2) && (index_1[0] < 0) && (index_2[0] < 0))
		{
			index_1[0]	= 0;
			index_2[0]	= 1;
		}

		if ((index_1[0] < 0) || (index_1[0] >= qty))
		{
			printf("Enter the first board index: ");
			fgets(buf, sizeof(buf), stdin);
			sscanf(buf, "%d", index_1);
		}

		if ((index_2[0] < 0) || (index_2[0] >= qty))
		{
			printf("Enter the second board index: ");
			fgets(buf, sizeof(buf), stdin);
			sscanf(buf, "%d", index_2);
		}

		gsc_label("Indexes to Access");

		if ((index_1[0] < 0) || (index_1[0] >= qty))
		{
			errs	= 1;
			printf("FAIL <--- (the first index is invalid: %d)\n", index_1[0]);
			break;
		}

		if ((index_2[0] < 0) || (index_2[0] >= qty) || (index_2 == index_1))
		{
			errs	= 1;
			printf("FAIL <--- (the second index is invalid: %d)\n", index_2[0]);
			break;
		}

		printf("%d and %d\n", index_1[0], index_2[0]);
		break;
	}

	return(errs);
}


