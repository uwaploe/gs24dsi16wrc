// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/linux/os_reg.c $
// $Rev: 32673 $
// $Date: 2015-07-09 15:56:43 -0500 (Thu, 09 Jul 2015) $

// Linux driver module

#include "main.h"
#include <linux/pci.h>



//*****************************************************************************
void os_reg_io_mx_u8(dev_data_t* dev, VADDR_T va, u8 value, u8 mask)
{
	u8	reg;

	if (dev)
		gsc_irq_access_lock(dev);

	reg	= inb((unsigned) (unsigned long) va);
	reg	= (reg & ~mask) | (value & mask);
	outb(value, (unsigned) (unsigned long) va);

	if (dev)
		gsc_irq_access_unlock(dev);
}



//*****************************************************************************
void os_reg_io_mx_u16(dev_data_t* dev, VADDR_T va, u16 value, u16 mask)
{
	u16	reg;

	if (dev)
		gsc_irq_access_lock(dev);

	reg	= inw((unsigned) (unsigned long) va);
	reg	= (reg & ~mask) | (value & mask);
	outw(value, (unsigned) (unsigned long) va);

	if (dev)
		gsc_irq_access_unlock(dev);
}



//*****************************************************************************
void os_reg_io_mx_u32(dev_data_t* dev, VADDR_T va, u32 value, u32 mask)
{
	u32	reg;

	if (dev)
		gsc_irq_access_lock(dev);

	reg	= inl((unsigned) (unsigned long) va);
	reg	= (reg & ~mask) | (value & mask);
	outl(value, (unsigned) (unsigned long) va);

	if (dev)
		gsc_irq_access_unlock(dev);
}



//*****************************************************************************
u8 os_reg_io_rx_u8(dev_data_t* dev, VADDR_T va)
{
	u8	value;

	if (dev)
		gsc_irq_access_lock(dev);

	value	= inb((unsigned) (unsigned long) va);

	if (dev)
		gsc_irq_access_unlock(dev);

	return(value);
}



//*****************************************************************************
u16 os_reg_io_rx_u16(dev_data_t* dev, VADDR_T va)
{
	u16	value;

	if (dev)
		gsc_irq_access_lock(dev);

	value	= inw((unsigned) (unsigned long) va);

	if (dev)
		gsc_irq_access_unlock(dev);

	return(value);
}



//*****************************************************************************
u32 os_reg_io_rx_u32(dev_data_t* dev, VADDR_T va)
{
	u32	value;

	if (dev)
		gsc_irq_access_lock(dev);

	value	= inl((unsigned) (unsigned long) va);

	if (dev)
		gsc_irq_access_unlock(dev);

	return(value);
}



//*****************************************************************************
void os_reg_io_tx_u8(dev_data_t* dev, VADDR_T va, u8 value)
{
	if (dev)
		gsc_irq_access_lock(dev);

	outb(value, (unsigned) (unsigned long) va);

	if (dev)
		gsc_irq_access_unlock(dev);
}



//*****************************************************************************
void os_reg_io_tx_u16(dev_data_t* dev, VADDR_T va, u16 value)
{
	if (dev)
		gsc_irq_access_lock(dev);

	outw(value, (unsigned) (unsigned long) va);

	if (dev)
		gsc_irq_access_unlock(dev);
}



//*****************************************************************************
void os_reg_io_tx_u32(dev_data_t* dev, VADDR_T va, u32 value)
{
	if (dev)
		gsc_irq_access_lock(dev);

	outl(value, (unsigned) (unsigned long) va);

	if (dev)
		gsc_irq_access_unlock(dev);
}



//*****************************************************************************
void os_reg_mem_mx_u8(dev_data_t* dev, VADDR_T va, u8 value, u8 mask)
{
	u8	reg;

	if (dev)
		gsc_irq_access_lock(dev);

	reg	= readb(va);
	reg	= (reg & ~mask) | (value & mask);
	writeb(reg, va);

	if (dev)
		gsc_irq_access_unlock(dev);
}



//*****************************************************************************
void os_reg_mem_mx_u16(dev_data_t* dev, VADDR_T va, u16 value, u16 mask)
{
	u16	reg;

	if (dev)
		gsc_irq_access_lock(dev);

	reg	= readw(va);
	reg	= (reg & ~mask) | (value & mask);
	writew(reg, va);

	if (dev)
		gsc_irq_access_unlock(dev);
}



//*****************************************************************************
void os_reg_mem_mx_u32(dev_data_t* dev, VADDR_T va, u32 value, u32 mask)
{
	u32	reg;

	if (dev)
		gsc_irq_access_lock(dev);

	reg	= readl(va);
	reg	= (reg & ~mask) | (value & mask);
	writel(reg, va);

	if (dev)
		gsc_irq_access_unlock(dev);
}



//*****************************************************************************
u8 os_reg_mem_rx_u8(dev_data_t* dev, VADDR_T va)
{
	u8	value;

	if (dev)
		gsc_irq_access_lock(dev);

	value	= readb(va);

	if (dev)
		gsc_irq_access_unlock(dev);

	return(value);
}



//*****************************************************************************
u16 os_reg_mem_rx_u16(dev_data_t* dev, VADDR_T va)
{
	u16	value;

	if (dev)
		gsc_irq_access_lock(dev);

	value	= readw(va);

	if (dev)
		gsc_irq_access_unlock(dev);

	return(value);
}



//*****************************************************************************
u32 os_reg_mem_rx_u32(dev_data_t* dev, VADDR_T va)
{
	u32	value;

	if (dev)
		gsc_irq_access_lock(dev);

	value	= readl(va);

	if (dev)
		gsc_irq_access_unlock(dev);

	return(value);
}



//*****************************************************************************
void os_reg_mem_tx_u8(dev_data_t* dev, VADDR_T va, u8 value)
{
	if (dev)
		gsc_irq_access_lock(dev);

	writeb(value, va);

	if (dev)
		gsc_irq_access_unlock(dev);
}



//*****************************************************************************
void os_reg_mem_tx_u16(dev_data_t* dev, VADDR_T va, u16 value)
{
	if (dev)
		gsc_irq_access_lock(dev);

	writew(value, va);

	if (dev)
		gsc_irq_access_unlock(dev);
}



//*****************************************************************************
void os_reg_mem_tx_u32(dev_data_t* dev, VADDR_T va, u32 value)
{
	if (dev)
		gsc_irq_access_lock(dev);

	writel(value, va);

	if (dev)
		gsc_irq_access_unlock(dev);
}



//*****************************************************************************
void os_reg_pci_mx_u8(dev_data_t* dev, u16 offset, u8 value, u8 mask)
{
	u8	reg;

	gsc_irq_access_lock(dev);
	pci_read_config_byte(dev->pci, offset, &reg);
	reg	= (reg & ~mask) | (value & mask);
	pci_write_config_byte(dev->pci, offset, reg);
	gsc_irq_access_unlock(dev);
}



//*****************************************************************************
void os_reg_pci_mx_u16(dev_data_t* dev, u16 offset, u16 value, u16 mask)
{
	u16	reg;

	gsc_irq_access_lock(dev);
	pci_read_config_word(dev->pci, offset, &reg);
	reg	= (reg & ~mask) | (value & mask);
	pci_write_config_word(dev->pci, offset, reg);
	gsc_irq_access_unlock(dev);
}



//*****************************************************************************
void os_reg_pci_mx_u32(dev_data_t* dev, u16 offset, u32 value, u32 mask)
{
	u32	reg;

	gsc_irq_access_lock(dev);
	pci_read_config_dword(dev->pci, offset, &reg);
	reg	= (reg & ~mask) | (value & mask);
	pci_write_config_dword(dev->pci, offset, reg);
	gsc_irq_access_unlock(dev);
}



//*****************************************************************************
u8 os_reg_pci_rx_u8(dev_data_t* dev, u16 offset)
{
	u8	value;

	gsc_irq_access_lock(dev);
	pci_read_config_byte(dev->pci, offset, &value);
	gsc_irq_access_unlock(dev);

	return(value);
}



//*****************************************************************************
u16 os_reg_pci_rx_u16(dev_data_t* dev, u16 offset)
{
	u16	value;

	gsc_irq_access_lock(dev);
	pci_read_config_word(dev->pci, offset, &value);
	gsc_irq_access_unlock(dev);

	return(value);
}



//*****************************************************************************
u32 os_reg_pci_rx_u32(dev_data_t* dev, u16 offset)
{
	u32	value;

	gsc_irq_access_lock(dev);
	pci_read_config_dword(dev->pci, offset, &value);
	gsc_irq_access_unlock(dev);

	return(value);
}



//*****************************************************************************
void os_reg_pci_tx_u8(dev_data_t* dev, u16 offset, u8 value)
{
	gsc_irq_access_lock(dev);
	pci_write_config_byte(dev->pci, offset, value);
	gsc_irq_access_unlock(dev);
}



//*****************************************************************************
void os_reg_pci_tx_u16(dev_data_t* dev, u16 offset, u16 value)
{
	gsc_irq_access_lock(dev);
	pci_write_config_word(dev->pci, offset, value);
	gsc_irq_access_unlock(dev);
}



//*****************************************************************************
void os_reg_pci_tx_u32(dev_data_t* dev, u16 offset, u32 value)
{
	gsc_irq_access_lock(dev);
	pci_write_config_dword(dev->pci, offset, value);
	gsc_irq_access_unlock(dev);
}


