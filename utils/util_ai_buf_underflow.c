// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI16WRC/utils/util_ai_buf_underflow.c $
// $Rev: 32102 $
// $Date: 2015-06-16 16:29:33 -0500 (Tue, 16 Jun 2015) $

#include <stdio.h>

#include "24dsi16wrc.h"
#include "24dsi16wrc_dsl.h"
#include "24dsi16wrc_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi16wrc_ai_buf_underflow
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI16WRC_IOCTL_AI_BUF_UNDERFLOW service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		set		This is the value to apply.
*
*		get		The results are reported here. This may be NULL.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi16wrc_ai_buf_underflow(int fd, int index, s32 set, s32* get)
{
	char		buf[128];
	int			errs;
	const char*	ptr;

	gsc_label_index("AI Buffer Underflow", index);
	errs	= dsi16wrc_dsl_ioctl(fd, DSI16WRC_IOCTL_AI_BUF_UNDERFLOW, &set);

	switch (set)
	{
		default:

			errs++;
			ptr	= buf;
			sprintf(buf, "Unrecognized option: 0x%lX", (long) set);
			break;

		case DSI16WRC_AI_BUF_UNDERFLOW_NO:

			ptr	= "No";
			break;

		case DSI16WRC_AI_BUF_UNDERFLOW_YES:

			ptr	= "Yes";
			break;
	}

	printf("%s  (%s)\n", errs ? "FAIL <---" : "PASS", ptr);

	if (get)
		get[0]	= set;

	return(errs);
}


