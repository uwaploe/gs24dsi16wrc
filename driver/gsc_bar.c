// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/gsc_bar.c $
// $Rev: 31783 $
// $Date: 2015-06-09 10:42:50 -0500 (Tue, 09 Jun 2015) $

#include "main.h"



/******************************************************************************
*
*	Function:	gsc_bar_create
*
*	Purpose:
*
*		Initialize the given structure according the the BAR index given.
*
*	Arguments:
*
*		dev		The structure for this device.
*
*		index	The BAR index to access.
*
*		bar		The structure to initialize.
*
*		mem		Must this BAR be memory mapped?
*
*		io		Must this BAR be I/O mapped?
*
*	Returned:
*
*		0		All went well.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

int gsc_bar_create(dev_data_t* dev, int index, os_bar_t* bar, int mem, int io)
{
	int	ret		= 0;

	memset(bar, 0,sizeof(os_bar_t));
	bar->index		= index;
	bar->offset		= 0x10 + 4 * index;
	bar->flags		= (u32) os_bar_pci_flags (dev, index);
	bar->phys_adrs	= os_bar_pci_address(dev, index);
	bar->size		= (u32) os_bar_pci_size(dev, index);
	bar->reg		= os_reg_pci_rx_u32(dev, bar->offset);
	bar->io_mapped	= (bar->flags & D0) ? 1 : 0;

	if ((bar->phys_adrs == 0) || (bar->size == 0))
	{
		// This region is unmapped.

		if ((mem) && (io))
		{
			// This BAR region must be mapped.
			ret	= -EINVAL;
		}
		else if (mem)
		{
			// This BAR region must be memory mapped.
			ret	= -EINVAL;
		}
		else if (io)
		{
			// This BAR region must be I/O mapped.
			ret	= -EINVAL;
		}
	}
	else
	{
		ret	= os_bar_region_acquire(bar);

		if (ret)
		{
		}
		else if ((io) && (bar->io_mapped))
		{
			// We wanted I/O mapped, and it is.
		}
		else if ((mem) && (bar->io_mapped == 0))
		{
			// We wanted memory mapped, and it is.
		}
		else if ((io) || (mem))
		{
			// The region is not mapped the correct way.
			ret	= -EINVAL;
		}
	}

	if (ret)
		printf("%s: BAR%d access error.\n", DEV_NAME, index);

#if DEV_BAR_SHOW
	printf(	"BAR%d"
			": Reg 0x%08lX"
			", Map %s"
			", Adrs 0x%08lX"
			", Size %4ld"
			", vaddr 0x%lX"
			"\n",
			(int) index,
			(long) bar->reg,
			bar->size ? (bar->io_mapped ? "I/O" : "mem") : "N/A",
			(long) bar->phys_adrs,
			(long) bar->size,
			(long) bar->vaddr);
#endif

	return(ret);
}



/******************************************************************************
*
*	Function:	gsc_bar_destroy
*
*	Purpose:
*
*		Release the given BAR region and its resources.
*
*	Arguments:
*
*		bar		The structure for the BAR to release.
*
*	Returned:
*
*		None.
*
******************************************************************************/

void gsc_bar_destroy(os_bar_t* bar)
{
	os_bar_region_release(bar);
	memset(bar, 0, sizeof(bar[0]));
}


